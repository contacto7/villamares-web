<?php
					$args = array( 'post_type' => 'curso');
					$query = new WP_Query( $args );
					while ($query->have_posts()) : $query->the_post();
						$url = wp_get_attachment_image_src ( get_post_thumbnail_id($post->ID), 'yamtheme_theme_image_size_476' ); ?>
			
			<div class="item_columna_curso et_pb_column et_pb_column_1_3">
				<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_left item_curso et_pb_blurb_position_top">
				<div class="et_pb_blurb_content">
					<div class="et_pb_main_blurb_image"><a href="<?php the_permalink(); ?>?curso=<?php echo types_render_field( 'asunto', array( ) ); ?>"><img src="<?php echo $url[0]; ?>" alt="<?php the_title(); ?>" class="et-waypoint et_pb_animation_top et-animated"></a></div>
					<div class="et_pb_blurb_container">
						<a href="<?php the_permalink(); ?>?curso=<?php echo types_render_field( 'asunto', array( ) ); ?>"><h6 class="et_pb_module_header"><?php the_title(); ?></h6></a>
						<div class="et_pb_blurb_description">
							<table style="width: 100%;">
							<tbody>
							<tr>
							<td class="info_fecha"><span class="icon_calendar"></span><?php echo types_render_field("fecha-de-curso", array("format" => "d M")); ?></td>
							<td class="link_pagar"><a href="<?php the_permalink(); ?>#pagar"><span class="icon_creditcard"></span>Pagar</a></td>
							</tr>
							</tbody>
							</table>
						</div><!-- .et_pb_blurb_description -->
					</div>
				</div> <!-- .et_pb_blurb_content -->
				</div> <!-- .et_pb_blurb -->
			</div> <!-- .et_pb_column -->

<?php endwhile; wp_reset_query(); ?>
