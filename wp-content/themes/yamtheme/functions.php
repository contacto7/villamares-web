<?php

    register_nav_menus( array(
        'call-to-top'   => esc_html__( 'Menu Call To Top', 'YamTheme' ),
    ) );

/** CHILD THEME **/
function child_theme_enqueue_styles() {
    $parent_style = 'Divi'; // Esta es la hoja de estilos del tema padre.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( $parent_style ), wp_get_theme()->get('Version') );
}
add_action( 'wp_enqueue_scripts', 'child_theme_enqueue_styles' );

/** BUILDER ON CUSTOM POST TYPE **/
function my_et_builder_post_types( $post_types ) {
    $post_types[] = 'curso';

    return $post_types;
}
add_filter( 'et_builder_post_types', 'my_et_builder_post_types' );

// Divi Builder on custom post types
add_filter('et_builder_post_types', 'divicolt_post_types');
add_filter('et_fb_post_types','divicolt_post_types' ); // Enable Divi Visual Builder on the custom post types
function divicolt_post_types($post_types)
{
    foreach (get_post_types() as $post_type) {
        if (!in_array($post_type, $post_types) and post_type_supports($post_type, 'editor')) {
            $post_types[] = $post_type;
        }
    }
    return $post_types;
}

/** builder post types **/

add_action('add_meta_boxes', 'divicolt_add_meta_box');
function divicolt_add_meta_box()
{
    foreach (get_post_types() as $post_type) {
        if (post_type_supports($post_type, 'editor') and function_exists('et_single_settings_meta_box')) {
            $obj= get_post_type_object( $post_type );
            add_meta_box('et_settings_meta_box', sprintf(__('Divi %s Settings', 'Divi'), $obj->labels->singular_name), 'et_single_settings_meta_box', $post_type, 'side', 'high');
        }
    }
}

add_action('admin_head', 'divicolt_admin_js');
function divicolt_admin_js()
{
    $s = get_current_screen();
    if (!empty($s->post_type) and $s->post_type != 'page' and $s->post_type != 'post') {
        ?>
        <script>
            jQuery(function ($) {
                $('#et_pb_layout').insertAfter($('#et_pb_main_editor_wrap'));
            });
        </script>
        <?php
    }
}

/** slug project **/
add_filter( 'et_project_posttype_rewrite_args', 'wpc_projects_slug', 10, 2 );
    function wpc_projects_slug( $slug ) {
    $slug = array( 'slug' => 'servicio' );
    return $slug;
}

//Shortcode to show the module
function showmodule_shortcode($moduleid) {
    extract(shortcode_atts(array('id' =>'*'),$moduleid));
    return do_shortcode('[et_pb_section global_module="'.$id.'"][/et_pb_section]');
}
add_shortcode('showmodule', 'showmodule_shortcode');

/* ============================================================================================================================================= */
/** shortcode cursos **/
function shortcode_lstCursos(  ) {
    ob_start();
    get_template_part('shortcodeCursos');
    return ob_get_clean();
}
add_shortcode( 'cursos', 'shortcode_lstCursos' );

function shortcode_FormularioPayu( $atts ) {
    extract( shortcode_atts( array(
        'apikey' => '4Vj8eK4rloUd272L48hsrarnUA',
        'merchant' => '508029',
        'account' => '512323',
        'referencecode' => 'TestPayU',
        'test' => '1',
        //'responseurl' => 'http://www.test.com/response',
        //'confirmationurl' => 'http://www.test.com/confirmation',
    ), $atts ));

    //$salida = $merchant;
    ob_start();
    //et_template_part('shortcodeFormularioPayu');
    include(locate_template('shortcodeFormularioPayu.php'));
    return ob_get_clean();
}
add_shortcode( 'payu', 'shortcode_FormularioPayu' );

/* ============================================================================================================================================= */
function my_register_scripts() {
wp_enqueue_style( 'slich', get_stylesheet_directory_uri() . '/js/slick/slick.css');
wp_enqueue_style( 'slichtheme', get_stylesheet_directory_uri() . '/js/slick/slick-theme.css');

//wp_enqueue_style( 'slichtcssValidate', get_stylesheet_directory_uri() . '/js/jquery.validate/css/screen.css');

wp_enqueue_script( 'slichjs', get_stylesheet_directory_uri() . '/js/slick/slick.min.js', array ( 'jquery' ), '1.0', false);
wp_enqueue_script( 'jqueryvalidate', get_stylesheet_directory_uri() . '/js/jquery.validate/jquery.validate.min.js', array ( 'jquery' ), '1.0', false);
wp_enqueue_script( 'main', get_stylesheet_directory_uri() . '/js/main.js', array ( 'jquery' ), '1.0', false);
}
add_action('init', 'my_register_scripts');
/* ============================================================================================================================================= */

    if ( function_exists( 'add_image_size' ) )
    {
        add_image_size( 'yamtheme_theme_image_size_476', 476 );
        add_image_size( 'yamtheme_theme_image_size_476_crop', 476, 249, true );
        add_image_size( 'yamtheme_theme_image_size_600', 600 );
        add_image_size( 'yamtheme_theme_image_size_780', 780 );
        add_image_size( 'yamtheme_theme_image_size_1200', 1200 );
        add_image_size( 'yamtheme_theme_image_size_1920', 1920 );
        //add_image_size( 'yamtheme_theme_image_size_300_blog_crop', 320, 180, true );
    }
/* ============================================================================================================================================= */
    function my_login_logo() { ?>
        <style type="text/css">
            #login h1 a, .login h1 a {
                background-image: url(https://s3-us-east-2.amazonaws.com/yam-images/wp-content/uploads/2018/01/logotipo-yam-consulting.png);
    		height:65px;
    		width:320px;
    		background-size: 214px 87px;
    		background-repeat: no-repeat;
            	padding-bottom: 30px;
            }
        </style>
    <?php }
    add_action( 'login_enqueue_scripts', 'my_login_logo' );
?>
