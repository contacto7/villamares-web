jQuery(document).ready(function(jQuery){

	/** jQuery(".bxslider").bxSlider({
		slideWidth: 250,
		minSlides: 1,
		maxSlides: 4,
		slideMargin: 10
	}); **/
	/** jQuery(".bxslider").owlCarousel(
{
    loop:true,
    margin:10,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:3,
            nav:false
        },
        1000:{
            items:5,
            nav:true,
            loop:false
        }
    }
}

		);**/

	/** jQuery(".your-class").slick({
		slideWidth: 250,
		minSlides: 1,
		maxSlides: 4,
		slideMargin: 10
	}); **/

	//$('.et_pb_salvattore_content').addClass('carouselVideos');
	/**jQuery("#formPago").validate({
			rules: {
				description: "required"
			},
			messages: {
				description: "Seleccione una opción",
			}
		});**/

	jQuery(".sliderCasosDeExito").slick({
		dots: true,
	  infinite: true,
	  speed: 500,
	  fade: true,
	  cssEase: 'linear'
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object

	});

	jQuery(".carouselVideos").slick({
	  arrows: true,
	  infinite: true,
	  speed: 300,
	  slidesToShow: 3,
	  slidesToScroll: 3,
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2,
	        infinite: true,
	      }
	    },
	    {
	      breakpoint: 769,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2,
	        infinite: true,
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});
});
