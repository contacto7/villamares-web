<?php if ( 'on' == et_get_option( 'divi_back_to_top', 'false' ) ) : ?>

	<span class="et_pb_scroll_top et-pb-icon"></span>

<?php endif;

// Fix Api Whatsapp on Desktops
// Dev: Jean Livino
$iphone = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone");
$android = strpos($_SERVER['HTTP_USER_AGENT'],"Android");
$palmpre = strpos($_SERVER['HTTP_USER_AGENT'],"webOS");
$berry = strpos($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
$ipod = strpos($_SERVER['HTTP_USER_AGENT'],"iPod");
// check if is a mobile
$url_foot_wp="";
if ($iphone || $android || $palmpre || $ipod || $berry == true){
 $url_foot_wp= "https://api.whatsapp.com/send?phone=51933873998&text=Hola%20Villamares";
}else {
 $url_foot_wp= "https://web.whatsapp.com/send?phone=51933873998&text=Hola%20Villamares";
}
// Fix Api Whatsapp on Desktops
// Dev: Jean Livino

if ( ! is_page_template( 'page-template-blank.php' ) ) : ?>
            
            <div class="footer-wp-wrapp">
                <a href="<?php echo $url_foot_wp; ?>" target="_blank" class="boton-wpp" style="position: fixed;
                    width: 54px;
                    text-align: center;
                    height: 54px;
                    line-height: 4.3;
                    border-radius: 30px;
                    background-color: #ffffff;
                    z-index: 199;
                    bottom: 20px;
                    right: 20px;
                border: 3px solid #0867b1;
                    box-shadow: 3px 3px 5px #2d29299e;"><img style="width:30px" src="/wp-content/uploads/2019/03/whatsapp.png"></a>
            </div>

			<footer id="main-footer heserleon">
				<?php echo do_shortcode('[et_pb_section global_module="400"][/et_pb_section]'); ?>
				<?php get_sidebar( 'footer' ); ?>


		<?php
			if ( has_nav_menu( 'footer-menu' ) ) : ?>

				<div id="et-footer-nav">
					<div class="container">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'footer-menu',
								'depth'          => '1',
								'menu_class'     => 'bottom-nav',
								'container'      => '',
								'fallback_cb'    => '',
							) );
						?>
					</div>
				</div> <!-- #et-footer-nav -->

			<?php endif; ?>

				<div id="footer-bottom">
					<div class="container clearfix">
				<?php
					if ( false !== et_get_option( 'show_footer_social_icons', true ) ) {
						get_template_part( 'includes/social_icons', 'footer' );
					}

					echo et_get_footer_credits();
				?>
					</div>	<!-- .container -->
				</div>
			</footer> <!-- #main-footer -->
		</div> <!-- #et-main-area -->

<?php endif; // ! is_page_template( 'page-template-blank.php' ) ?>

	</div> <!-- #page-container -->

	<?php wp_footer(); ?>
</body>
</html>