<div class="wrap">
  <div id="smpush-icon-devsetting" class="icon32"><br></div>
  <h2><?php echo __('Smart Notification Settings', 'smpush-plugin-lang')?></h2>

  <div id="col-container" class="smpush-settings-page">
    <form action="<?php echo $page_url; ?>" method="post" id="smpush_jform" class="validate">

      

      <input class="smpush_jradio" name="selectDIV" value="web" type="radio" data-icon="<?php echo smpush_imgpath; ?>/web.png" data-labelauty='<?php echo __('Web Push', 'smpush-plugin-lang')?>' />
      <input class="smpush_jradio" name="selectDIV" value="welcmsg" type="radio" data-icon="<?php echo smpush_imgpath; ?>/welcome.png" data-labelauty='<?php echo __('Welcome', 'smpush-plugin-lang')?>' />


      <div id="col-left" class="smpush-tabs-web smpush-tabs-desktop smpush-tabs-popup smpush-tabs-chrome smpush-tabs-firefox smpush-tabs-safari smpush-tabs-opera smpush-tabs-samsung smpush-radio-tabs" style="display:none">
        <input class="smpush_jradio" name="selectDIV" value="desktop" type="radio" data-icon="<?php echo smpush_imgpath; ?>/desktop.png" data-labelauty='<?php echo __('General', 'smpush-plugin-lang')?>' />
        <input class="smpush_jradio" name="selectDIV" value="popup" type="radio" data-icon="<?php echo smpush_imgpath; ?>/popup.png" data-labelauty='<?php echo __('Pop-up', 'smpush-plugin-lang')?>' />
        <input class="smpush_jradio" name="selectDIV" value="chrome" type="radio" data-icon="<?php echo smpush_imgpath; ?>/chrome.png" data-labelauty='<?php echo __('Chrome', 'smpush-plugin-lang')?>' />
        <input class="smpush_jradio" name="selectDIV" value="firefox" type="radio" data-icon="<?php echo smpush_imgpath; ?>/firefox.png" data-labelauty='<?php echo __('Firefox', 'smpush-plugin-lang')?>' />

        <input class="smpush_jradio" name="selectDIV" value="opera" type="radio" data-icon="<?php echo smpush_imgpath; ?>/opera.png" data-labelauty='<?php echo __('Opera', 'smpush-plugin-lang')?>' />
        <input class="smpush_jradio" name="selectDIV" value="samsung" type="radio" data-icon="<?php echo smpush_imgpath; ?>/samsung.png" data-labelauty='<?php echo __('Samsung', 'smpush-plugin-lang')?>' />
      </div>



      <div id="col-left" class="smpush-tabs-desktop smpush-radio-tabs">
        <div id="post-body" class="metabox-holder columns-2">
          <div>
            <div id="namediv" class="stuffbox">
              <h3><label><img src="<?php echo smpush_imgpath; ?>/desktop.png" alt="" /> <span><?php echo __('Desktop Notifications', 'smpush-plugin-lang')?></span></label></h3>
              <div class="inside">
                <table class="form-table">
                  <tbody>
                    <tr valign="top">
                      <td class="first"><label><?php echo __('Title', 'smpush-plugin-lang')?></label></td>
                      <td>
                        <input name="desktop_title" type="text" value="<?php echo self::$apisetting['desktop_title']; ?>" class="regular-text" size="40">
                        <p class="description"><?php echo __('Title of notification appears above the message body.', 'smpush-plugin-lang')?></p>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td colspan="2">
                        <label><input name="desktop_status" type="checkbox" value="1" <?php if (self::$apisetting['desktop_status'] == 1) { ?>checked="checked"<?php } ?>> <?php echo __('Enable desktop push notification', 'smpush-plugin-lang')?></label>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td colspan="2">
                        <label><input name="desktop_gps_status" type="checkbox" value="1" <?php if (self::$apisetting['desktop_gps_status'] == 1) { ?>checked="checked"<?php } ?>> <?php echo __('Enable GPS location detector', 'smpush-plugin-lang')?></label>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td colspan="2">
                        <label><input name="desktop_debug" type="checkbox" value="1" <?php if (self::$apisetting['desktop_debug'] == 1) { ?>checked="checked"<?php } ?>> <?php echo __('Enable debug mode to track any errors in the browser console', 'smpush-plugin-lang')?></label>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td colspan="2">
                        <label><input name="desktop_logged_only" type="checkbox" value="1" <?php if (self::$apisetting['desktop_logged_only'] == 1) { ?>checked="checked"<?php } ?>> <?php echo __('Enable for logged users only', 'smpush-plugin-lang')?></label>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td colspan="2">
                        <label><input name="desktop_admins_only" type="checkbox" value="1" <?php if (self::$apisetting['desktop_admins_only'] == 1) { ?>checked="checked"<?php } ?>> <?php echo __('Enable for administrators only', 'smpush-plugin-lang')?></label>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td colspan="2">
                          <input type="submit" name="submit" class="button button-primary" value="<?php echo __('Save All Settings', 'smpush-plugin-lang')?>">
                          <img src="<?php echo smpush_imgpath; ?>/wpspin_light.gif" class="smpush_process" alt="" />
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="col-left" class="smpush-tabs-popup smpush-radio-tabs">
        <div id="post-body" class="metabox-holder columns-2">
          <div>
            <div id="namediv" class="stuffbox">
              <h3><label><img src="<?php echo smpush_imgpath; ?>/desktop.png" alt="" /> <span><?php echo __('Popup Box Settings', 'smpush-plugin-lang')?></span></label></h3>
              <div class="inside">
                <table class="form-table">
                  <tbody>
                    <tr valign="top">
                      <td class="first"><?php echo __('Request Type', 'smpush-plugin-lang')?></td>
                      <td>
                        <select name="desktop_request_type" class="smpushReuqestTypePicker">
                          <option value="native"><?php echo __('Native Opt-in', 'smpush-plugin-lang')?></option>
                          <option value="popup" <?php if (self::$apisetting['desktop_request_type'] == 'popup'):?>selected="selected"<?php endif;?>><?php echo __('Popup Box', 'smpush-plugin-lang')?></option>
                          <option value="icon" <?php if (self::$apisetting['desktop_request_type'] == 'icon'):?>selected="selected"<?php endif;?>><?php echo __('Icon', 'smpush-plugin-lang')?></option>
                        </select>
                      </td>
                    </tr>
                    <tr valign="top" class="smpush-popup-settings" <?php if(self::$apisetting['desktop_request_type'] != 'popup'):?>style="display:none"<?php endif;?>>
                      <td class="first"><?php echo __('Layout', 'smpush-plugin-lang')?></td>
                      <td>
                        <select name="desktop_popup_layout">
                          <option value="modern"><?php echo __('Modern', 'smpush-plugin-lang')?></option>
                          <option value="native" <?php if (self::$apisetting['desktop_popup_layout'] == 'native'):?>selected="selected"<?php endif;?>><?php echo __('Like Native', 'smpush-plugin-lang')?></option>
                          <option value="flat" <?php if (self::$apisetting['desktop_popup_layout'] == 'flat'):?>selected="selected"<?php endif;?>><?php echo __('Flat Design', 'smpush-plugin-lang')?></option>
                          <option value="fancy" <?php if (self::$apisetting['desktop_popup_layout'] == 'fancy'):?>selected="selected"<?php endif;?>><?php echo __('Fancy Layout', 'smpush-plugin-lang')?></option>
                          <option value="dark" <?php if (self::$apisetting['desktop_popup_layout'] == 'dark'):?>selected="selected"<?php endif;?>><?php echo __('Dark', 'smpush-plugin-lang')?></option>
                          <option value="ocean" <?php if (self::$apisetting['desktop_popup_layout'] == 'ocean'):?>selected="selected"<?php endif;?>><?php echo __('Light Ocean', 'smpush-plugin-lang')?></option>
                        </select>
                      </td>
                    </tr>
                    <tr valign="top" class="smpush-popup-settings" <?php if(self::$apisetting['desktop_request_type'] != 'popup'):?>style="display:none"<?php endif;?>>
                      <td class="first"><?php echo __('Position', 'smpush-plugin-lang')?></td>
                      <td>
                        <select name="desktop_popup_position">
                          <option value="center"><?php echo __('Center of screen', 'smpush-plugin-lang')?></option>
                          <option value="topcenter" <?php if (self::$apisetting['desktop_popup_position'] == 'topcenter'):?>selected="selected"<?php endif;?>><?php echo __('Top center', 'smpush-plugin-lang')?></option>
                          <option value="topright" <?php if (self::$apisetting['desktop_popup_position'] == 'topright'):?>selected="selected"<?php endif;?>><?php echo __('Top right', 'smpush-plugin-lang')?></option>
                          <option value="topleft" <?php if (self::$apisetting['desktop_popup_position'] == 'topleft'):?>selected="selected"<?php endif;?>><?php echo __('Top left', 'smpush-plugin-lang')?></option>
                          <option value="bottomright" <?php if (self::$apisetting['desktop_popup_position'] == 'bottomright'):?>selected="selected"<?php endif;?>><?php echo __('Bottom right', 'smpush-plugin-lang')?></option>
                          <option value="bottomleft" <?php if (self::$apisetting['desktop_popup_position'] == 'bottomleft'):?>selected="selected"<?php endif;?>><?php echo __('Bottom left', 'smpush-plugin-lang')?></option>
                        </select>
                      </td>
                    </tr>
                    <tr valign="top" class="smpush-icon-settings" <?php if(self::$apisetting['desktop_request_type'] != 'icon'):?>style="display:none"<?php endif;?>>
                      <td class="first"><?php echo __('Position', 'smpush-plugin-lang')?></td>
                      <td>
                        <select name="desktop_icon_position">
                          <option value="topright" <?php if (self::$apisetting['desktop_icon_position'] == 'topright'):?>selected="selected"<?php endif;?>><?php echo __('Top right', 'smpush-plugin-lang')?></option>
                          <option value="topleft" <?php if (self::$apisetting['desktop_icon_position'] == 'topleft'):?>selected="selected"<?php endif;?>><?php echo __('Top left', 'smpush-plugin-lang')?></option>
                          <option value="bottomright" <?php if (self::$apisetting['desktop_icon_position'] == 'bottomright'):?>selected="selected"<?php endif;?>><?php echo __('Bottom right', 'smpush-plugin-lang')?></option>
                          <option value="bottomleft" <?php if (self::$apisetting['desktop_icon_position'] == 'bottomleft'):?>selected="selected"<?php endif;?>><?php echo __('Bottom left', 'smpush-plugin-lang')?></option>
                        </select>
                      </td>
                    </tr>
                    <tr valign="top" class="smpush-icon-settings" <?php if(self::$apisetting['desktop_request_type'] != 'icon'):?>style="display:none"<?php endif;?>>
                      <td class="first"><?php echo __('Quick Message', 'smpush-plugin-lang')?></td>
                      <td>
                        <textarea name="desktop_icon_message" rows="8" cols="70" class="regular-text"><?php echo self::$apisetting['desktop_icon_message']; ?></textarea>
                      </td>
                    </tr>
                    <tr valign="top" class="smpush-popup-settings" <?php if(self::$apisetting['desktop_request_type'] != 'popup'):?>style="display:none"<?php endif;?>>
                      <td class="first"><?php echo __('Modal Head Title', 'smpush-plugin-lang')?></td>
                      <td>
                        <input type="text" name="desktop_modal_title" value="<?php echo self::$apisetting['desktop_modal_title']; ?>" class="regular-text" size="40" />
                      </td>
                    </tr>
                    <tr valign="middle" class="smpush-popup-settings" <?php if(self::$apisetting['desktop_request_type'] != 'popup'):?>style="display:none"<?php endif;?>>
                      <td class="first"><?php echo __('Modal Message', 'smpush-plugin-lang')?></td>
                      <td>
                        <textarea name="desktop_modal_message" rows="8" cols="70" class="regular-text"><?php echo self::$apisetting['desktop_modal_message']; ?></textarea>
                      </td>
                    </tr>
                    <tr valign="top" class="smpush-popup-settings" <?php if(self::$apisetting['desktop_request_type'] != 'popup'):?>style="display:none"<?php endif;?>>
                      <td class="first"><?php echo __('Subscribe Button Text', 'smpush-plugin-lang')?></td>
                      <td>
                        <input type="text" name="desktop_btn_subs_text" value="<?php echo self::$apisetting['desktop_btn_subs_text']; ?>" class="regular-text" size="40" />
                      </td>
                    </tr>
                    <tr valign="top" class="smpush-popup-settings" <?php if(self::$apisetting['desktop_request_type'] != 'popup'):?>style="display:none"<?php endif;?>>
                      <td class="first"><?php echo __('Unsubscribe Button Text', 'smpush-plugin-lang')?></td>
                      <td>
                        <input type="text" name="desktop_btn_unsubs_text" value="<?php echo self::$apisetting['desktop_btn_unsubs_text']; ?>" class="regular-text" size="40" />
                      </td>
                    </tr>
                    <tr valign="top" class="smpush-popup-settings" <?php if(self::$apisetting['desktop_request_type'] != 'popup'):?>style="display:none"<?php endif;?>>
                      <td class="first"><?php echo __('Ignore Button Text', 'smpush-plugin-lang')?></td>
                      <td>
                        <input type="text" name="desktop_modal_cancel_text" value="<?php echo self::$apisetting['desktop_modal_cancel_text']; ?>" class="regular-text" size="20" />
                      </td>
                    </tr>
                    <tr valign="top" class="smpush-popup-settings" <?php if(self::$apisetting['desktop_request_type'] != 'popup'):?>style="display:none"<?php endif;?>>
                      <td class="first"><?php echo __('Saved Button Text', 'smpush-plugin-lang')?></td>
                      <td>
                        <input type="text" name="desktop_modal_saved_text" value="<?php echo self::$apisetting['desktop_modal_saved_text']; ?>" class="regular-text" size="20" />
                      </td>
                    </tr>
                    <tr valign="top">
                      <td class="first"><?php echo __('Logo Icon', 'smpush-plugin-lang')?></td>
                      <td>
                        <input class="smpush_upload_field_popupicon" type="url" size="50" name="desktop_popupicon" value="<?php echo self::$apisetting['desktop_popupicon']; ?>" />
                        <input class="smpush_upload_file_btn button action" data-container="smpush_upload_field_popupicon" type="button" value="<?php echo __('Select File', 'smpush-plugin-lang')?>" />
                        <p class="description"><?php echo __('Set a website logo to appear in the pop-up body.', 'smpush-plugin-lang')?></p>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td class="first"><?php echo __('Custom CSS', 'smpush-plugin-lang')?></td>
                      <td>
                        <textarea name="desktop_popup_css" rows="8" cols="70" class="regular-text"><?php echo self::$apisetting['desktop_popup_css']; ?></textarea>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td class="first"><?php echo __('Delay Time', 'smpush-plugin-lang')?></td>
                      <td>
                        <input name="desktop_delay" type="number" value="<?php echo self::$apisetting['desktop_delay']; ?>" class="regular-text" style="width:70px"> <?php echo __('Number of seconds to delay appearing the request permissions for visitors.', 'smpush-plugin-lang')?>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td class="first"><?php echo __('Request Again', 'smpush-plugin-lang')?></td>
                      <td>
                        <input name="desktop_reqagain" type="number" value="<?php echo self::$apisetting['desktop_reqagain']; ?>" class="regular-text" style="width:70px"> <?php echo __('Number of days to request the permissions again from users when click on ignore button.', 'smpush-plugin-lang')?>
                      </td>
                    </tr>
                    <tr valign="middle">
                      <td class="first"><?php echo __('Not Supported Message', 'smpush-plugin-lang')?></td>
                      <td>
                        <textarea name="desktop_notsupport_msg" rows="8" cols="70" class="regular-text"><?php echo self::$apisetting['desktop_notsupport_msg']; ?></textarea>
                        <p class="description"><?php echo __('Show a message if user visits website with browser does not support web push notification.', 'smpush-plugin-lang')?></p>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td class="first"><?php echo __('Show In', 'smpush-plugin-lang')?></td>
                      <td>
                        <select name="desktop_run_places[]" multiple="multiple" size="7" style="width:250px">
                          <option value="noplace" <?php if (in_array('noplace', self::$apisetting['desktop_run_places'])):?>selected="selected"<?php endif;?>><?php echo __('No Place', 'smpush-plugin-lang')?></option>
                          <option value="all" <?php if (in_array('all', self::$apisetting['desktop_run_places'])):?>selected="selected"<?php endif;?>><?php echo __('All Places', 'smpush-plugin-lang')?></option>
                          <option value="homepage" <?php if (in_array('homepage', self::$apisetting['desktop_run_places'])):?>selected="selected"<?php endif;?>><?php echo __('Homepage', 'smpush-plugin-lang')?></option>
                          <option value="post" <?php if (in_array('post', self::$apisetting['desktop_run_places'])):?>selected="selected"<?php endif;?>><?php echo __('Post', 'smpush-plugin-lang')?></option>
                          <option value="page" <?php if (in_array('page', self::$apisetting['desktop_run_places'])):?>selected="selected"<?php endif;?>><?php echo __('Page', 'smpush-plugin-lang')?></option>
                          <option value="category" <?php if (in_array('category', self::$apisetting['desktop_run_places'])):?>selected="selected"<?php endif;?>><?php echo __('Category', 'smpush-plugin-lang')?></option>
                          <option value="taxonomy" <?php if (in_array('taxonomy', self::$apisetting['desktop_run_places'])):?>selected="selected"<?php endif;?>><?php echo __('Taxonomy', 'smpush-plugin-lang')?></option>
                        </select>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td class="first"><?php echo __('Show In Specific Pages', 'smpush-plugin-lang')?></td>
                      <td>
                        <input name="desktop_showin_pageids" type="text" value="<?php echo self::$apisetting['desktop_showin_pageids']; ?>" placeholder="44,1,32,48,3,56,8,43,1713" size="50" class="regular-text">
                        <p class="description"><?php echo __('Put each page ID separated by (,) to request push permissions these pages only.', 'smpush-plugin-lang')?></p>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td colspan="2">
                          <input type="submit" name="submit" class="button button-primary" value="<?php echo __('Save All Settings', 'smpush-plugin-lang')?>">
                          <img src="<?php echo smpush_imgpath; ?>/wpspin_light.gif" class="smpush_process" alt="" />
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="col-left" class="smpush-tabs-popup smpush-radio-tabs">
        <div id="post-body" class="metabox-holder columns-2">
          <div>
            <div id="namediv" class="stuffbox">
              <h3><label><img src="<?php echo smpush_imgpath; ?>/desktop.png" alt="" /> <span><?php echo __('Pay To Read', 'smpush-plugin-lang')?></span></label></h3>
              <div class="inside">
                <table class="form-table">
                  <tbody>
                    <tr valign="top">
                      <td class="first"><?php echo __('Status', 'smpush-plugin-lang')?></td>
                      <td>
                        <select name="desktop_paytoread" onchange="smpushPayToSelector(this.value)">
                          <option value="0"><?php echo __('Disabled', 'smpush-plugin-lang')?></option>
                          <option value="1" <?php if (self::$apisetting['desktop_paytoread'] == 1):?>selected="selected"<?php endif;?>><?php echo __('Popup without dismiss option', 'smpush-plugin-lang')?></option>
                          <option value="2" <?php if (self::$apisetting['desktop_paytoread'] == 2):?>selected="selected"<?php endif;?>><?php echo __('Truncate post contents', 'smpush-plugin-lang')?></option>
                        </select>
                        <p class="description"><?php echo __('Force the visitor to subscribe to continue browsing your content.', 'smpush-plugin-lang')?></p>
                      </td>
                    </tr>
                    <tr valign="middle" class="paytoreadOptions1" <?php if (self::$apisetting['desktop_paytoread'] != 1):?>style="display:none"<?php endif;?>>
                      <td class="first"><?php echo __('Message', 'smpush-plugin-lang')?></td>
                      <td>
                        <textarea name="desktop_paytoread_message" rows="8" cols="70" class="regular-text"><?php echo self::$apisetting['desktop_paytoread_message']; ?></textarea>
                        <p class="description"><?php echo __('Show a message if pay to read option is enabled and visitor blocked the push permissions.', 'smpush-plugin-lang')?></p>
                      </td>
                    </tr>
                    <tr valign="top" class="paytoreadOptions1" <?php if (self::$apisetting['desktop_paytoread'] != 1):?>style="display:none"<?php endif;?>>
                      <td class="first"><?php echo __('Darkness Grade', 'smpush-plugin-lang')?></td>
                      <td>
                        <input name="desktop_paytoread_darkness" type="number" min="1" max="10" value="<?php echo self::$apisetting['desktop_paytoread_darkness']; ?>" class="regular-text" style="width:70px"> <?php echo __('Pay To Read darkness grade from 1 to 10.', 'smpush-plugin-lang')?>
                      </td>
                    </tr>
                    <tr valign="top" class="paytoreadOptions2" <?php if (self::$apisetting['desktop_paytoread'] != 2):?>style="display:none"<?php endif;?>>
                      <td class="first"><?php echo __('Text Size', 'smpush-plugin-lang')?></td>
                      <td>
                        <input name="desktop_paytoread_textsize" type="number" step="50" value="<?php echo self::$apisetting['desktop_paytoread_textsize']; ?>" class="regular-text" style="width:70px"> <?php echo __('Number of allowed characters for post contents before cutting.', 'smpush-plugin-lang')?>
                      </td>
                    </tr>
                    <tr valign="top" class="paytoreadOptions2" <?php if (self::$apisetting['desktop_paytoread'] != 2):?>style="display:none"<?php endif;?>>
                      <td class="first"><?php echo __('Subscribe Message', 'smpush-plugin-lang')?></td>
                      <td>
                        <input name="desktop_paytoread_substext" type="text" value="<?php echo self::$apisetting['desktop_paytoread_substext']; ?>" class="regular-text" size="80">
                        <p class="description"><?php echo __('Write a message for user to know about why can not see the complete post contents.', 'smpush-plugin-lang')?></p>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td colspan="2">
                          <input type="submit" name="submit" class="button button-primary" value="<?php echo __('Save All Settings', 'smpush-plugin-lang')?>">
                          <img src="<?php echo smpush_imgpath; ?>/wpspin_light.gif" class="smpush_process" alt="" />
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="col-left" class="smpush-tabs-chrome smpush-radio-tabs">
        <div id="post-body" class="metabox-holder columns-2">
          <div>
            <div id="namediv" class="stuffbox">
              <h3><label><img src="<?php echo smpush_imgpath; ?>/chrome.png" alt="" /> <span>Chrome</span></label></h3>
              <div class="inside">
                <table class="form-table">
                  <tbody>
                    <tr valign="top">
                      <td colspan="2">
                        <label><input name="desktop_chrome_status" type="checkbox" value="1" <?php if (self::$apisetting['desktop_chrome_status'] == 1) { ?>checked="checked"<?php } ?>> <?php echo __('Enable desktop push notification listener for Chrome browser', 'smpush-plugin-lang')?></label>
                        <p class="description">
                          <?php echo __('Chrome push notification requires your site working under <code>HTTPS</code> protocol .', 'smpush-plugin-lang')?>
                          <a href="//namecheap.pxf.io/c/477005/386535/5618" target="_blank"><?php echo __('Buy one for $9 only', 'smpush-plugin-lang')?></a>
                        </p>
                        <p class="description">
                          <?php echo __('Or get free SSL certificate for 3 months from', 'smpush-plugin-lang')?> <a href="https://ssl.comodo.com/free-ssl-certificate.php" target="_blank"><?php echo __('Comodo.com', 'smpush-plugin-lang')?></a>
                        </p>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td class="first"><?php echo __('API Key', 'smpush-plugin-lang')?></td>
                      <td>
                        <input type="text" name="chrome_apikey" value="<?php echo self::$apisetting['chrome_apikey']; ?>" class="regular-text" size="50" />
                      </td>
                    </tr>
                    <tr valign="top">
                      <td class="first"><?php echo __('Project Number', 'smpush-plugin-lang')?></td>
                      <td>
                        <input type="text" name="chrome_projectid" value="<?php echo self::$apisetting['chrome_projectid']; ?>" class="regular-text" size="30" />
                        <p class="description"><?php echo __('For how to get API key and project number', 'smpush-plugin-lang')?> <a href="https://smartiolabs.com/blog/61/get-api-key-sender-id-fcm-push-notification-firebase/" target="_blank"><?php echo __('click here', 'smpush-plugin-lang')?></a></p>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td class="first"><?php echo __('Default Icon', 'smpush-plugin-lang')?></td>
                      <td>
                        <input class="smpush_upload_field_deskicon" type="url" size="50" name="desktop_deficon" value="<?php echo self::$apisetting['desktop_deficon']; ?>" />
                        <input class="smpush_upload_file_btn button action" data-container="smpush_upload_field_deskicon" type="button" value="<?php echo __('Select File', 'smpush-plugin-lang')?>" />
                        <p class="description"><?php echo __('Choose an icon in a standard size 192x192 px', 'smpush-plugin-lang')?></p>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td colspan="2">
                          <input type="submit" name="submit" class="button button-primary" value="<?php echo __('Save All Settings', 'smpush-plugin-lang')?>">
                          <img src="<?php echo smpush_imgpath; ?>/wpspin_light.gif" class="smpush_process" alt="" />
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="col-left" class="smpush-tabs-firefox smpush-radio-tabs">
        <div id="post-body" class="metabox-holder columns-2">
          <div>
            <div id="namediv" class="stuffbox">
              <h3><label><img src="<?php echo smpush_imgpath; ?>/firefox.png" alt="" /> <span>Firefox</span></label></h3>
              <div class="inside">
                <table class="form-table">
                  <tbody>
                    <tr valign="top">
                      <td colspan="2">
                        <label><input name="desktop_firefox_status" type="checkbox" value="1" <?php if (self::$apisetting['desktop_firefox_status'] == 1) { ?>checked="checked"<?php } ?>> <?php echo __('Enable desktop push notification listener for Firefox browser', 'smpush-plugin-lang')?></label>
                        <p class="description">
                          <?php echo __('Firefox push notification requires your site working under <code>HTTPS</code> protocol .', 'smpush-plugin-lang')?>
                          <a href="//namecheap.pxf.io/c/477005/386535/5618" target="_blank"><?php echo __('Buy one for $9 only', 'smpush-plugin-lang')?></a>
                        </p>
                        <p class="description">
                          <?php echo __('Or get free SSL certificate for 3 months from', 'smpush-plugin-lang')?> <a href="https://ssl.comodo.com/free-ssl-certificate.php" target="_blank"><?php echo __('Comodo.com', 'smpush-plugin-lang')?></a>
                        </p>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td colspan="2">
                          <input type="submit" name="submit" class="button button-primary" value="<?php echo __('Save All Settings', 'smpush-plugin-lang')?>">
                          <img src="<?php echo smpush_imgpath; ?>/wpspin_light.gif" class="smpush_process" alt="" />
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="col-left" class="smpush-tabs-opera smpush-radio-tabs">
        <div id="post-body" class="metabox-holder columns-2">
          <div>
            <div id="namediv" class="stuffbox">
              <h3><label><img src="<?php echo smpush_imgpath; ?>/opera.png" alt="" /> <span>Opera</span></label></h3>
              <div class="inside">
                <table class="form-table">
                  <tbody>
                    <tr valign="top">
                      <td colspan="2">
                        <label><input name="desktop_opera_status" type="checkbox" value="1" <?php if (self::$apisetting['desktop_opera_status'] == 1) { ?>checked="checked"<?php } ?>> <?php echo __('Enable desktop push notification listener for Opera browser', 'smpush-plugin-lang')?></label>
                        <p class="description">
                          <?php echo __('Opera push notification depends on Chrome configurations to work .', 'smpush-plugin-lang')?>
                        </p>
                        <?php echo __('Opera push notification requires your site working under <code>HTTPS</code> protocol .', 'smpush-plugin-lang')?>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td colspan="2">
                          <input type="submit" name="submit" class="button button-primary" value="<?php echo __('Save All Settings', 'smpush-plugin-lang')?>">
                          <img src="<?php echo smpush_imgpath; ?>/wpspin_light.gif" class="smpush_process" alt="" />
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="col-left" class="smpush-tabs-samsung smpush-radio-tabs">
        <div id="post-body" class="metabox-holder columns-2">
          <div>
            <div id="namediv" class="stuffbox">
              <h3><label><img src="<?php echo smpush_imgpath; ?>/samsung.png" alt="" /> <span>Samsung Browser</span></label></h3>
              <div class="inside">
                <table class="form-table">
                  <tbody>
                    <tr valign="top">
                      <td colspan="2">
                        <label><input name="desktop_samsung_status" type="checkbox" value="1" <?php if (self::$apisetting['desktop_samsung_status'] == 1) { ?>checked="checked"<?php } ?>> <?php echo __('Enable desktop push notification listener for Samsung Browser', 'smpush-plugin-lang')?></label>
                        <p class="description">
                          <?php echo __('Samsung Browser push notification depends on Chrome configurations to work .', 'smpush-plugin-lang')?>
                        </p>
                        <?php echo __('Samsung Browser push notification requires your site working under <code>HTTPS</code> protocol .', 'smpush-plugin-lang')?>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td colspan="2">
                          <input type="submit" name="submit" class="button button-primary" value="<?php echo __('Save All Settings', 'smpush-plugin-lang')?>">
                          <img src="<?php echo smpush_imgpath; ?>/wpspin_light.gif" class="smpush_process" alt="" />
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div id="col-left" class="smpush-tabs-welcmsg smpush-radio-tabs">
        <div id="post-body" class="metabox-holder columns-2">
          <div>
            <div id="namediv" class="stuffbox">
              <h3><label><img src="<?php echo smpush_imgpath; ?>/welcome.png" alt="" /> <span><?php echo __('Desktop Welcome Message', 'smpush-plugin-lang')?></span></label></h3>
              <div class="inside">
                <table class="form-table">
                  <tbody>
                    <tr valign="top">
                      <td colspan="2">
                        <label><input name="desktop_welc_status" type="checkbox" value="1" <?php if (self::$apisetting['desktop_welc_status'] == 1) { ?>checked="checked"<?php } ?>> <?php echo __('Enable desktop push notification welcome message', 'smpush-plugin-lang')?></label>
                      </td>
                    </tr>
                    <tr valign="middle">
                      <td class="first"><?php echo __('Title', 'smpush-plugin-lang')?></td>
                      <td>
                         <input name="desktop_welc_title" class="regular-text" type="text" size="50" value="<?php echo self::$apisetting['desktop_welc_title']?>" onkeyup="$('.smpush-sample_notification_title').html(this.value)" />
                      </td>
                      <td rowspan="3">
                        <div class="smpush-sample_notification">
                          <img src="" class="smpush-sample_notification_logo">
                          <button type="button" class="smpush-close" aria-label="Close"><span aria-hidden="true">×</span></button>
                          <div class="smpush-sample_notification_title"><?php echo __('Notification Title', 'smpush-plugin-lang')?></div>
                          <div class="smpush-sample_notification_message"><?php echo __('This is the Notification Message', 'smpush-plugin-lang')?></div>
                          <div class="smpush-sample_notification_url"><?php echo $_SERVER['HTTP_HOST']?></div>
                        </div>
                      </td>
                   </tr>
                   <tr valign="top">
                      <td class="first"><?php echo __('Message', 'smpush-plugin-lang')?></td>
                      <td>
                         <textarea name="desktop_welc_message" cols="40" rows="10" id="smpush-message" onkeyup="$('.smpush-sample_notification_message').html(this.value)" class="regular-text"><?php echo self::$apisetting['desktop_welc_message']?></textarea>
                      </td>
                   </tr>
                   <tr valign="middle">
                      <td class="first"><?php echo __('Icon', 'smpush-plugin-lang')?></td>
                      <td>
                         <input class="smpush_upload_field_deskicon" type="url" size="60" name="desktop_welc_icon" value="<?php echo self::$apisetting['desktop_welc_icon']?>" onchange="$('.smpush-sample_notification_logo').attr('src',this.value)" />
                          <input class="smpush_upload_file_btn button action" data-container="smpush_upload_field_deskicon" type="button" value="<?php echo __('Select File', 'smpush-plugin-lang')?>" />
                          <p class="description"><?php echo __('Choose an icon in a standard size 192x192 px', 'smpush-plugin-lang')?></p>
                      </td>
                   </tr>
                   <tr valign="middle">
                      <td class="first"><?php echo __('Link To Open', 'smpush-plugin-lang')?></td>
                      <td>
                         <input name="desktop_welc_link" class="regular-text" type="url" size="50" value="<?php echo self::$apisetting['desktop_welc_link']?>" />
                         <p class="description"><?php echo __('Open link when user clicks on notification message', 'smpush-plugin-lang')?></p>
                      </td>
                   </tr>
                    <tr valign="top">
                      <td colspan="2">
                          <input type="submit" name="submit" class="button button-primary" value="<?php echo __('Save All Settings', 'smpush-plugin-lang')?>">
                          <img src="<?php echo smpush_imgpath; ?>/wpspin_light.gif" class="smpush_process" alt="" />
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>


    </form>
  </div>
</div>
<style>
input.labelauty + label{margin-top: 5px;height:60px!important;width:70px!important;background-color: #eaeaea!important;}
input.labelauty + label > img{width: 24px!important;height: 24px;}
</style>
<script type="text/javascript">
jQuery(document).ready(function() {
  $(".smpush_jradio").change(function () {
    $(".smpush-radio-tabs").hide();
    $(".smpush-tabs-"+$(this).val()).show();
  });
});
</script>
