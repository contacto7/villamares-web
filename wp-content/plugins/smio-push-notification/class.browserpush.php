<?php

/*======================================================================*\
|| #################################################################### ||
|| # Push Notification System Wordpress Plugin                        # ||
|| # ---------------------------------------------------------------- # ||
|| # Copyright ©2014-2017 Smart IO Labs Inc. All Rights Reserved.     # ||
|| # This file may not be redistributed in whole or significant part. # ||
|| # --- Smart Push Notification System IS NOT FREE SOFTWARE ---      # ||
|| # https://smartiolabs.com/product/push-notification-system         # ||
|| #################################################################### ||
\*======================================================================*/

class smpush_browser_push extends smpush_controller {

  public function __construct() {
    parent::__construct();
  }

  private static function safari() {
    $output = '
var devicetype = "safari";

function smpushRegisterServiceWorker(){
  var pushButton = jQuery(".smpush-push-permission-button");
    pushButton.removeAttr("disabled");
    
    if(smpush_getCookie("smpush_safari_device_token") != ""){
      pushButton.html("'.addslashes(self::$apisetting['desktop_btn_unsubs_text']).'");
    }
    else{
      pushButton.html("'.addslashes(self::$apisetting['desktop_btn_subs_text']).'");
    }
    
    pushButton.click(function() {
      var permissionData = window.safari.pushNotification.permission("'.self::$apisetting['safari_web_id'].'");
      checkRemotePermission(permissionData);
    });
    jQuery(".smpush-push-subscriptions-button").click(function() {
      var permissionData = window.safari.pushNotification.permission("'.self::$apisetting['safari_web_id'].'");
      checkRemotePermission(permissionData);
    });
    
    if("'.self::$apisetting['desktop_request_type'].'" == "native"){
      document.getElementsByClassName("smpush-push-permission-button")[0].click();
    }
}

var checkRemotePermission = function (permissionData) {
  if (permissionData.permission === "default") {
    window.safari.pushNotification.requestPermission(
        "'.get_bloginfo('url') .'/'.self::$apisetting['push_basename'].'/safari",
        "'.self::$apisetting['safari_web_id'].'",
        {},
        checkRemotePermission
    );
  }
  else if (permissionData.permission === "denied") {
    smpush_endpoint_unsubscribe(smpush_getCookie("smpush_safari_device_token"));
    smpush_setCookie("smpush_desktop_request", "true", 10);
    smpush_setCookie("smpush_safari_device_token", "false", -1);
    smpush_setCookie("smpush_device_token", "false", -1);
    smpushDrawNotifyPopup();
  }
  else if (permissionData.permission === "granted") {
    smpushDestroyReqWindow();
    if(smpush_getCookie("smpush_safari_device_token") != ""){
      smpush_endpoint_unsubscribe(smpush_getCookie("smpush_safari_device_token"));
      smpush_setCookie("smpush_desktop_request", "true", 10);
      smpush_setCookie("smpush_safari_device_token", "false", -1);
      smpush_setCookie("smpush_device_token", "false", -1);
      pushButton.attr("disabled","disabled");
      jQuery(".smpush-push-subscriptions-button").attr("disabled","disabled");
      jQuery(".smpush-push-subscriptions-button").html("'.self::$apisetting['desktop_modal_saved_text'].'");
    }
    else{
      smpush_setCookie("smpush_safari_device_token", permissionData.deviceToken, 365);
      smpush_endpoint_subscribe(permissionData.deviceToken);
      pushButton.attr("disabled","disabled");
      jQuery(".smpush-push-subscriptions-button").attr("disabled","disabled");
      jQuery(".smpush-push-subscriptions-button").html("'.self::$apisetting['desktop_modal_saved_text'].'");
    }
  }
};

';
    $output .= self::bootstrap();
    echo preg_replace('/\s+/', ' ', $output);
  }
  
  private static function bootstrap() {
    
    switch(self::$apisetting['desktop_popup_position']):
      case 'center':
        $popup_pos = '["center", "middle"]';
        break;
      case 'topcenter':
        $popup_pos = '["center", "top"]';
        break;
      case 'topright':
        $popup_pos = '["right - 20", "top + 20"]';
        break;
      case 'topleft':
        $popup_pos = '["left + 20", "top + 20"]';
        break;
      case 'bottomright':
        $popup_pos = '["right - 20", "bottom - 20"]';
        break;
      case 'bottomleft':
        $popup_pos = '["left + 20", "bottom - 20"]';
        break;
    endswitch;
    
    switch(self::$apisetting['desktop_icon_position']):
      case 'topright':
        $icon_tooltip_pos = 'left';
        $icon_pos = 'top: 10px; right: 10px;';
        break;
      case 'topleft':
        $icon_tooltip_pos = 'right';
        $icon_pos = 'top: 10px; left: 10px;';
        break;
      case 'bottomright':
        $icon_tooltip_pos = 'left';
        $icon_pos = 'bottom: 10px; right: 10px;';
        break;
      case 'bottomleft':
        $icon_tooltip_pos = 'right';
        $icon_pos = 'bottom: 10px; left: 10px;';
        break;
    endswitch;
    
    return '

function smpush_debug(object) {
  if('.self::$apisetting['desktop_debug'].' == 1){
    console.log(object);
  }
}

function smpush_endpoint_subscribe(subscriptionId) {
  if(subscriptionId == ""){
    return false;
  }
  smpush_setCookie("smpush_desktop_request", "true", 365);
  smpush_setCookie("smpush_device_token", subscriptionId, 365);
  
  var data = {};
  data["device_token"] = subscriptionId;
  data["device_type"] = devicetype;
  data["active"] = 1;
  data["user_id"] = '.get_current_user_id().';
  data["latitude"] = (smpush_getCookie("smart_push_smio_coords_latitude") != "")? smpush_getCookie("smart_push_smio_coords_latitude") : "";
  data["longitude"] = (smpush_getCookie("smart_push_smio_coords_longitude") != "")? smpush_getCookie("smart_push_smio_coords_longitude") : "";
  
  var subsChannels = [];
  jQuery("input.smpush_desktop_channels_subs:checked").each(function(index) {
    subsChannels.push(jQuery(this).val());
  });
  subsChannels = subsChannels.join(",");
  
  if(jQuery(".smpush-push-subscriptions-button").length > 0){
    var apiService = "channels_subscribe";
    data["channels_id"] = subsChannels;
  }
  else{
    var apiService = "savetoken";
  }
  
  smpushDestroyReqWindow();
  
  jQuery.ajax({
    method: "POST",
    url: "'.get_bloginfo('url') .'/index.php?smpushcontrol="+apiService,
    data: data
  })
  .done(function( msg ) {
    smpushWelcomeMSG();
    jQuery(".smpush-push-subscriptions-button").attr("disabled","disabled");
    jQuery(".smpush-push-subscriptions-button").html("'.self::$apisetting['desktop_modal_saved_text'].'");
    smpush_debug("Data Sent");
    if('.self::$apisetting['desktop_gps_status'].' == 1){
      smpushUpdateGPS();
    }
    smpush_link_user_cookies();
  });
}

function smpush_endpoint_unsubscribe(subscriptionId) {
  jQuery.ajax({
    method: "POST",
    url: "'.get_bloginfo('url') .'/index.php?smpushcontrol=deletetoken",
    data: { device_token: subscriptionId, device_type: devicetype}
  })
  .done(function( msg ) {
    smpush_debug("Data Sent");
    smpush_setCookie("smpush_linked_user", "false", -1);
    smpush_setCookie("smpush_safari_device_token", "false", -1);
    smpush_setCookie("smpush_device_token", "false", -1);
    smpush_setCookie("smpush_desktop_request", "false", -1);
  });
}

function smpush_bootstrap_init(){
  var pushSupported = false;
  
  if("safari" in window && "pushNotification" in window.safari){
    pushSupported = true;
  }
  if (typeof(ServiceWorkerRegistration) != "undefined" && ("showNotification" in ServiceWorkerRegistration.prototype)) {
    pushSupported = true;
  }
  
  if(! pushSupported || Notification.permission === "denied"){
    smpushDrawUnSupportedPopup();
    smpush_debug("Browser not support push notification");
    return;
  }
  
  if(smpush_getCookie("smpush_desktop_request") != "true"){
    jQuery("body").append("<style>'.str_replace('"', '\'', self::$apisetting['desktop_popup_css']).'</style>");
    setTimeout(function(){ smpushDrawReqWindow() }, '.(self::$apisetting['desktop_delay']*1000).');
  }
  else{
    smpush_link_user_cookies();
    if('.self::$apisetting['desktop_gps_status'].' == 1){
      smpushUpdateGPS();
    }
    var pushButton = jQuery(".smpush-push-permission-button");
    smpush_isPushEnabled = true;
    pushButton.html("'.addslashes(self::$apisetting['desktop_btn_unsubs_text']).'");
    pushButton.removeAttr("disabled");
    jQuery(".smpush-push-subscriptions-button").removeAttr("disabled");
    jQuery(".smpush-push-subscriptions-button").click(function() {
      smpush_endpoint_subscribe(smpush_getCookie("smpush_device_token"));
    });
    pushButton.click(function() {
      smpush_endpoint_unsubscribe(smpush_getCookie("smpush_device_token"));
      jQuery(".smpush-push-permission-button").remove();
      jQuery(".smpush-push-subscriptions-button").remove();
    });
  }
  
}

function smpushUpdateGPS(){
  if(smpush_getCookie("smpush_device_token") != "" && smpush_getCookie("smart_push_smio_coords_latitude") == ""){
    if (! navigator.geolocation) {
      smpush_debug("Geolocation is not supported for this Browser/OS.");
      return;
    }
    var geoSuccess = function(startPos) {
      smpush_debug(startPos.coords.latitude);
      smpush_debug(startPos.coords.longitude);
      smpush_setCookie("smart_push_smio_coords_latitude", startPos.coords.latitude, (1/24));
      smpush_setCookie("smart_push_smio_coords_longitude", startPos.coords.longitude, (1/24));
      
      smpush_endpoint_subscribe(smpush_getCookie("smpush_device_token"));
    };
    var geoError = function(error) {
      smpush_debug("Error occurred. Error code: " + error.code);
      /*0: unknown error, 1: permission denied, 2: position unavailable (error response from location provider), 3: timed out*/
    };
    navigator.geolocation.getCurrentPosition(geoSuccess);
  }
}

function smpushDestroyReqWindow(){
  jQuery("#smart_push_smio_window").remove();
  jQuery("#smart_push_smio_overlay").remove();
  jQuery(".smpushTooltipPara").remove();
  jQuery("#smpushIconRequest").remove();
  smpush_setCookie("smpush_desktop_request", "true", '.((empty(self::$apisetting['desktop_reqagain']))? 1: self::$apisetting['desktop_reqagain']).');
  if("'.self::$apisetting['desktop_paytoread'].'" == "2" && jQuery("#SMIOPayToReadButton").length > 0 && smpush_getCookie("smpush_device_token") != ""){
    location.reload();
  }
}

function smpushDrawNotifyPopup(){
  if("'.self::$apisetting['desktop_paytoread'].'" != "1")return;
  jQuery("#smart_push_smio_window").remove();
  jQuery("#smart_push_smio_overlay").remove();
  
  jQuery("body").append(\''.self::buildPopupLayout(true).'\');
  document.getElementById("smart_push_smio_overlay").style.opacity = "'.((empty(self::$apisetting['desktop_paytoread_darkness']))? 0.8:(self::$apisetting['desktop_paytoread_darkness']/10) ).'";
  document.getElementById("smart_push_smio_window").style.position = "fixed";
  document.getElementById("smart_push_smio_overlay").style.display = "block";
  document.getElementById("smart_push_smio_window").style.display = "block";

  document.getElementById("smart_push_smio_window").style.left = ((window.innerWidth/2) - (document.getElementById("smart_push_smio_window").offsetWidth/2)) + "px";
  document.getElementById("smart_push_smio_window").style.top = ((window.innerHeight/2) - (document.getElementById("smart_push_smio_window").offsetHeight/2)) + "px";
}

function smpushDrawUnSupportedPopup(){
  if("'.self::$apisetting['desktop_paytoread'].'" != "1")return;
  jQuery("#smart_push_smio_window").remove();
  jQuery("#smart_push_smio_overlay").remove();
  
  jQuery("body").append(\''.self::buildPopupLayout(self::$apisetting['desktop_notsupport_msg']).'\');
  document.getElementById("smart_push_smio_overlay").style.opacity = "'.((empty(self::$apisetting['desktop_paytoread_darkness']))? 0.8:(self::$apisetting['desktop_paytoread_darkness']/10) ).'";
  document.getElementById("smart_push_smio_window").style.position = "fixed";
  document.getElementById("smart_push_smio_overlay").style.display = "block";
  document.getElementById("smart_push_smio_window").style.display = "block";

  document.getElementById("smart_push_smio_window").style.left = ((window.innerWidth/2) - (document.getElementById("smart_push_smio_window").offsetWidth/2)) + "px";
  document.getElementById("smart_push_smio_window").style.top = ((window.innerHeight/2) - (document.getElementById("smart_push_smio_window").offsetHeight/2)) + "px";
  
  document.getElementById("smart_push_smio_allow").style.display = "none";
}

function smpushDrawReqWindow(){
  if("'.self::$apisetting['desktop_request_type'].'" == "popup"){
    jQuery("body").append(\''.self::buildPopupLayout().'\');
    document.getElementById("smart_push_smio_overlay").style.opacity = "'.((empty(self::$apisetting['desktop_paytoread_darkness']))? 0.8:(self::$apisetting['desktop_paytoread_darkness']/10) ).'";
    document.getElementById("smart_push_smio_window").style.position = "fixed";
    document.getElementById("smart_push_smio_overlay").style.display = "block";
    document.getElementById("smart_push_smio_window").style.display = "block";
    
    var position = "'.self::$apisetting['desktop_popup_position'].'";

    if(position == "topright"){
      document.getElementById("smart_push_smio_window").style.right = "10px";
      document.getElementById("smart_push_smio_window").style.top = "10px";
    }
    else if(position == "topleft"){
      document.getElementById("smart_push_smio_window").style.left = "10px";
      document.getElementById("smart_push_smio_window").style.top = "10px";
    }
    else if(position == "bottomright"){
      document.getElementById("smart_push_smio_window").style.bottom = "10px";
      document.getElementById("smart_push_smio_window").style.right = "10px";
    }
    else if(position == "bottomleft"){
      document.getElementById("smart_push_smio_window").style.left = "10px";
      document.getElementById("smart_push_smio_window").style.bottom = "10px";
    }
    else if(position == "topcenter"){
      document.getElementById("smart_push_smio_window").style.left = ((window.innerWidth/2) - (document.getElementById("smart_push_smio_window").offsetWidth/2)) + "px";
      document.getElementById("smart_push_smio_window").style.top = "0";
    }
    else{
      document.getElementById("smart_push_smio_window").style.left = ((window.innerWidth/2) - (document.getElementById("smart_push_smio_window").offsetWidth/2)) + "px";
      document.getElementById("smart_push_smio_window").style.top = ((window.innerHeight/2) - (document.getElementById("smart_push_smio_window").offsetHeight/2)) + "px";
    }
  }
  else if("'.self::$apisetting['desktop_request_type'].'" == "icon"){
    if("'.self::$apisetting['desktop_paytoread'].'" == "1"){
      jQuery("body").append(\'<div id="smart_push_smio_overlay" tabindex="-1" style="opacity:'.((empty(self::$apisetting['desktop_paytoread_darkness']))? 0.8:(self::$apisetting['desktop_paytoread_darkness']/10) ).'; display: block;ms-filter:progid:DXImageTransform.Microsoft.Alpha(Opacity=40); background-color:#000; position: fixed; left: 0; right: 0; top: 0; bottom: 0; z-index: 10000;"></div>\');
    }
    jQuery("body").append("<style>#smpushIconRequest{display: block;position: fixed;width: 50px;height: 50px;background: url('.smpush_imgpath.'/alert.png) no-repeat;text-indent: -9999px;padding: 0;margin: 0;border: 0;z-index: 999999999;border-radius: 50px;-webkit-border-radius: 50px;-moz-border-radius: 50px;-webkit-box-shadow: 7px 3px 16px 0px rgba(50, 50, 50, 0.2);-moz-box-shadow:    7px 3px 16px 0px rgba(50, 50, 50, 0.2);box-shadow:         7px 3px 16px 0px rgba(50, 50, 50, 0.2);}.smpushTooltipPara {display:none;position:absolute;border:1px solid #333;background-color:#161616;border-radius:5px;padding:5px;color:#fff;font:bold 12px Arial;z-index: 999999999;margin: 0;border: 0;}</style>");
    jQuery("body").append("<button class=\"smpush-push-permission-button smpushTooltip\" id=\"smpushIconRequest\" style=\"'.$icon_pos.'\" tooltip-side=\"'.$icon_tooltip_pos.'\" title=\"'.addslashes(self::$apisetting['desktop_icon_message']).'\" disabled></button>");
  }
  else{
    if("'.self::$apisetting['desktop_paytoread'].'" == "1"){
      jQuery("body").append(\'<div id="smart_push_smio_overlay" tabindex="-1" style="opacity:'.((empty(self::$apisetting['desktop_paytoread_darkness']))? 0.8:(self::$apisetting['desktop_paytoread_darkness']/10) ).'; display: block;ms-filter:progid:DXImageTransform.Microsoft.Alpha(Opacity=40); background-color:#000; position: fixed; left: 0; right: 0; top: 0; bottom: 0; z-index: 10000;"></div>\');
    }
    jQuery("body").append("<button class=\"smpush-push-permission-button\" style=\"display:none\" disabled>'.addslashes(self::$apisetting['desktop_btn_subs_text']).'</button>");
  }
  smpushTooltip();
  smpushRegisterServiceWorker();
}

function smpush_link_user_cookies() {
  if(smpush_getCookie("smpush_linked_user") == "" && smpush_getCookie("smpush_device_token") != ""){
    if('.get_current_user_id().' > 0){
      smpush_endpoint_subscribe(smpush_getCookie("smpush_device_token"));
      smpush_setCookie("smpush_linked_user", "true", 30);
    }
  }
}

function smpushWelcomeMSG(){
  if("'.self::$apisetting['desktop_welc_status'].'" == "0"){return;}
  if(smpush_getCookie("smpush_desktop_welcmsg_seen") == "true"){
    return;
  }
  smpush_setCookie("smpush_desktop_welcmsg_seen", "true", 365);
  if("safari" in window){
    var n = new Notification(
      "'.addslashes(self::$apisetting['desktop_welc_title']).'",
      {
        "body": "'.addslashes(self::$apisetting['desktop_welc_message']).'",
        "tag" : "'.self::$apisetting['desktop_welc_link'].'"
      }
    );
    n.onclick = function () {
      this.close();
      window.open("'.self::$apisetting['desktop_welc_link'].'", "_blank");
    };
  }
  else{
    navigator.serviceWorker.ready.then(function(registration) {
      registration.showNotification("'.addslashes(self::$apisetting['desktop_welc_title']).'", {
        icon: "'.self::$apisetting['desktop_welc_icon'].'",
        body: "'.addslashes(self::$apisetting['desktop_welc_message']).'",
        tag: "'.addslashes(self::$apisetting['desktop_welc_link']).'",
        requireInteraction: true
      });
    });
    self.addEventListener("notificationclick", function (event) {
      event.notification.close();
      event.waitUntil(clients.matchAll({
        type: "window"
      }).then(function (clientList) {
        for (var i = 0; i < clientList.length; i++) {
          var client = clientList[i];
          if (client.url === event.notification.tag && "focus" in client) {
            return client.focus();
          }
        }
        if (clients.openWindow) {
          return clients.openWindow(event.notification.tag);
        }
      }));
    });
  }
}

function smpush_setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires + ";path='.COOKIEPATH.'";
}

function smpush_getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(";");
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==" "){
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}

function smpushTooltip() {
  jQuery(".smpushTooltip").mouseenter(function(e){
    var title = jQuery(this).attr("title");
    var side = jQuery(this).attr("tooltip-side");
    jQuery(this).data("tipText", title).removeAttr("title");
    jQuery("<p class=\"smpushTooltipPara\"></p>").text(title).appendTo("body");
    if(window.innerWidth > jQuery(".smpushTooltipPara").width()+80){
      var width = "auto";
    }
    else{
      var width = (window.innerWidth-150)+"px";
    }
    jQuery(".smpushTooltipPara").attr("style", "width:"+width);
    if(side == "right"){
        var mousex = jQuery(this).offset().left + 55;
    }
    else{
        var mousex = jQuery(this).offset().left - (jQuery(".smpushTooltipPara").width()+20);
    }
    var mousey = jQuery(this).offset().top + 5;
    jQuery(".smpushTooltipPara").attr("style", "display: block; top: "+mousey+"px; left: "+mousex+"px;width:"+width);
  }).mouseleave(function() {
    jQuery(this).attr("title", jQuery(this).data("tipText"));
    jQuery(".smpushTooltipPara").remove();
  });
}

if ("safari" in window && "pushNotification" in window.safari) {
  smpush_bootstrap_init();
}

function openFBpopup(url, elm){
  var new_fbwindow = window.open(url, "", "width=800,height=600");
  new_fbwindow.onbeforeunload = function(){ $(elm).hide(); }
}

';
  }
  
  private static function chrome($type) {
    $output = '
"use strict";

var smpush_isPushEnabled = false;
var devicetype = "'.$type.'";

document.getElementsByTagName("HEAD")[0].insertAdjacentHTML("afterbegin", "<link rel=\"manifest\" href=\"'.get_bloginfo('url') .'/?smpushprofile=manifest\">");

function smpush_endpointWorkaround(endpoint){
	var device_id = "";
	if(endpoint.indexOf("mozilla") > -1){
        device_id = endpoint.split("/")[endpoint.split("/").length-1]; 
    }
	else if(endpoint.indexOf("send/") > -1){
		device_id = endpoint.slice(endpoint.search("send/")+5);
	}
    else{
      smpush_debug(endpoint);
      smpush_debug("error while getting device_id from endpoint");
      alert("error while getting device_id from endpoint");
      window.close();
    }
    smpush_debug(device_id);
	return device_id;
}

function smpush_sendSubscriptionToServer(subscription) {
  var subscriptionId = smpush_endpointWorkaround(subscription.endpoint);
  smpush_debug(subscriptionId);
  smpush_endpoint_subscribe(subscriptionId);
}

function smpush_unsubscribe() {
  smpush_setCookie("smpush_desktop_request", "true", 10);
  var pushButton = jQuery(".smpush-push-permission-button");
  pushButton.attr("disabled","disabled");

  navigator.serviceWorker.ready.then(function(serviceWorkerRegistration) {
    serviceWorkerRegistration.pushManager.getSubscription().then(
      function(pushSubscription) {
        if (!pushSubscription) {
          smpush_isPushEnabled = false;
          pushButton.removeAttr("disabled");
          pushButton.html("'.addslashes(self::$apisetting['desktop_btn_subs_text']).'");
          return;
        }
        
        var subscriptionId = smpush_endpointWorkaround(pushSubscription.endpoint);
        smpush_debug(subscriptionId);
        smpush_endpoint_unsubscribe(subscriptionId);

        pushSubscription.unsubscribe().then(function() {
          pushButton.removeAttr("disabled");
          pushButton.html("'.addslashes(self::$apisetting['desktop_btn_subs_text']).'");
          smpush_isPushEnabled = false;
        }).catch(function(e) {
          smpush_debug("Unsubscription error: ", e);
          pushButton.removeAttr("disabled");
        });
      }).catch(function(e) {
        smpush_debug("Error thrown while unsubscribing from push messaging.", e);
      });
  });
}

function smpush_subscribe() {
  var pushButton = jQuery(".smpush-push-permission-button");
  pushButton.attr("disabled","disabled");

  navigator.serviceWorker.ready.then(function(serviceWorkerRegistration) {
    serviceWorkerRegistration.pushManager.subscribe({userVisibleOnly: true})
      .then(function(subscription) {
        smpush_isPushEnabled = true;
        pushButton.html("'.addslashes(self::$apisetting['desktop_btn_unsubs_text']).'");
        pushButton.removeAttr("disabled");
        return smpush_sendSubscriptionToServer(subscription);
      })
      .catch(function(e) {
        if (Notification.permission === "denied") {
          smpushDrawNotifyPopup();
          smpush_debug("Permission for Notifications was denied");
          pushButton.attr("disabled","disabled");
          smpush_endpoint_unsubscribe(smpush_getCookie("smpush_device_token"));
        } else {
          smpush_debug(e);
          if(smpush_getCookie("smart_push_smio_allow_before") == ""){
            smpush_setCookie("smart_push_smio_allow_before", "true", 1);
            smpush_subscribe();
          }
          pushButton.html("'.addslashes(self::$apisetting['desktop_btn_subs_text']).'");
          pushButton.removeAttr("disabled");
        }
      });
  });
}

function smpush_initialiseState() {
  if (!("showNotification" in ServiceWorkerRegistration.prototype)) {
    smpush_debug("Notifications aren\'t supported.");
    return;
  }

  if (Notification.permission === "denied") {
    smpushDrawNotifyPopup();
    smpush_debug("The user has blocked notifications.");
    smpush_endpoint_unsubscribe(smpush_getCookie("smpush_device_token"));
    return;
  }

  if (!("PushManager" in window)) {
    smpush_debug("Push messaging isn\'t supported.");
    return;
  }

  navigator.serviceWorker.ready.then(function(serviceWorkerRegistration) {
    serviceWorkerRegistration.pushManager.getSubscription()
      .then(function(subscription) {
        var pushButton = jQuery(".smpush-push-permission-button");
        pushButton.removeAttr("disabled");

        if (!subscription) {
          if("'.self::$apisetting['desktop_request_type'].'" == "native"){
            document.getElementsByClassName("smpush-push-permission-button")[0].click();
          }
          return;
        }

        pushButton.html("'.addslashes(self::$apisetting['desktop_btn_unsubs_text']).'");
        smpush_isPushEnabled = true;
        smpush_sendSubscriptionToServer(subscription);
      })
      .catch(function(err) {
        smpush_debug("Error during getSubscription()", err);
      });
  });
}

window.addEventListener("load", function() {
  smpush_bootstrap_init();
});

function smpushRegisterServiceWorker(){
  if ("serviceWorker" in navigator) {
    navigator.serviceWorker.register("'.get_bloginfo('url') .'/?smpushprofile=service_worker&platform="+devicetype).then(smpush_initialiseState);
  } else {
    smpush_debug("Service workers aren\'t supported in this browser.");
  }
  
  if(jQuery(".smpush-push-permission-button").length < 1){
    return false;
  }
  
  var pushButton = jQuery(".smpush-push-permission-button");

  pushButton.click(function() {
    if (smpush_isPushEnabled) {
      smpush_unsubscribe();
    } else {
      smpush_subscribe();
    }
  });
  
  jQuery(".smpush-push-subscriptions-button").click(function() {
    smpush_subscribe();
  });
}

';
    $output .= self::bootstrap();
    echo preg_replace('/\s+/', ' ', $output);
  }
  
  public static function start_all_lisenter() {
    include(smpush_dir.'/class.browser.detect.php');
    $detector = new BrowserDetection();
    $browser = strtolower($detector->getName());

    switch ($browser){
      case 'chrome':
        if(self::$apisetting['desktop_status'] == 1 && self::$apisetting['desktop_chrome_status'] == 1){
          self::chrome('chrome');
        }
        break;
      case 'firefox':
        if(self::$apisetting['desktop_status'] == 1 && self::$apisetting['desktop_firefox_status'] == 1){
          self::chrome('firefox');
        }
        break;
      case 'opera':
        if(self::$apisetting['desktop_status'] == 1 && self::$apisetting['desktop_opera_status'] == 1){
          self::chrome('opera');
        }
        break;
      case 'samsung':
        if(self::$apisetting['desktop_status'] == 1 && self::$apisetting['desktop_samsung_status'] == 1){
          self::chrome('samsung');
        }
        break;
      case 'safari':
        if(self::$apisetting['desktop_status'] == 1 && self::$apisetting['desktop_safari_status'] == 1){
          self::safari();
        }
        break;
    }
  }
  
  private static function buildPopupLayout($second_msg=false){
    if($second_msg === true){
      $second_msg = str_replace('\'', '`', htmlspecialchars(nl2br(self::$apisetting['desktop_paytoread_message'])));
    }
    elseif($second_msg === false){
      $second_msg = str_replace('\'', '`', htmlspecialchars(nl2br(self::$apisetting['desktop_modal_message'])));
    }
    else{
      $second_msg = str_replace('\'', '`', htmlspecialchars(nl2br($second_msg)));
    }
    $html = '<style>';
    if(empty(self::$apisetting['desktop_popup_layout']) || self::$apisetting['desktop_popup_layout'] == 'modern'){
      $html .= '
#smart_push_smio_window{
direction:ltr;display: none;width:600px;max-width: 87%;background-color: white; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; padding: 17px; border-radius: 5px; text-align: center; overflow: hidden; z-index: 99999999;
}
#smart_push_smio_logo{
border-radius:50%;max-width:150px;max-height:150px;width:50%;height:50%;
}
#smart_push_smio_msg{
margin-top: 23px;color: #797979; font-size: 18px; text-align: center; font-weight: 300;padding: 0;line-height: normal;
}
#smart_push_smio_note{
color: #797979; font-size: 15px; text-align: center; font-weight: 300; position: relative; float: none; margin: 16px 0; padding: 0; line-height: normal;
}
#smart_push_smio_footer{
text-align: center;
}
#smart_push_smio_not_allow{
background-color: #9E9E9E;text-transform: none; color: white; border: none; box-shadow: none; font-size: 17px; font-weight: 500; -webkit-border-radius: 4px; border-radius: 5px; padding: 10px 32px; margin: 5px; cursor: pointer;
}
#smart_push_smio_allow{
background-color: #8BC34A;text-transform: none; color: white; border: none; box-shadow: none; font-size: 17px; font-weight: 500; -webkit-border-radius: 4px; border-radius: 5px; padding: 10px 32px; margin: 5px ; cursor: pointer;
}
';
    }
    elseif(self::$apisetting['desktop_popup_layout'] == 'native'){
      $html .= '
#smart_push_smio_window{
direction:ltr;display:none;max-width: 87%;z-index:99999999;font-family: Helvetica Neue, Helvetica, Arial, sans-serif;text-align:left;margin-top: 5px;border: 1px solid rgb(170, 170, 170);background: rgb(251, 251, 251);width: 320px;font-size: 13px;padding: 12px 12px 12px 6px;border-radius: 2px;box-shadow: rgba(0, 0, 0, 0.298039) 0px 2px 1px 0px;
}
#smart_push_smio_window:after {
bottom: 100%;left: 20%;border: solid transparent;content: " ";height: 0;width: 0;position: absolute;pointer-events: none;border-color: rgba(255, 255, 255, 0);border-bottom-color: #fff;border-width: 10px;margin-left: -10px;
}
#smart_push_smio_close{
position: absolute;right: 5px;top: 2px;background: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAQCAIAAABGNLJTAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAABpSURBVChTzZBLCsAgDER7Zg9iCDmqSzd9MCJtbSvunEXI540kHnVFm9LuHhGlFJUklDRVohvNLKWUc4ZDJJQ02/hBQ5iZDEKJNNt43LsbRhS90Hp1TneU2Fe6Gv6ulOHzyrUfnGoXutYTA3eKL8daaukAAAAASUVORK5CYII=");width: 12px;height: 13px;cursor: pointer;
}
#smart_push_smio_logo{
border:0;float:left;width:24px;height:24px;
}
#smart_push_smio_msg{
margin:2px 0 20px 30px;width: 100%;font-weight: 300;
}
#smart_push_smio_note{
display:none;
}
#smart_push_smio_footer{
text-align: right;
}
#smart_push_smio_not_allow{
display: inline-block;width: 80px;border-radius: 1px;border: 1px solid rgb(170, 170, 170);box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 1px 0px;text-align: center;padding: 4px 2px 5px;cursor: pointer;background: #fff;text-transform: none;color:#000;font-weight:300;
}
#smart_push_smio_allow{
display: inline-block;width: 80px;border-radius: 1px;border: 1px solid rgb(170, 170, 170);box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 1px 0px;text-align: center;padding: 4px 2px 5px;cursor: pointer;background: #fff;text-transform: none;color:#000;font-weight:300;
}
      ';
    }
    elseif(self::$apisetting['desktop_popup_layout'] == 'flat'){
      $html .= '
#smart_push_smio_window{
direction:ltr;display: none;z-index: 99999999;max-width:87%;width: 500px;margin: 0 auto;box-shadow: 0 0 20px 3px rgba(0,0,0,.22)!important;background: #fff!important;padding: 1.286em;border-bottom-left-radius: 2px;border-bottom-right-radius: 2px;font-family: Roboto,Noto,Helvetica Neue,Helvetica,Arial,sans-serif;
}
#smart_push_smio_logo{
float: left;width:80px;height:80px;margin: 10px 0 0 10px;border:0;
}
#smart_push_smio_msg{
margin:0;margin-left:100px;padding:7px 10px;font-size: 1.143em;line-height:19px;cursor: default;color: #666!important;text-align:left;
}
#smart_push_smio_note{
margin:0;margin-left:100px;padding:7px 10px;font-size: 16px;line-height:19px;cursor: default;color: #666!important;text-align:left;
}
#smart_push_smio_footer{
text-align: right;margin-top: 35px;
}
#smart_push_smio_not_allow{
background: transparent;color: #4285f4!important;font-size: 1em;text-transform: uppercase;font-weight: 400;line-height: 1.5;text-align: center;white-space: nowrap;vertical-align: middle;cursor: pointer;letter-spacing: .05em;transition: background-color 75ms ease;border:0;margin:0 10px 0 0;padding-top:8px;
}
#smart_push_smio_allow{
box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 6px 0 rgba(0,0,0,.12);background: #4285f4!important;color: #fff!important;padding: .714em 2em;font-size: 1em;text-transform: uppercase;border-radius: 2px;font-weight: 400;line-height: 1.5;text-align: center;white-space: nowrap;vertical-align: middle;cursor: pointer;letter-spacing: .05em;transition: background-color 75ms ease;border: 1px solid transparent;margin:0 10px 0 0;
}
      ';
    }
    elseif(self::$apisetting['desktop_popup_layout'] == 'fancy'){
      $html .= '
#smart_push_smio_window{
direction:ltr;display:none;max-width:94%;z-index:99999999;font-family: Helvetica Neue, Helvetica, Arial, sans-serif;text-align:left;width:600px;height:200px;background:#fff;padding:0;border-radius: 15px;-webkit-border-radius: 15px;-moz-border-radius: 15px;
}
#smart_push_smio_logo{
float:left;width:200px;height:200px;margin: 0 10px 0 0;border:0;border-radius: 15px 0px 0px 15px;-moz-border-radius: 15px 0px 0px 15px;-webkit-border-radius: 15px 0px 0px 15px;
}
#smart_push_smio_msg{
margin:0;margin-left:205px;padding:20px 0 15px 0;font-size: 1.143em;line-height:19px;cursor: default;color: #ff5722;text-align:left;font-weight:700;
}
#smart_push_smio_note{
margin:0;margin-left:205px;padding:0 5px 10px 0;font-size: 15px;line-height:19px;cursor: default;color: #ff5722;text-align:left;
}
#smart_push_smio_footer{
text-align: center;margin-top: 30px;
}
#smart_push_smio_not_allow{
width:120px;text-transform: none;padding:10px;margin: 0 5px;border:0;border-radius: 5px;-webkit-border-radius: 5px;-moz-border-radius: 5px;background:#ff5722;color:#fff;font-weight:700;cursor:pointer;
}
#smart_push_smio_allow{
width:120px;text-transform: none;padding:10px;margin: 0 5px;border:0;border-radius: 5px;-webkit-border-radius: 5px;-moz-border-radius: 5px;background:#ff5722;color:#fff;font-weight:700;cursor:pointer;
}
#smart_push_smio_copyrights{
position: absolute;padding: 0;font-size: 11px;color: #ccc;left: 210px;bottom: 0;
}
@media (max-width: 450px) {
  #smart_push_smio_logo{
    width:100px;height:100px;
  }
  #smart_push_smio_msg{
    margin-left:105px;
  }
  #smart_push_smio_note{
    margin-left:105px;
  }
}
      ';
    }
    elseif(self::$apisetting['desktop_popup_layout'] == 'ocean'){
      $html .= '
#smart_push_smio_window{
direction:ltr;display:none;max-width:94%;z-index:99999999;font-family: Helvetica Neue, Helvetica, Arial, sans-serif;text-align:left;width:440px;background:#fff;padding:0;border: 1px solid #D0D0D0;border-radius: 0 0 4px 4px;-webkit-border-radius: 0 0 4px 4px;-moz-border-radius: 0 0 4px 4px;box-shadow: 1px 1px 2px #DCDCDC;
}
#smart_push_smio_logo{
float:left;width:74px;height:74px;margin:10px;border:0;
}
#smart_push_smio_msg{
font: 15px/17px open_sansbold,Arial,Helvetica,sans-serif;margin:0;margin-left:100px;padding:13px 0 5px 0;cursor:default;color:#4A4A4A;text-align:left;font-weight:bold;
}
#smart_push_smio_note{
font: 15px/17px open_sanslight,Arial,Helvetica,sans-serif;margin:0;margin-left:100px;padding:0 5px 10px 0;cursor:default;color:#4A4A4A;text-align:left;
}
#smart_push_smio_footer{
text-align: right;margin: 0px 15px 7px 0;
}
#smart_push_smio_not_allow{
width:115px;text-transform: none;padding:5px;margin: 0 5px;border:1px solid #ddd;border-radius: 3px;-webkit-border-radius: 3px;-moz-border-radius: 3px;background:#fff;color:#aaa;font-weight:300;cursor:pointer;font-size: 14px;
}
#smart_push_smio_allow{
width:115px;text-transform: none;padding:5px;margin: 0 5px;border:1px solid #aaa;border-radius: 3px;-webkit-border-radius: 3px;-moz-border-radius: 3px;background:#fff;color:#000;font-weight:300;cursor:pointer;font-size: 14px;
}
#smart_push_smio_not_allow:hover{background-color: #fff!important}
#smart_push_smio_allow:hover{background-color: #fff!important}
#smart_push_smio_copyrights{
position: absolute;padding: 0;font-size: 11px;color: #ccc;left: 210px;bottom: 0;
}
@media (max-width: 450px) {
  #smart_push_smio_logo{
    width:50px;height:50px;
  }
  #smart_push_smio_msg{
    margin-left:70px;
  }
  #smart_push_smio_note{
    margin-left:70px;
  }
}
      ';
    }
    elseif(self::$apisetting['desktop_popup_layout'] == 'dark'){
      $html .= '
#smart_push_smio_window{
direction:ltr;display: none;width:540px;max-width: 87%;background-color: #373737; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; padding: 12px 0; border-radius: 5px; text-align: left; overflow: hidden; z-index: 99999999;
}
#smart_push_smio_logo{
float:left;width:80px;height:80px;margin-left:10px;border:0;
}
#smart_push_smio_msg{
margin-left:100px;margin-top: 23px;color: #e1e1df; font-size: 18px; font-weight: 300;padding: 0 5px 0 0;line-height: normal;
}
#smart_push_smio_note{
color: #828284;font-size: 15px;font-weight: 300; position: relative;margin:56px 0 20px 0;padding:20px 5px;line-height: normal;border-top: solid 1px #464646;border-bottom: solid 1px #464646;text-align: center;
}
#smart_push_smio_footer{
text-align: center;
}
#smart_push_smio_not_allow{
background-color: #5f5f5f;text-transform: none; color: #929292; border: none; box-shadow: none; font-size: 17px; font-weight: 500; -webkit-border-radius: 20px; border-radius: 20px; padding: 10px 32px; margin: 5px; cursor: pointer;
}
#smart_push_smio_allow{
background-color: #5db166;text-transform: none; color: #fff; border: none; box-shadow: none; font-size: 17px; font-weight: 500; -webkit-border-radius: 20px; border-radius: 20px; padding: 10px 32px; margin: 5px ; cursor: pointer;
}
      ';
    }
    if(empty(self::$apisetting['desktop_popupicon'])){
      $logo = smpush_imgpath.'/megaphone.png';
    }
    else{
      $logo = self::$apisetting['desktop_popupicon'];
    }
    if(self::$apisetting['desktop_paytoread'] != 1){
      $unsubsBTN = '<button type="button" onclick="smpushDestroyReqWindow()" id="smart_push_smio_not_allow">'.addslashes(self::$apisetting['desktop_modal_cancel_text']).'</button>';
    }
    else{
      $unsubsBTN = '';
    }
    $html .= '
</style>
<div id="smart_push_smio_overlay" tabindex="-1" style="opacity:'.((empty(self::$apisetting['desktop_paytoread_darkness']))? 0.8:(self::$apisetting['desktop_paytoread_darkness']/10) ).'; display: none;ms-filter:progid:DXImageTransform.Microsoft.Alpha(Opacity=40); background-color:#000; position: fixed; left: 0; right: 0; top: 0; bottom: 0; z-index: 10000;"></div>
<div id="smart_push_smio_window">
  <div id="smart_push_smio_close" onclick="smpushDestroyReqWindow()" '.((self::$apisetting['desktop_popup_layout'] != 'native' || self::$apisetting['desktop_paytoread'] == 1)? 'style="display:none"': '').'></div>
  <img id="smart_push_smio_logo" src="'.$logo.'" />
  <p id="smart_push_smio_msg">'.addslashes(self::$apisetting['desktop_modal_title']).'</p>
  <p id="smart_push_smio_note">'.$second_msg.'</p>
  <div id="smart_push_smio_footer">
    '.$unsubsBTN.'
    <button type="button" class="smpush-push-permission-button" id="smart_push_smio_allow" disabled>'.addslashes(self::$apisetting['desktop_btn_subs_text']).'</button> 
  </div>
</div>
    ';
    return $html;
  }
  
  public static function messengerOfficialWidget(){
    echo '<div class="fb-customerchat" minimized="true" page_id="'.self::$apisetting['msn_official_fbpage_id'].'"></div>';
  }
  
  public static function messengerCustomWidget(){
      $html = '<style>.smpush-fb-livechat,.smpush-fb-widget{display:none}.smpush-ctrlq.smpush-fb-button{position:fixed;right:26px;cursor:pointer}.smpush-ctrlq.smpush-fb-close{position:absolute;right:3px;cursor:pointer}.smpush-ctrlq.smpush-fb-button{z-index:99;background:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDEyOCAxMjgiIGhlaWdodD0iMTI4cHgiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCAxMjggMTI4IiB3aWR0aD0iMTI4cHgiIHhtbDpzcGFjZT0icHJlc2VydmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxnPjxyZWN0IGZpbGw9IiMwMDg0RkYiIGhlaWdodD0iMTI4IiB3aWR0aD0iMTI4Ii8+PC9nPjxwYXRoIGQ9Ik02NCwxNy41MzFjLTI1LjQwNSwwLTQ2LDE5LjI1OS00Niw0My4wMTVjMCwxMy41MTUsNi42NjUsMjUuNTc0LDE3LjA4OSwzMy40NnYxNi40NjIgIGwxNS42OTgtOC43MDdjNC4xODYsMS4xNzEsOC42MjEsMS44LDEzLjIxMywxLjhjMjUuNDA1LDAsNDYtMTkuMjU4LDQ2LTQzLjAxNUMxMTAsMzYuNzksODkuNDA1LDE3LjUzMSw2NCwxNy41MzF6IE02OC44NDUsNzUuMjE0ICBMNTYuOTQ3LDYyLjg1NUwzNC4wMzUsNzUuNTI0bDI1LjEyLTI2LjY1N2wxMS44OTgsMTIuMzU5bDIyLjkxLTEyLjY3TDY4Ljg0NSw3NS4yMTR6IiBmaWxsPSIjRkZGRkZGIiBpZD0iQnViYmxlX1NoYXBlIi8+PC9zdmc+) center no-repeat #0084ff;width:60px;height:60px;text-align:center;bottom:24px;border:0;outline:0;border-radius:60px;-webkit-border-radius:60px;-moz-border-radius:60px;-ms-border-radius:60px;-o-border-radius:60px;box-shadow:0 1px 6px rgba(0,0,0,.06),0 2px 32px rgba(0,0,0,.16);-webkit-transition:box-shadow .2s ease;background-size:80%;transition:all .2s ease-in-out}.smpush-ctrlq.smpush-fb-button:focus,.smpush-ctrlq.smpush-fb-button:hover{transform:scale(1.1);box-shadow:0 2px 8px rgba(0,0,0,.09),0 4px 40px rgba(0,0,0,.24)}.smpush-fb-widget{background:#fff;z-index:100;position:fixed;width:360px;height:400px;overflow:hidden;opacity:0;bottom:0;right:24px;border-radius:6px;-o-border-radius:6px;-webkit-border-radius:6px;box-shadow:0 5px 40px rgba(0,0,0,.16);-webkit-box-shadow:0 5px 40px rgba(0,0,0,.16);-moz-box-shadow:0 5px 40px rgba(0,0,0,.16);-o-box-shadow:0 5px 40px rgba(0,0,0,.16)}.fb-credit{text-align:center;margin-top:8px}.fb-credit a{transition:none;color:#bec2c9;font-family:Helvetica,Arial,sans-serif;font-size:12px;text-decoration:none;border:0;font-weight:400}.smpush-ctrlq.smpush-fb-overlay{z-index:98;position:fixed;height:100vh;width:100vw;-webkit-transition:opacity .4s,visibility .4s;transition:opacity .4s,visibility .4s;top:0;left:0;background:rgba(0,0,0,.05);display:none}.smpush-ctrlq.smpush-fb-close{z-index:4;padding:0 6px;background:#365899;font-weight:700;font-size:11px;color:#fff;margin:8px;border-radius:3px}.smpush-ctrlq.smpush-fb-close::after{content:"x";font-family:sans-serif}</style>
<div class="smpush-fb-livechat">
  <div class="smpush-ctrlq smpush-fb-overlay"></div>
  <div class="smpush-fb-widget">
    <div class="smpush-ctrlq smpush-fb-close"></div>
    <div class="fb-page" data-href="'.self::$apisetting['msn_fbpage_link'].'" data-tabs="messages" data-width="360" data-height="400" data-small-header="true" data-hide-cover="true" data-show-facepile="false">
      <blockquote cite="'.self::$apisetting['msn_fbpage_link'].'" class="fb-xfbml-parse-ignore"> </blockquote>
    </div>
    <div id="fb-root"></div>
  </div>
  <a href="'.self::$apisetting['msn_fbpage_link'].'" title="'.self::$apisetting['msn_widget_title'].'" class="smpush-ctrlq smpush-fb-button"></a> 
</div>';
echo 'document.getElementsByTagName("BODY")[0].insertAdjacentHTML("beforeend", \''.preg_replace('/\s+/', ' ', $html).'\');
jQuery(document).ready(function(){var t={delay:125,overlay:jQuery(".smpush-fb-overlay"),widget:jQuery(".smpush-fb-widget"),button:jQuery(".smpush-fb-button")};setTimeout(function(){jQuery("div.smpush-fb-livechat").fadeIn()},8*t.delay),jQuery(".smpush-ctrlq").on("click",function(e){e.preventDefault(),t.overlay.is(":visible")?(t.overlay.fadeOut(t.delay),t.widget.stop().animate({bottom:0,opacity:0},2*t.delay,function(){jQuery(this).hide("slow"),t.button.show()})):t.button.fadeOut("medium",function(){t.widget.stop().show().animate({bottom:"30px",opacity:1},2*t.delay),t.overlay.fadeIn(t.delay)})})});
';
  }
  
}