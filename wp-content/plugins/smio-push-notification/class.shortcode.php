<?php

/*======================================================================*\
|| #################################################################### ||
|| # Push Notification System Wordpress Plugin                        # ||
|| # ---------------------------------------------------------------- # ||
|| # Copyright ©2014-2017 Smart IO Labs Inc. All Rights Reserved.     # ||
|| # This file may not be redistributed in whole or significant part. # ||
|| # --- Smart Push Notification System IS NOT FREE SOFTWARE ---      # ||
|| # https://smartiolabs.com/product/push-notification-system         # ||
|| #################################################################### ||
\*======================================================================*/

class smpush_shortcode extends smpush_controller {

  public function __construct() {
    parent::__construct();
  }
  
  public static function fbloign($args){
    include(smpush_dir.'/facebook/fbsdk.php');
    $facebook = new FacebookSDK(array(
      'appId' => (!empty(self::$apisetting['fbnotify_appid']))? self::$apisetting['fbnotify_appid'] : self::$apisetting['msn_appid'],
      'secret' => (!empty(self::$apisetting['fbnotify_secret']))? self::$apisetting['fbnotify_secret'] : self::$apisetting['msn_secret'],
      'cookie' => false
     ));
    $fbloginurl = $facebook->getLoginUrl($params = array('scope' => 'public_profile,email', 'redirect_uri' => get_bloginfo('url').'/'.self::$apisetting['push_basename'].'/facebook/?action=login'));
    
    $width = (empty($args['width']))? self::$apisetting['fblogin_btn_width'] : $args['width'];
    $height = (empty($args['height']))? self::$apisetting['fblogin_btn_height'] : $args['height'];
    $text = (empty($args['text']))? self::$apisetting['fblogin_btn_text'] : $args['text'];
    $color = (empty($args['color']))? self::$apisetting['fblogin_btn_color'] : $args['color'];
    $bgcolor = (empty($args['bgcolor']))? self::$apisetting['fblogin_btn_bgcolor'] : $args['bgcolor'];
    $icon = self::$apisetting['fblogin_btn_icon'];
    
    echo '<style>.smpush-fblogin-button img{border:0;margin-right: 5px}.smpush-fblogin-button{display: inline-block;color:'.$color.'!important;background-color:'.$bgcolor.';width:'.$width.'px;height:'.$height.'px;line-height:'.($height-3).'px;text-decoration: none!important;text-align:center;border:0;outline:0;border-radius:3px;-webkit-border-radius:3px;-moz-border-radius:3px;-ms-border-radius:3px;-o-border-radius:3px;box-shadow:0 1px 6px rgba(0,0,0,.06),0 2px 32px rgba(0,0,0,.16);-webkit-transition:box-shadow .2s ease;transition:all .2s ease-in-out}.smpush-fblogin-button:focus,.smpush-fblogin-button:hover{transform:scale(1.1);box-shadow:0 2px 8px rgba(0,0,0,.09),0 4px 40px rgba(0,0,0,.24)}</style>';
    echo '<a href="#" onclick="return smpushOpenFBpopup(\''.$fbloginurl.'\', this)" class="smpush-fblogin-button"><img src="'.$icon.'" /> '.$text.'</a>';
    echo '<script data-cfasync="false" type="text/javascript">function smpushOpenFBpopup(url, elm){var new_fbwindow = window.open(url, "", "width=800,height=600");var popupTick = setInterval(function() {if (new_fbwindow.closed) {clearInterval(popupTick);window.location="'.get_bloginfo('url').'";}}, 500);return false;}</script>';
  }
  
  public static function messenger($args){
    $width = (empty($args['width']))? self::$apisetting['msn_btn_width'] : $args['width'];
    $height = (empty($args['height']))? self::$apisetting['msn_btn_height'] : $args['height'];
    $text = (empty($args['text']))? self::$apisetting['msn_btn_text'] : $args['text'];
    $color = (empty($args['color']))? self::$apisetting['msn_btn_color'] : $args['color'];
    $bgcolor = (empty($args['bgcolor']))? self::$apisetting['msn_btn_bgcolor'] : $args['bgcolor'];
    $icon = self::$apisetting['msn_btn_icon'];
    
      echo '<style>.smpush-btn-fb-livechat,.smpush-btn-fb-widget{display:none}.smpush-btn-ctrlq.fb-close{position:absolute;right:3px;cursor:pointer}.smpush-btn-ctrlq.smpush-btn-fb-button img{border:0;margin-right: 5px}.smpush-btn-ctrlq.smpush-btn-fb-button{display: inline-block;z-index:99;color:'.$color.'!important;background-color:'.$bgcolor.';width:'.$width.'px;height:'.$height.'px;line-height:'.($height-3).'px;text-decoration: none!important;text-align:center;border:0;outline:0;border-radius:3px;-webkit-border-radius:3px;-moz-border-radius:3px;-ms-border-radius:3px;-o-border-radius:3px;box-shadow:0 1px 6px rgba(0,0,0,.06),0 2px 32px rgba(0,0,0,.16);-webkit-transition:box-shadow .2s ease;transition:all .2s ease-in-out}.smpush-btn-ctrlq.smpush-btn-fb-button:focus,.smpush-btn-ctrlq.smpush-btn-fb-button:hover{transform:scale(1.1);box-shadow:0 2px 8px rgba(0,0,0,.09),0 4px 40px rgba(0,0,0,.24)}.smpush-btn-fb-widget{background:#fff;z-index:100;position:absolute;width:360px;height:400px;overflow:hidden;opacity:0;border-radius:6px;-o-border-radius:6px;-webkit-border-radius:6px;box-shadow:0 5px 40px rgba(0,0,0,.16);-webkit-box-shadow:0 5px 40px rgba(0,0,0,.16);-moz-box-shadow:0 5px 40px rgba(0,0,0,.16);-o-box-shadow:0 5px 40px rgba(0,0,0,.16)}.fb-credit{text-align:center;margin-top:8px}.fb-credit a{transition:none;color:#bec2c9;font-family:Helvetica,Arial,sans-serif;font-size:12px;text-decoration:none;border:0;font-weight:400}.smpush-btn-ctrlq.smpush-btn-fb-overlay{z-index:98;position:fixed;height:100vh;width:100vw;-webkit-transition:opacity .4s,visibility .4s;transition:opacity .4s,visibility .4s;top:0;left:0;background:rgba(0,0,0,.05);display:none}.smpush-btn-ctrlq.fb-close{z-index:4;padding:0 6px;background:#365899;font-weight:700;font-size:11px;color:#fff;margin:8px;border-radius:3px}.smpush-btn-ctrlq.fb-close::after{content:"x";font-family:sans-serif}</style>
<div class="smpush-btn-fb-livechat">
  <div class="smpush-btn-ctrlq smpush-btn-fb-overlay"></div>
  <div class="smpush-btn-fb-widget">
    <div class="smpush-btn-ctrlq fb-close"></div>
    <div class="fb-page" data-href="'.self::$apisetting['msn_fbpage_link'].'" data-tabs="messages" data-width="360" data-height="400" data-small-header="true" data-hide-cover="true" data-show-facepile="false">
      <blockquote cite="'.self::$apisetting['msn_fbpage_link'].'" class="fb-xfbml-parse-ignore"> </blockquote>
    </div>
    <div id="fb-root"></div>
  </div>
  <a href="'.self::$apisetting['msn_btn_fblink'].'" class="smpush-btn-ctrlq smpush-btn-fb-button"><img src="'.$icon.'" /> '.$text.'</a>
</div>';
echo '<script data-cfasync="false" type="text/javascript">
jQuery(document).ready(function(){var t={delay:125,overlay:jQuery(".smpush-btn-fb-overlay"),widget:jQuery(".smpush-btn-fb-widget"),button:jQuery(".smpush-btn-fb-button")};setTimeout(function(){jQuery("div.smpush-btn-fb-livechat").fadeIn()},8*t.delay),jQuery(".smpush-btn-ctrlq").on("click",function(e){e.preventDefault(),t.overlay.is(":visible")?(t.overlay.fadeOut(t.delay),t.widget.stop().animate({opacity:0},2*t.delay,function(){jQuery(this).hide("slow"),t.button.show()})):t.button.fadeOut("medium",function(){t.widget.stop().show().animate({opacity:1},2*t.delay),t.overlay.fadeIn(t.delay)})})});</script>
';
  }
  
}