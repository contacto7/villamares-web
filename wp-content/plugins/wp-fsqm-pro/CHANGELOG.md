From version 4.0.0 this file will only contain changelog for the current active
version according to [semver](http://semver.org/). For older changelog, kindly
see version history.

## Version 4.1.3

> Buf fixes and compatibility with easySubmission Add-on

### Changes

* **Fix** - JS bug with dependent datepicker element.
* **Fix** - CSS issue with datepicker div, not being hidden under some theme.
* **Fix** - Compatibility issue with easySubmission add-on.

--------------------------------------------------------------------------------

## Version 4.1.2

> Bug fixes

### Changes

* **Fix** - Blank value appearance issue for inline element
* **Fix** - Stripe Elements appearance issue for dark themes

--------------------------------------------------------------------------------

## Version 4.1.1

> Bug fixes and feature enhancements

### Changes

* **New** - Add Support for MailPoet 3
* **Update** - Underline fill in the blank question in summary table
* **Update** - Highlight correct feedback questions according to settings
* **Update** - Make score and average score output configurable
* **Fix** - Auto shrink thumbnail elements on smaller screen

--------------------------------------------------------------------------------

## Version 4.1.0

> Implement Subscription Payments

### Changes

* **New** - Subscription Payment with Stripe.
* **New** - Option to highlight all positive scores in summary table.
* **New** - Option to reverse the order of smiley rating.
* **Fix** - Appearance issue with styled container without icons.
* **Fix** - Bug in iFrame/GPS inside hidden/collapsible containers.
* **Fix** - Missing prefix and suffix in feedback small inline appearance.
* **Fix** - Make Center/Vertical appearance work for payment element.
* **Fix** - Hidden Stripe Payment causing JS error.
* **Fix** - Primary fields not getting disabled after adding to the form.
* **Fix** - GetResponse Integration now updated with v3 API.

### Under the hood

* **Update** - Improve Auto-Update and error reporting functionality.

--------------------------------------------------------------------------------

## Version 4.0.3

> Fix Guest Blogging Element appearance issues.

### Changes

* **Fix** - Issue with Guest Blog element placeholder.
* **Fix** - Issue with Guest Blog Editor toolbar modals.

### Under the hood

* **New** - Option to disable eForm activation notice.

--------------------------------------------------------------------------------

## Version 4.0.2

> Fix Payment and ZIP code related issues.

### Changes

* **Fix** - Issue with payment retry form.
* **Fix** - Issue with ZIP code field not accepting alphanumeric codes.

### Under the hood

* **Update** - Move `hiddens` method to `IPT_Plugin_UIF_Base` to expose to all classes.
* **Update** - Updated Composer dependencies to latest (`Stripe`).

--------------------------------------------------------------------------------

## Version 4.0.1

> Quick patch for WordPress MultiSite.

### Changes

* **Fix** - Static database table naming issue with WordPress MS

--------------------------------------------------------------------------------

## Version 4.0.0

> Major code refactor to introduce modern workflow and features focused on payment
and cost estimation.

Many breaking API changes. Check the [DevOps](https://wpq-develop.wpquark.xyz/wp-fsqm-pro/)
page for more information.

### Changes

* **New** - Authorize.net payment integration
* **New** - Auto Update Functionality
* **New** - Automatic score for feedback elements
* **New** - Estimation Slider interface for payment forms
* **New** - Input masking on freetype form elements
* **New** - Interactive form elements support for piping element values into labels
* **New** - OpenGraph & Twitter metadata in standalone form pages
* **New** - Option to change color of summary table icons
* **New** - Pricing Table Form Element
* **New** - Row index for checkbox, radio and thumnail numeric values in math element
* **New** - Zoom for statistics charts
* **Update** - Better colorpicker for Form Builder
* **Update** - Better looking payment forms
* **Update** - Better Signature Element
* **Update** - Implement changes according to new facebook API
* **Update** - Inline appearance for feedback small element
* **Update** - iziModal in popup forms with support for better manual popup
* **Update** - jQuery UI Sliders are now more responsive
* **Update** - Leaderboard shows rank and timer value
* **Update** - Select2 styling is now consistent with inputs
* **Fix** - Auto fix bad color codes in customizable material theme
* **Fix** - Auto Save Form Progress UI inconsistency
* **Fix** - Cookies based limitation not working under IE11
* **Fix** - Hidden mathematical element appearance issue
* **Fix** - Issue with file upload size
* **Fix** - Issue with sort by name in payment listing
* **Fix** - Issue with User Portal page logout redirect
* **Fix** - Placeholder issue in multiple grading settings

### Under the hood

* **New** - Adaptation to modern workflow with modular approach
* **New** - Grunt based CI/CD with support for automatic plugin updates for clients
* **New** - Payment module refactoring
* **New** - PHPUnit testing for a better continuous integration
* **New** - UI class refactoring
* **New** - Use bower to manage front-end dependencies
* **New** - Use composer to manage PHP dependencies
* **New** - Use NPM to manage dev dependencies
