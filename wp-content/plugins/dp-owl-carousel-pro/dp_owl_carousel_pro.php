<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link http://www.diviplugins.com
 * @since 1.5
 * @package Dp_Owl_Carousel_Pro
 *
 * @wordpress-plugin
 * Plugin Name: DP Owl Carousel Pro
 * Plugin URI: http://www.diviplugins.com/divi/owl-carousel-pro-plugin/
 * Description: Adds two new modules to the Divi Builder. One module creates a carousel from posts and custom post types. The other module creates a carousel from images you add. The Pro version adds support for Custom Post Types, custom query, change thumbnail size, change number of images shown at once, open image in lightbox, and display custom fields.
 * Version: 1.5.8
 * Author: DiviPlugins
 * Author URI: http://www.diviplugins.com
 * License: GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain: dp-owl-carousel-pro
 * Domain Path: /languages
 */
// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

define('DPOCP_NAME', 'dp-owl-carousel-pro');
define('DPOCP_VERSION', '1.5.8');
define('DPOCP_DIR', plugin_dir_path(__FILE__));
define('DPOCP_URL', plugin_dir_url(__FILE__));
define('DPOCP_LICENSE_PAGE', 'dp_ocp_license');
define('DPOCP_STORE_URL', 'https://diviplugins.com/');
define('DPOCP_ITEM_NAME', 'Owl Carousel Pro');
define('DPOCP_ITEM_ID', '4541');

/**
 * The code that runs during plugin activation. This action is documented in includes/class-dp-owl-carousel-pro-activator.php
 */
function activate_dp_owl_carousel_pro() {
    require_once DPOCP_DIR . 'includes/class-dp-owl-carousel-pro-activator.php';
    Dp_Owl_Carousel_Pro_Activator::activate();
}

register_activation_hook(__FILE__, 'activate_dp_owl_carousel_pro');

/**
 * The code that runs during plugin deactivation. This action is documented in includes/class-dp-owl-carousel-pro-deactivator.php
 */
function deactivate_dp_owl_carousel_pro() {
    require_once DPOCP_DIR . 'includes/class-dp-owl-carousel-pro-deactivator.php';
    Dp_Owl_Carousel_Pro_Deactivator::deactivate();
}

register_deactivation_hook(__FILE__, 'deactivate_dp_owl_carousel_pro');

/**
 * The core plugin class that is used to define internationalization, admin-specific hooks, and public-facing site hooks.
 */
require DPOCP_DIR . 'includes/class-dp-owl-carousel-pro.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks, then kicking off the plugin from this point in the file does not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_dp_owl_carousel_pro() {
    $plugin = new Dp_Owl_Carousel_Pro();
    $plugin->run();
}

run_dp_owl_carousel_pro();

/**
 * Custom function to render the custom post types on the module panel
 * 
 * @since 1.5
 */
function dp_ocp_custom_post_types_option() {
    $post_types = Dp_Owl_Carousel_Pro_Utils::get_post_types();
    $output = "\t" . "<% var et_pb_cpt_name_temp = typeof et_pb_cpt_name !== 'undefined' ? et_pb_cpt_name.split( ',' ) : []; %>" . "\n";
    foreach ($post_types as $post_type) {
        $contains = sprintf('<%%= _.contains( et_pb_cpt_name_temp, "%1$s" ) ? checked="checked" : "" %%>', esc_html($post_type['pt']));
        $output .= sprintf('%4$s<label><input type="checkbox" class="et_dp_ocp_cpt" name="et_pb_cpt_name" value="%1$s"%3$s> %2$s</label><br/>', esc_attr($post_type['pt']), esc_html(ucfirst($post_type['pt'])), $contains, "\n\t\t\t\t\t");
    }
    return apply_filters('dp_ocp_custom_post_types_option_html', $output);
}

/**
 * Custom function to render the taxonomy terms on the module panel
 * 
 * @since 1.5
 */
function dp_ocp_categories_option() {
    $output = "\t" . "<% var et_pb_cpt_categories_temp = typeof et_pb_cpt_categories !== 'undefined' ? et_pb_cpt_categories.split( ',' ) : []; %>" . "\n";
    $cats_array = Dp_Owl_Carousel_Pro_Utils::get_terms();
    if (empty($cats_array)) {
        $output = '<p>' . __("You currently don't have any custom post type assigned to a category.", DPOCP_NAME) . '</p>';
    } else {
        foreach ($cats_array as $category) {
            if ($category['name'] != 'dp_none_taxonomy') {
                $contains = sprintf('<%%= _.contains( et_pb_cpt_categories_temp, "%1$s" ) ? checked="checked" : "" %%>', esc_html($category['term_id']));
                $output .= sprintf('%4$s<label class="et_dp_ocp_categories"><input type="checkbox"  name="et_pb_cpt_categories" value="%1$s"%3$s> %2$s</label><br/>', esc_attr($category['term_id']), esc_html($category['name'] . " (" . $category['pt'] . ")"), $contains, "\n\t\t\t\t\t");
            }
        }
    }
    return apply_filters('dp_ocp_categories_option_html', $output);
}

/**
 * Add settings, license and get support links to the plugins lists in the plugin meta row.
 * 
 * @since 1.5
 */
function dp_ocp_add_plugin_row_meta($links, $file) {
    if ($file === plugin_basename(__FILE__)) {
        $links['license'] = sprintf('<a href="%s"> %s </a>', admin_url('plugins.php?page=dp_ocp_license'), __('License', DPOCP_NAME));
        $links['support'] = sprintf('<a href="%s" target="_blank"> %s </a>', 'https://diviplugins.com/documentation/owl-carousel-pro/', __('Get support', DPOCP_NAME));
    }
    return $links;
}

add_filter('plugin_row_meta', 'dp_ocp_add_plugin_row_meta', 10, 2);
