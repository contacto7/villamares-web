<?php
if (!class_exists('ET_Builder_Module_DP_OC')) {

    class ET_Builder_Module_DP_OC extends ET_Builder_Module {

        private $terms_array, $image_sizes;

        function init() {
            $this->name = esc_html__('DP Owl Carousel', DPOCP_NAME);
            $this->slug = 'et_pb_dp_oc';
            $this->fb_support = true;
            global $dp_terms_array;
            global $dp_image_sizes;
            $this->terms_array = $dp_terms_array;
            $this->image_sizes = $dp_image_sizes;
            $this->whitelisted_fields = array(
                'module_id',
                'module_class',
                'custom_query',
                'cpt_name',
                'cpt_categories',
                'number_post',
                'offset_number',
                'taxonomy_tags',
                'include_tags',
                'exclude_tags',
                'orderby',
                'order',
                'show_post_title',
                'show_post_category',
                'show_post_date',
                'custom_fields',
                'custom_field_names',
                'custom_field_labels',
                'show_post_excerpt',
                'post_excerpt_length',
                'item_margin',
                'show_arrow',
                'show_control',
                'use_hash_thumbnail',
                'hash_thumbnail_align',
                'hash_thumbnail_size',
                'arrow_color',
                'arrow_size',
                'control_color',
                'control_size',
                'lightbox',
                'lightbox_gallery',
                'behavior',
                'direction',
                'center',
                'items_per_dot',
                'items_per_slide',
                'slide_auto',
                'slide_speed',
                'animation_speed',
                'arrows_speed',
                'dots_speed',
                'slide_hover',
                'thumbnail_size',
                'thumbnail_original',
                'number_thumb',
                'number_thumb_tablet',
                'number_thumb_phone',
                'number_thumb_last_edited',
                'module_bg',
                'background_layout',
                'read_more',
                'read_more_text',
                'auto_width',
                'lazy_load',
                'remove_current_post'
            );
            $this->fields_defaults = array(
                'custom_query' => array('off'),
                'number_post' => array('10', 'add_default_setting'),
                'orderby' => array('date'),
                'order' => array('DESC', 'add_default_setting'),
                'show_post_title' => array('off'),
                'show_post_category' => array('off'),
                'show_post_date' => array('off'),
                'custom_fields' => array('off'),
                'show_post_excerpt' => array('off'),
                'post_excerpt_length' => array('270', 'add_default_setting'),
                'item_margin' => array('8', 'add_default_setting'),
                'show_arrow' => array('on'),
                'show_control' => array('on'),
                'use_hash_thumbnail' => array('off'),
                'hash_thumbnail_align' => array('dpoc-align-center', 'add_default_setting'),
                'hash_thumbnail_size' => array('thumbnail', 'add_default_setting'),
                'arrow_size' => array('4em', 'add_default_setting'),
                'control_size' => array('16px', 'add_default_setting'),
                'behavior' => array('loop', 'add_default_setting'),
                'direction' => array('rlf', 'add_default_setting'),
                'center' => array('off'),
                'items_per_dot' => array('3', 'add_default_setting'),
                'items_per_slide' => array('1', 'add_default_setting'),
                'lightbox' => array('off'),
                'lightbox_gallery' => array('off'),
                'slide_auto' => array('on'),
                'slide_speed' => array('5000', 'add_default_setting'),
                'animation_speed' => array('500', 'add_default_setting'),
                'arrows_speed' => array('500', 'add_default_setting'),
                'dots_speed' => array('500', 'add_default_setting'),
                'slide_hover' => array('off'),
                'thumbnail_original' => array('off'),
                'thumbnail_size' => array('et-pb-portfolio-image', 'add_default_setting'),
                'read_more' => array('off'),
                'read_more_text' => array('read more', 'add_default_setting'),
                'auto_width' => array('off'),
                'lazy_load' => array('off'),
                'remove_current_post' => array('off'),
            );
            $this->options_toggles = array(
                'general' => array(
                    'toggles' => array(
                        'content' => esc_html__('Content', DPOCP_NAME),
                        'elements' => esc_html__('Elements', DPOCP_NAME),
                    ),
                ),
                'advanced' => array(
                    'toggles' => array(
                        'text' => array(
                            'title' => esc_html__('Text', DPOCP_NAME),
                            'priority' => 1
                        ),
                        'arrows' => array(
                            'title' => esc_html__('Arrow', DPOCP_NAME),
                            'priority' => 94
                        ),
                        'controls' => array(
                            'title' => esc_html__('Controls', DPOCP_NAME),
                            'priority' => 95
                        ),
                        'thumbnail' => array(
                            'title' => esc_html__('Thumbnails', DPOCP_NAME),
                            'priority' => 97
                        ),
                        'thumbnail_nav' => array(
                            'title' => esc_html__('Thumbnails Navigation', DPOCP_NAME),
                            'priority' => 98
                        ),
                        'onclick' => array(
                            'title' => esc_html__('Click Action', DPOCP_NAME),
                            'priority' => 99
                        ),
                        'background' => array(
                            'title' => esc_html__('Background', DPOCP_NAME),
                            'priority' => 100
                        ),
                    ),
                ),
            );
            $this->main_css_element = '%%order_class%%';
            $this->advanced_options = array(
                'fonts' => array(
                    'post_title' => array(
                        'label' => __('Post Title', DPOCP_NAME),
                        'css' => array(
                            'main' => "{$this->main_css_element} .dp_oc_item .dp_oc_post_title",
                        ),
                    ),
                    'post_meta' => array(
                        'label' => __('Post Meta', DPOCP_NAME),
                        'css' => array(
                            'main' => "{$this->main_css_element} .dp_oc_item .dp_oc_post_meta ",
                        ),
                    ),
                    'post_excerpt' => array(
                        'label' => __('Post Excerpt', DPOCP_NAME),
                        'css' => array(
                            'main' => "{$this->main_css_element} .dp_oc_item .dp_oc_post_excerpt",
                        ),
                    ),
                    'read_more' => array(
                        'label' => __('Read More Link', DPOCP_NAME),
                        'css' => array(
                            'main' => "{$this->main_css_element} .dp_oc_item .dp_oc_post_excerpt .dp_oc_read_more_link",
                        ),
                    ),
                    'dp_custom_field' => array(
                        'label' => __('Custom Fields', DPOCP_NAME),
                        'css' => array(
                            'main' => "{$this->main_css_element} .dp_oc_item .dp_custom_field",
                        ),
                    ),
                ),
                'custom_margin_padding' => array(),
                'max_width' => array(),
                'border' => array(),
            );
            $this->custom_css_options = array(
                'ocp_item' => array(
                    'label' => __('Carousel Items Container', DPOCP_NAME),
                    'selector' => '.dp_oc_item',
                ),
                'ocp_arrow_prev' => array(
                    'label' => __('Previous Arrow', DPOCP_NAME),
                    'selector' => '.owl-carousel .owl-nav .owl-prev::before',
                ),
                'ocp_arrow_next' => array(
                    'label' => __('Next Arrow', DPOCP_NAME),
                    'selector' => '.owl-carousel .owl-nav .owl-next::before',
                ),
                'ocp_control' => array(
                    'label' => __('Controls', DPOCP_NAME),
                    'selector' => '.owl-carousel .owl-dots .owl-dot',
                ),
                'ocp_control_active' => array(
                    'label' => __('Active Control', DPOCP_NAME),
                    'selector' => '.owl-carousel .owl-dots .owl-dot.active',
                ),
                'ocp_item_image' => array(
                    'label' => __('Post Image', DPOCP_NAME),
                    'selector' => 'img.dp_oc_post_thumb',
                ),
                'ocp_item_title' => array(
                    'label' => __('Post Title', DPOCP_NAME),
                    'selector' => '.dp_oc_post_title',
                ),
                'ocp_item_meta' => array(
                    'label' => __('Post Meta', DPOCP_NAME),
                    'selector' => '.dp_oc_post_meta',
                ),
                'ocp_item_cf' => array(
                    'label' => __('Custom Fields Container', DPOCP_NAME),
                    'selector' => '.dp_custom_field',
                ),
                'ocp_cf_name' => array(
                    'label' => __('Custom Field Label', DPOCP_NAME),
                    'selector' => '.dp_custom_field_name',
                ),
                'ocp_cf_label' => array(
                    'label' => __('Custom Field Value', DPOCP_NAME),
                    'selector' => '.dp_custom_field_value',
                ),
                'ocp_item_excerpt' => array(
                    'label' => __('Post Excerpt', DPOCP_NAME),
                    'selector' => '.dp_oc_post_excerpt',
                ),
                'ocp_item_read_more' => array(
                    'label' => __('Read More Link', DPOCP_NAME),
                    'selector' => '.dp_oc_read_more_link',
                ),
                'ocp_hash_container' => array(
                    'label' => __('Navigation Thumbnail Container', DPOCP_NAME),
                    'selector' => '.dp_ocp_hash_container',
                ),
                'ocp_hash_image' => array(
                    'label' => __('Navigation Thumbnail Images', DPOCP_NAME),
                    'selector' => '.dp_ocp_hash_image',
                ),
            );
        }

        function get_fields() {
            $fields = array(
                'custom_query' => array(
                    'label' => __('Custom Query', DPOCP_NAME),
                    'type' => 'yes_no_button',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => __('No', DPOCP_NAME),
                        'on' => __('Yes', DPOCP_NAME),
                    ),
                    'default' => 'off',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'content',
                    'description' => __('Turn this option on if you want to create a custom query that is not possible using the options below. Once this option is turned on, all Content options below will be ignored and the module will load the 10 most recent blog posts by default. You can override this query using the following filter in your child theme\'s functions.php file: <strong>dp_ocp_custom_query_args</strong>. For more information and to see an example, see demo at <a href="https://www.diviplugins.com/divi-custom-queries/" target="_blank">Divi Plugins</a>  ', DPOCP_NAME),
                ),
                'number_post' => array(
                    'label' => __('Posts number', DPOCP_NAME),
                    'type' => 'text',
                    'option_category' => 'configuration',
                    'default' => '10',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'content',
                    'description' => __('How many posts you want to include in the carousel', DPOCP_NAME),
                ),
                'offset_number' => array(
                    'label' => __('Offset number', DPOCP_NAME),
                    'type' => 'text',
                    'option_category' => 'configuration',
                    'default' => '0',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'content',
                    'description' => __('Choose how many posts you would like to offset by', DPOCP_NAME),
                ),
                'cpt_name' => array(
                    'label' => __('Custom Post Type Name', DPOCP_NAME),
                    'option_category' => 'basic_option',
                    'renderer' => 'dp_ocp_custom_post_types_option',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'content',
                    'description' => __('Check which posts types you would like to include in the layout <hr><a href="#" class="dp_ocp_reload_cpt">Refresh Post Types and Categories</a>', DPOCP_NAME),
                ),
                'cpt_categories' => array(
                    'label' => __('Categories', DPOCP_NAME),
                    'option_category' => 'basic_option',
                    'renderer' => 'dp_ocp_categories_option',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'content',
                    'description' => __('Check which categories you would like to include in the carousel', DPOCP_NAME),
                ),
                'taxonomy_tags' => array(
                    'label' => __('Include/Exclude Taxonomy', DPOCP_NAME),
                    'type' => 'text',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'content',
                    'description' => __('Here you can control which taxonomy the include/exclude tags apply to. Leave empty for posts. For other CPTs, enter the tag name above. For projects, the tag name is project_tag.', DPOCP_NAME),
                ),
                'include_tags' => array(
                    'label' => __('Include Tags', DPOCP_NAME),
                    'type' => 'text',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'content',
                    'description' => __('Enter a single tag slug or a comma separated list of tag slugs. All posts in the categories above AND WITH these tags will load. Leave empty if you only want to filter using the categories above.', DPOCP_NAME),
                ),
                'exclude_tags' => array(
                    'label' => __('Exclude Tags', DPOCP_NAME),
                    'type' => 'text',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'content',
                    'description' => __('Enter a single tag slug or a comma separated list of tag slugs. All posts in the categories above AND WITHOUT these tags will load. Leave empty if you only want to filter using the categories above.', DPOCP_NAME),
                ),
                'orderby' => array(
                    'label' => __('Order By', DPOCP_NAME),
                    'type' => 'select',
                    'option_category' => 'configuration',
                    'options' => array(
                        'date' => __('Date', DPOCP_NAME),
                        'title' => __('Title', DPOCP_NAME),
                        'name' => __('Slug', DPOCP_NAME),
                        'rand' => __('Random', DPOCP_NAME),
                    ),
                    'description' => __('Choose how to sort posts', DPOCP_NAME),
                    'tab_slug' => 'general',
                    'toggle_slug' => 'content',
                ),
                'order' => array(
                    'label' => __('Order', DPOCP_NAME),
                    'type' => 'select',
                    'option_category' => 'configuration',
                    'options' => array(
                        'DESC' => __('Desc', DPOCP_NAME),
                        'ASC' => __('Asc', DPOCP_NAME),
                    ),
                    'description' => __('Choose which order to display posts', DPOCP_NAME),
                    'tab_slug' => 'general',
                    'toggle_slug' => 'content',
                ),
                'show_post_title' => array(
                    'label' => __('Show title', DPOCP_NAME),
                    'type' => 'yes_no_button',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => __('No', DPOCP_NAME),
                        'on' => __('Yes', DPOCP_NAME),
                    ),
                    'default' => 'off',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'elements',
                    'description' => __('Turn the title on or off.', DPOCP_NAME),
                ),
                'show_post_category' => array(
                    'label' => __('Show category', DPOCP_NAME),
                    'type' => 'yes_no_button',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => __('No', DPOCP_NAME),
                        'on' => __('Yes', DPOCP_NAME),
                    ),
                    'default' => 'off',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'elements',
                    'description' => __('Turn the category on or off.', DPOCP_NAME),
                ),
                'show_post_date' => array(
                    'label' => __('Show date', DPOCP_NAME),
                    'type' => 'yes_no_button',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => __('No', DPOCP_NAME),
                        'on' => __('Yes', DPOCP_NAME),
                    ),
                    'default' => 'off',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'elements',
                    'description' => __('Turn the post date on or off.', DPOCP_NAME),
                ),
                'custom_fields' => array(
                    'label' => __('Show Custom Fields', DPOCP_NAME),
                    'type' => 'yes_no_button',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => __('No', DPOCP_NAME),
                        'on' => __('Yes', DPOCP_NAME),
                    ),
                    'default' => 'off',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'elements',
                    'affects' => array(
                        '#et_pb_custom_field_names',
                        '#et_pb_custom_field_labels',
                    ),
                    'description' => __('Displays custom fields set in each post.', DPOCP_NAME),
                ),
                'custom_field_names' => array(
                    'label' => __('Custom Field Names', DPOCP_NAME),
                    'type' => 'text',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'elements',
                    'description' => __('Enter a single custom field name or a comma separated list of names.', DPOCP_NAME),
                ),
                'custom_field_labels' => array(
                    'label' => __('Custom Field Labels', DPOCP_NAME),
                    'type' => 'text',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'elements',
                    'description' => __('Enter custom field label (including separator and spaces) or a comma separated list of labels in the same order as the names above. The number of labels must equal the number of names above, otherwise the name above will be used as the label for each custom field. For more information, see demo at <a href="http://www.diviplugins.com/owl-carousel-pro-plugin/">Divi Plugins</a>', DPOCP_NAME),
                ),
                'show_post_excerpt' => array(
                    'label' => __('Show excerpt', DPOCP_NAME),
                    'type' => 'yes_no_button',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => __('No', DPOCP_NAME),
                        'on' => __('Yes', DPOCP_NAME),
                    ),
                    'default' => 'off',
                    'affects' => array('post_excerpt_length', 'read_more'),
                    'tab_slug' => 'general',
                    'toggle_slug' => 'elements',
                    'description' => __('Turn the post excerpt on or off.', DPOCP_NAME),
                ),
                'post_excerpt_length' => array(
                    'label' => esc_html__('Automatic Excerpt Length', DPOCP_NAME),
                    'type' => 'text',
                    'option_category' => 'configuration',
                    'depends_show_if' => 'on',
                    'description' => esc_html__('Define the length of automatically generated excerpts. Leave blank for default ( 270 ) ', DPOCP_NAME),
                    'tab_slug' => 'general',
                    'toggle_slug' => 'elements',
                ),
                'read_more' => array(
                    'label' => __('Show Read More Link', DPOCP_NAME),
                    'type' => 'yes_no_button',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => __('No', DPOCP_NAME),
                        'on' => __('Yes', DPOCP_NAME),
                    ),
                    'default' => 'off',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'elements',
                    'affects' => array('read_more_text'),
                    'description' => __('Turn the read more link on or off.', DPOCP_NAME),
                ),
                'read_more_text' => array(
                    'label' => __('Read More Text', DPOCP_NAME),
                    'type' => 'text',
                    'option_category' => 'configuration',
                    'depends_show_if' => 'on',
                    'default' => 'read_more',
                    'description' => __('Define the read more text. Leave blank for default ( read more ) ', DPOCP_NAME),
                    'tab_slug' => 'general',
                    'toggle_slug' => 'elements',
                ),
                'remove_current_post' => array(
                    'label' => __('Remove Current Post', DPOCP_NAME),
                    'type' => 'yes_no_button',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => __('No', DPOCP_NAME),
                        'on' => __('Yes', DPOCP_NAME),
                    ),
                    'default' => 'off',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'elements',
                    'description' => __('Turn on if you want to remove the current post when you are using the carousel from the query. Useful if you want to show a carousel of related content.', DPOCP_NAME),
                ),
                'show_arrow' => array(
                    'label' => __('Arrows', DPOCP_NAME),
                    'type' => 'yes_no_button',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => __('No', DPOCP_NAME),
                        'on' => __('Yes', DPOCP_NAME),
                    ),
                    'default' => 'on',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'elements',
                    'affects' => array('arrow_size', 'arrow_color', 'items_per_slide'),
                    'description' => __('This setting allows you to turn the navigation arrows on or off. Arrows will only display if the number of available posts exceeds the number of thumbnails set to display per slide.', DPOCP_NAME),
                ),
                'items_per_slide' => array(
                    'label' => __('Items Per Slide Action', DPOCP_NAME),
                    'type' => 'text',
                    'option_category' => 'configuration',
                    'default' => '1',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'elements',
                    'default_show_if' => 'on',
                    'description' => __('Number of posts to slide left or right when clicking the arrows.', DPOCP_NAME),
                ),
                'show_control' => array(
                    'label' => __('Controls', DPOCP_NAME),
                    'type' => 'yes_no_button',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => __('No', DPOCP_NAME),
                        'on' => __('Yes', DPOCP_NAME),
                    ),
                    'default' => 'on',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'elements',
                    'affects' => array('control_size', 'control_color', 'items_per_dot', 'use_hash_thumbnail'),
                    'description' => __('Turn navigation controls on or off. Controls will only display if the number of available posts exceeds the number of thumbnails set to display.', DPOCP_NAME),
                ),
                'items_per_dot' => array(
                    'label' => __('Items Per Control Action', DPOCP_NAME),
                    'type' => 'text',
                    'option_category' => 'configuration',
                    'default' => '3',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'elements',
                    'default_show_if' => 'on',
                    'description' => __('Number of posts to slide left or right when clicking the control dots. Disabled if Center option is turned on.', DPOCP_NAME),
                ),
                'use_hash_thumbnail' => array(
                    'label' => __('Use Navigation Thumbnail Images', DPOCP_NAME),
                    'type' => 'yes_no_button',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => __('No', DPOCP_NAME),
                        'on' => __('Yes', DPOCP_NAME),
                    ),
                    'default' => 'off',
                    'affects' => array('hash_thumbnail_align', 'hash_thumbnail_size',),
                    'tab_slug' => 'general',
                    'toggle_slug' => 'elements',
                    'description' => __('Use navigation thumbnails images instead of controls.', DPOCP_NAME),
                ),
                'hash_thumbnail_align' => array(
                    'label' => __('Navigation Thumbnail Alignment', DPOCP_NAME),
                    'type' => 'select',
                    'option_category' => 'configuration',
                    'options' => array(
                        'dpoc-align-center' => __('Center', DPOCP_NAME),
                        'dpoc-align-right' => __('Right', DPOCP_NAME),
                        'dpoc-align-left' => __('Left', DPOCP_NAME),
                    ),
                    'default' => 'dpoc-align-center',
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'thumbnail_nav',
                    'description' => __('Navigation thumbnails image alignment.', DPOCP_NAME),
                ),
                'hash_thumbnail_size' => array(
                    'label' => __('Navigation Thumbnail Size', DPOCP_NAME),
                    'type' => 'select',
                    'default' => 'thumbnail',
                    'option_category' => 'configuration',
                    'options' => $this->image_sizes,
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'thumbnail_nav',
                    'description' => __('Navigation thumbnails image source size. You can further adjust the image size using CSS in the Navigation Thumbnail Image box in the Advanced tab.', DPOCP_NAME),
                ),
                'arrow_color' => array(
                    'label' => __('Arrows color', DPOCP_NAME),
                    'type' => 'color',
                    'option_category' => 'configuration',
                    'custom_color' => true,
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'arrows',
                ),
                'arrow_size' => array(
                    'label' => __('Arrows size', DPOCP_NAME),
                    'type' => 'select',
                    'option_category' => 'configuration',
                    'options' => array(
                        '2em' => __('Small', DPOCP_NAME),
                        '4em' => __('Medium', DPOCP_NAME),
                        '6em' => __('Large', DPOCP_NAME),
                    ),
                    'default' => '4em',
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'arrows',
                ),
                'control_color' => array(
                    'label' => __('Control color', DPOCP_NAME),
                    'type' => 'color',
                    'option_category' => 'configuration',
                    'custom_color' => true,
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'controls',
                ),
                'control_size' => array(
                    'label' => __('Control size', DPOCP_NAME),
                    'type' => 'select',
                    'option_category' => 'configuration',
                    'options' => array(
                        '8px' => __('Small', DPOCP_NAME),
                        '16px' => __('Medium', DPOCP_NAME),
                        '24px' => __('Large', DPOCP_NAME),
                    ),
                    'default' => '16px',
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'controls',
                ),
                'lightbox' => array(
                    'label' => __('Open in Lightbox', DPOCP_NAME),
                    'type' => 'yes_no_button',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => __('No', DPOCP_NAME),
                        'on' => __('Yes', DPOCP_NAME),
                    ),
                    'default' => 'off',
                    'tab_slug' => 'advanced',
                    'affects' => array('lightbox_gallery'),
                    'toggle_slug' => 'onclick',
                    'description' => __('Image opens in lightbox instead of opening blog post.', DPOCP_NAME),
                ),
                'lightbox_gallery' => array(
                    'label' => __('Lightbox Gallery', DPOCP_NAME),
                    'type' => 'yes_no_button',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => __('No', DPOCP_NAME),
                        'on' => __('Yes', DPOCP_NAME),
                    ),
                    'default' => 'off',
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'onclick',
                    'description' => __('Turn this option on if you want the lightbox to display all images from the carousel in a gallery. Leave this option off if you only want the clicked image to display in the lightbox.', DPOCP_NAME),
                ),
                'behavior' => array(
                    'label' => __('Carousel Behavior', DPOCP_NAME),
                    'type' => 'select',
                    'option_category' => 'configuration',
                    'options' => array(
                        'loop' => __('Loop', DPOCP_NAME),
                        'rewind' => __('Rewind', DPOCP_NAME),
                        'linear' => __('Linear', DPOCP_NAME),
                    ),
                    'default' => 'loop',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'elements',
                    'description' => __('Choose whether carousel should advance in an infinite loop, jump to first item when it reaches the end, or stop when it reaches the end.', DPOCP_NAME),
                ),
                'direction' => array(
                    'label' => __('Carousel Direction', DPOCP_NAME),
                    'type' => 'select',
                    'option_category' => 'configuration',
                    'options' => array(
                        'rtl' => __('Right to left', DPOCP_NAME),
                        'ltr' => __('Left to right', DPOCP_NAME),
                    ),
                    'default' => 'rtl',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'elements',
                    'description' => __('Choose which direction the carousel should advance.', DPOCP_NAME),
                ),
                'center' => array(
                    'label' => __('Center', DPOCP_NAME),
                    'type' => 'yes_no_button',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => __('No', DPOCP_NAME),
                        'on' => __('Yes', DPOCP_NAME),
                    ),
                    'default' => 'off',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'elements',
                    'description' => __('First carousel item will always start in the center of the carousel. This option must be turned on to center the clicked image in the carousel when using Navigation Thumbnails.', DPOCP_NAME),
                ),
                'auto_width' => array(
                    'label' => __('Auto Width Images', DPOCP_NAME),
                    'type' => 'yes_no_button',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => __('No', DPOCP_NAME),
                        'on' => __('Yes', DPOCP_NAME),
                    ),
                    'default' => 'off',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'elements',
                    'description' => __('Turn this option on if you want to display images in the size set in the Thumbnail Size option in the Design tab. Leave this option off if you want images to display evenly and adhere to Thumbnails Per Slide option in the Design tab.', DPOCP_NAME),
                ),
                'slide_auto' => array(
                    'label' => __('Automatic Rotate', DPOCP_NAME),
                    'type' => 'yes_no_button',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => esc_html__("No", DPOCP_NAME),
                        'on' => esc_html__('Yes', DPOCP_NAME),
                    ),
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'animation',
                    'affects' => array('slide_speed', 'slide_hover', 'animation_speed'),
                    'default' => 'on',
                    'description' => __('If you would like the carousel to rotate automatically, without the visitor having to click the next button, enable this option and then adjust the rotation speed below if desired.', DPOCP_NAME),
                ),
                'slide_speed' => array(
                    'label' => __('Automatic Rotate Speed (in ms)', DPOCP_NAME),
                    'type' => 'text',
                    'option_category' => 'configuration',
                    'default' => '5000',
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'animation',
                    'description' => __('Here you can designate how fast the carousel rotates between each slide, if \'Automatic Rotate\' option is enabled above. The higher the number the longer the pause between each rotation.', DPOCP_NAME),
                ),
                'animation_speed' => array(
                    'label' => __('Auto Rotate Animation Speed (in ms)', DPOCP_NAME),
                    'type' => 'text',
                    'option_category' => 'configuration',
                    'default' => '500',
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'animation',
                    'description' => __('Here you can designate how long it takes for the carousel to complete a rotation. The higher the number the slower the animation.', DPOCP_NAME),
                ),
                'arrows_speed' => array(
                    'label' => __('Arrow Animation Speed (in ms)', DPOCP_NAME),
                    'type' => 'text',
                    'option_category' => 'configuration',
                    'default' => '500',
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'animation',
                    'description' => __('Here you can designate how long it takes for the carousel to complete a rotation when the arrows are clicked. The higher the number the slower the animation.', DPOCP_NAME),
                ),
                'dots_speed' => array(
                    'label' => __('Control Animation Speed (in ms)', DPOCP_NAME),
                    'type' => 'text',
                    'option_category' => 'configuration',
                    'default' => '500',
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'animation',
                    'description' => __('Here you can designate how long it takes for the carousel to complete a rotation when the control dots are clicked. The higher the number the slower the animation.', DPOCP_NAME),
                ),
                'slide_hover' => array(
                    'label' => __('Pause on Hover', DPOCP_NAME),
                    'type' => 'yes_no_button',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => esc_html__("No", DPOCP_NAME),
                        'on' => esc_html__('Yes', DPOCP_NAME),
                    ),
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'animation',
                    'default' => 'off',
                    'description' => __('Pause carousel rotation when user hovers over the slides', DPOCP_NAME),
                ),
                'thumbnail_original' => array(
                    'label' => __('Original Size', DPOCP_NAME),
                    'type' => 'yes_no_button',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => esc_html__("No", DPOCP_NAME),
                        'on' => esc_html__('Yes', DPOCP_NAME),
                    ),
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'thumbnail',
                    'default' => 'off',
                    'affects' => array('thumbnail_size'),
                    'description' => __('Load the original image instead of looking for image thumbnail. If images are small and do not have a thumbnail in the size selected, this option will load the full size of the image.', DPOCP_NAME),
                ),
                'thumbnail_size' => array(
                    'label' => __('Thumbnail Size', DPOCP_NAME),
                    'type' => 'select',
                    'default' => 'et-pb-portfolio-image',
                    'option_category' => 'configuration',
                    'depends_show_if' => 'off',
                    'options' => $this->image_sizes,
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'thumbnail'
                ),
                'number_thumb' => array(
                    'label' => __('Thumbnails to Display', DPOCP_NAME),
                    'type' => 'range',
                    'mobile_options' => true,
                    'validate_unit' => false,
                    'default_unit' => '',
                    'option_category' => 'configuration',
                    'range_settings' => array(
                        'min' => '1',
                        'max' => '20',
                        'step' => '1',
                    ),
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'thumbnail',
                    'description' => 'This setting determines how many carousel items will be initially displayed on the screen.'
                ),
                'number_thumb_tablet' => array(
                    'type' => 'skip',
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'thumbnail'
                ),
                'number_thumb_phone' => array(
                    'type' => 'skip',
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'thumbnail'
                ),
                'number_thumb_last_edited' => array(
                    'type' => 'skip',
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'thumbnail'
                ),
                'item_margin' => array(
                    'label' => esc_html__('Item Margin', DPOCP_NAME),
                    'type' => 'text',
                    'option_category' => 'configuration',
                    'description' => esc_html__('Define the margin for each item at the carousel. Leave blank for default ( 8 ) ', DPOCP_NAME),
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'thumbnail',
                ),
                'lazy_load' => array(
                    'label' => __('Lazy Load', DPOCP_NAME),
                    'type' => 'yes_no_button',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => __('No', DPOCP_NAME),
                        'on' => __('Yes', DPOCP_NAME),
                    ),
                    'default' => 'off',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'elements',
                    'description' => __('Turn this option on if you want images to load on demand. This option may not be compatible with some caching and optimizing plugins and should be turned off if you experience any display issues with the carousel.', DPOCP_NAME),
                ),
                'module_bg' => array(
                    'label' => __('Items Background', DPOCP_NAME),
                    'type' => 'color',
                    'option_category' => 'layout',
                    'custom_color' => true,
                    'tab_slug' => 'general',
                    'toggle_slug' => 'background',
                ),
                'background_layout' => array(
                    'label' => esc_html__('Text Color', DPOCP_NAME),
                    'type' => 'select',
                    'option_category' => 'color_option',
                    'options' => array(
                        'light' => esc_html__('Dark', DPOCP_NAME),
                        'dark' => esc_html__('Light', DPOCP_NAME),
                    ),
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'text',
                    'description' => esc_html__('Here you can choose whether your text should be light or dark. If you are working with a dark background, then your text should be light. If your background is light, then your text should be set to dark.', DPOCP_NAME),
                ),
                'module_id' => array(
                    'label' => __('CSS ID', DPOCP_NAME),
                    'type' => 'text',
                    'option_category' => 'configuration',
                    'tab_slug' => 'custom_css',
                    'toggle_slug' => 'classes',
                    'description' => __('Enter an optional CSS ID to be used for this module. An ID can be used to create custom CSS styling, or to create links to particular sections of your page.', DPOCP_NAME),
                ),
                'module_class' => array(
                    'label' => __('CSS Class', DPOCP_NAME),
                    'type' => 'text',
                    'option_category' => 'configuration',
                    'tab_slug' => 'custom_css',
                    'toggle_slug' => 'classes',
                    'description' => __('Enter optional CSS classes to be used for this module. A CSS class can be used to create custom CSS styling. You can add multiple classes, separated with a space.', DPOCP_NAME),
                ),
                'disabled_on' => array(
                    'label' => __('Disable on', DPOCP_NAME),
                    'type' => 'multiple_checkboxes',
                    'options' => array(
                        'phone' => __('Phone', DPOCP_NAME),
                        'tablet' => __('Tablet', DPOCP_NAME),
                        'desktop' => __('Desktop', DPOCP_NAME),
                    ),
                    'additional_att' => 'disable_on',
                    'option_category' => 'configuration',
                    'description' => __('This will disable the module on selected devices', DPOCP_NAME),
                    'tab_slug' => 'custom_css',
                    'toggle_slug' => 'visibility',
                ),
                'admin_label' => array(
                    'label' => __('Admin Label', DPOCP_NAME),
                    'type' => 'text',
                    'toggle_slug' => 'admin_label',
                    'description' => __('This will change the label of the module in the builder for easy identification.', DPOCP_NAME),
                ),
            );
            return $fields;
        }

        function dp_ocp_get_taxonomies() {
            foreach ($this->terms_array as $value) {
                $tax_name[] = $value['tax'];
            }
            return array_combine($tax_name, $tax_name);
        }

        function dp_ocp_get_taxonomy_of_post_type($pt) {
            $taxonomy = "";
            foreach ($this->terms_array as $value) {
                if ($pt === $value['pt']) {
                    $taxonomy = $value['tax'];
                    break;
                }
            }
            return $taxonomy;
        }

        function shortcode_callback($atts, $content = null, $function_name) {
            $module_id = $this->shortcode_atts['module_id'];
            $module_class = $this->shortcode_atts['module_class'];
            $number_post = $this->shortcode_atts['number_post'];
            $offset_number = $this->shortcode_atts['offset_number'];
            $custom_query = $this->shortcode_atts['custom_query'];
            $cpt_name = $this->shortcode_atts['cpt_name'];
            $cpt_categories = $this->shortcode_atts['cpt_categories'];
            $taxonomy_tags = $this->shortcode_atts['taxonomy_tags'];
            $include_tags = $this->shortcode_atts['include_tags'];
            $exclude_tags = $this->shortcode_atts['exclude_tags'];
            $orderby = $this->shortcode_atts['orderby'];
            $order = $this->shortcode_atts['order'];
            $show_post_title = $this->shortcode_atts['show_post_title'];
            $show_post_category = $this->shortcode_atts['show_post_category'];
            $show_post_date = $this->shortcode_atts['show_post_date'];
            $custom_fields = $this->shortcode_atts['custom_fields'];
            $custom_field_names = $this->shortcode_atts['custom_field_names'];
            $custom_field_labels = $this->shortcode_atts['custom_field_labels'];
            $show_post_excerpt = $this->shortcode_atts['show_post_excerpt'];
            $post_excerpt_length = $this->shortcode_atts['post_excerpt_length'];
            $item_margin = $this->shortcode_atts['item_margin'];
            $show_arrow = $this->shortcode_atts['show_arrow'];
            $arrow_size = $this->shortcode_atts['arrow_size'];
            $arrow_color = $this->shortcode_atts['arrow_color'];
            $show_control = $this->shortcode_atts['show_control'];
            $use_hash_thumbnail = $this->shortcode_atts['use_hash_thumbnail'];
            $hash_thumbnail_align = $this->shortcode_atts['hash_thumbnail_align'];
            $hash_thumbnail_size = $this->shortcode_atts['hash_thumbnail_size'];
            $control_size = $this->shortcode_atts['control_size'];
            $control_color = $this->shortcode_atts['control_color'];
            $behavior = $this->shortcode_atts['behavior'];
            $direction = $this->shortcode_atts['direction'];
            $center = $this->shortcode_atts['center'];
            $items_per_dot = $this->shortcode_atts['items_per_dot'];
            $items_per_slide = $this->shortcode_atts['items_per_slide'];
            $slide_auto = $this->shortcode_atts['slide_auto'];
            $slide_speed = $this->shortcode_atts['slide_speed'];
            $animation_speed = $this->shortcode_atts['animation_speed'];
            $arrows_speed = $this->shortcode_atts['arrows_speed'];
            $control_speed = $this->shortcode_atts['dots_speed'];
            $slide_hover = $this->shortcode_atts['slide_hover'];
            $thumbnail_size = $this->shortcode_atts['thumbnail_size'];
            $thumbnail_original = $this->shortcode_atts['thumbnail_original'];
            $number_thumb = $this->shortcode_atts['number_thumb'];
            $number_thumb_tablet = $this->shortcode_atts['number_thumb_tablet'];
            $number_thumb_phone = $this->shortcode_atts['number_thumb_phone'];
            $number_thumb_last_edited = $this->shortcode_atts['number_thumb_last_edited'];
            $module_bg = $this->shortcode_atts['module_bg'];
            $background_layout = $this->shortcode_atts['background_layout'];
            $lightbox = $this->shortcode_atts['lightbox'];
            $lightbox_gallery = $this->shortcode_atts['lightbox_gallery'];
            $read_more = $this->shortcode_atts['read_more'];
            $read_more_text = $this->shortcode_atts['read_more_text'];
            $auto_width = $this->shortcode_atts['auto_width'];
            $lazy_load = $this->shortcode_atts['lazy_load'];
            $remove_current_post = $this->shortcode_atts['remove_current_post'];

            wp_enqueue_style('dp-ocp-owl-carousel');
            wp_enqueue_style('dp-ocp-custom');
            wp_enqueue_script('dp-ocp-owl-carousel');
            wp_enqueue_script('dp-ocp-custom');

            if (et_pb_get_responsive_status($number_thumb_last_edited)) {
                if ($number_thumb_tablet === '') {
                    $number_thumb_tablet = $number_thumb;
                }
                if ($number_thumb_phone === '') {
                    $number_thumb_phone = $number_thumb_tablet;
                }
            }
            if ('' !== $module_bg) {
                ET_Builder_Element::set_style($function_name, array(
                    'selector' => '%%order_class%%.et_pb_dp_oc .dp_oc_item',
                    'declaration' => sprintf(
                            'background-color: %1$s;', esc_html($module_bg)
                    ),
                ));
            }
            if ('' !== $arrow_color) {
                ET_Builder_Element::set_style($function_name, array(
                    'selector' => '%%order_class%%.et_pb_dp_oc .owl-carousel .owl-nav',
                    'declaration' => sprintf(
                            'color: %1$s;', esc_html($arrow_color)
                    ),
                ));
            }
            if ('' !== $control_color) {
                ET_Builder_Element::set_style($function_name, array(
                    'selector' => '%%order_class%%.et_pb_dp_oc .owl-carousel .owl-dots .owl-dot',
                    'declaration' => sprintf(
                            'background-color: %1$s;', esc_html($control_color)
                    ),
                ));
            }
            if ('' !== $control_size) {
                ET_Builder_Element::set_style($function_name, array(
                    'selector' => '%%order_class%%.et_pb_dp_oc .owl-carousel .owl-dots .owl-dot',
                    'declaration' => sprintf(
                            'width: %1$s; height: %1$s', esc_html($control_size)
                    ),
                ));
            }
            $post_types = array();
            if (!empty($cpt_name)) {
                foreach (explode(',', $cpt_name) as $value) {
                    $post_types[] = $value;
                }
            }
            $post_categories = array();
            if (!empty($cpt_categories)) {
                foreach (explode(',', $cpt_categories) as $value) {
                    $post_categories[] = $value;
                }
            }
            if ('on' == $custom_query) {
                $args = Dp_Owl_Carousel_Pro_Utils::dp_ocp_custom_query();
            } else {
                $args['posts_per_page'] = intval($number_post);
                if (is_user_logged_in()) {
                    $args['post_status'] = array('publish', 'private');
                } else {
                    $args['post_status'] = array('publish');
                }
                if (!empty($offset_number)) {
                    $args['offset'] = intval($offset_number);
                }
                if (!empty($post_types)) {
                    $args['post_type'] = $post_types;
                }
                $tax_query = array();
                if (!empty($post_categories)) {
                    foreach ($this->dp_ocp_get_taxonomies() as $tax) {
                        $tax_query[] = array(
                            'taxonomy' => $tax,
                            'field' => 'term_id',
                            'terms' => $post_categories,
                        );
                    }
                    if (count($tax_query) >= 2) {
                        $tax_query['relation'] = 'OR';
                    }
                }
                if ($taxonomy_tags === '') {
                    $taxonomy_tags = 'post_tag';
                }
                $tag_query = array();
                if (!empty($include_tags)) {
                    $tag_query[] = array(
                        'taxonomy' => $taxonomy_tags,
                        'field' => 'slug',
                        'terms' => explode(',', $include_tags),
                        'operator' => 'IN'
                    );
                }
                if (!empty($exclude_tags)) {
                    $tag_query[] = array(
                        'taxonomy' => $taxonomy_tags,
                        'field' => 'slug',
                        'terms' => explode(',', $exclude_tags),
                        'operator' => 'NOT IN'
                    );
                }
                if (count($tag_query) >= 2) {
                    $tag_query['relation'] = 'AND';
                }
                if (!empty($tax_query) && empty($tag_query)) {
                    $args['tax_query'] = $tax_query;
                } else if (empty($tax_query) && !empty($tag_query)) {
                    $args['tax_query'] = $tag_query;
                } else if (!empty($tax_query) && !empty($tag_query)) {
                    $args['tax_query'][] = $tax_query;
                    $args['tax_query'][] = $tag_query;
                    $args['tax_query']['relation'] = 'AND';
                }
                $args['orderby'] = $orderby;
                $args['order'] = $order;
                if ($remove_current_post === 'on' && is_single()) {
                    $args['post__not_in'] = array(get_the_ID());
                }
            }
            $hash_thumbnail_array = array();
            $hash_thumbnail_counter = 0;
            $gallery_images = 0;
            if ($use_hash_thumbnail === 'on') {
                $show_control = 'off';
            }
            $module_class = ET_Builder_Element::add_module_order_class($module_class, $function_name);
            $class = " et_pb_module et_pb_bg_layout_{$background_layout}";

            $selected_size_width = explode('x', $this->image_sizes[$thumbnail_size])[0];
            $default_size_width = explode('x', $this->image_sizes['et-pb-portfolio-image'])[0];

            $posts = new WP_Query($args);
            ob_start();
            if ($posts->have_posts()):
                echo sprintf('<div class="owl-carousel" data-rotate="%1$s" data-speed="%2$s" data-hover="%3$s" data-arrow="%4$s" data-control="%5$s" data-items="%6$s" data-items-tablet="%7$s" data-items-phone="%8$s" data-margin="%9$s" data-behaviour="%10$s" data-direction="%11$s" data-center="%12$s" data-items-per-dot="%13$s" data-items-per-slide="%14$s" data-arrow-size="%15$s" data-use-hash="%16$s" data-use-auto-width="%17$s" data-module="%18$s" data-animation-speed="%19$s" data-arrows-speed="%20$s" data-controls-speed="%21$s" data-lazy="%22$s">', $slide_auto, $slide_speed, $slide_hover, $show_arrow, $show_control, $number_thumb, $number_thumb_tablet, $number_thumb_phone, $item_margin, $behavior, $direction, $center, $items_per_dot, $items_per_slide, $arrow_size, $use_hash_thumbnail, $auto_width, trim($module_class), $animation_speed, $arrows_speed, $control_speed, $lazy_load);
                while ($posts->have_posts()) : $posts->the_post();
                    $post_title = get_the_title();
                    $post_id = get_the_ID();
                    /*
                     * Get all custom field names and display as names
                     */
                    $post_custom_fields = '';
                    if (($custom_fields == 'on') && ($custom_field_names != '')) {
                        $custom_fields_array = explode(",", $custom_field_names);
                        if ($custom_field_labels != '') {
                            $custom_fields_display = explode(",", $custom_field_labels);
                            if (is_array($custom_fields_array) && is_array($custom_fields_display) && count($custom_fields_array) == count($custom_fields_display)) {
                                $custom_fields_array = array_combine($custom_fields_display, $custom_fields_array);
                                $post_custom_fields = Dp_Owl_Carousel_Pro_Utils::dp_get_keyed_custom_fields($custom_fields_array, $post_id);
                            } else {
                                $post_custom_fields = Dp_Owl_Carousel_Pro_Utils::dp_get_custom_fields($custom_fields_array, $post_id);
                            }
                        } else {
                            $post_custom_fields = Dp_Owl_Carousel_Pro_Utils::dp_get_custom_fields($custom_fields_array, $post_id);
                        }
                    }
                    /*
                     * Init item warp an activate data-hash navigation
                     */
                    $has_post_thumbnail = has_post_thumbnail();
                    $data_hash = '';
                    if ($use_hash_thumbnail === 'on' && $has_post_thumbnail) {
                        $data_hash = ' data-hash="' . trim($module_class) . '_' . $hash_thumbnail_counter . '" ';
                        $hash_thumbnail_counter++;
                    }
                    /*
                     * Determine the image size width that will be apply to the item size
                     */
                    $image_size_width = '';
                    if ($auto_width === 'on') {
                        if ($thumbnail_original === 'on') {
                            if ($has_post_thumbnail) {
                                $image_size_width = 'style="width: ' . wp_get_attachment_metadata(get_post_thumbnail_id())['width'] . 'px"';
                            } else {
                                $image_size_width = 'style="width: ' . $default_size_width . 'px"';
                            }
                        } else {
                            if ($has_post_thumbnail) {
                                $image_size_width = wp_get_attachment_metadata(get_post_thumbnail_id())['sizes'][$thumbnail_size]['width'];
                                if ($image_size_width !== '') {
                                    $image_size_width = 'style="width: ' . $image_size_width . 'px"';
                                } else {
                                    $image_size_width = 'style="width: ' . $selected_size_width . 'px"';
                                }
                            } else {
                                $image_size_width = 'style="width: ' . $selected_size_width . 'px"';
                            }
                        }
                    }
                    echo sprintf('<div class="dp_oc_item" %1$s %2$s>', $data_hash, $image_size_width);
                    /*
                     * Add post thumbnail
                     */
                    if ($has_post_thumbnail) {
                        if ($use_hash_thumbnail === 'on') {
                            $hash_thumbnail_array[] = get_the_post_thumbnail_url(get_the_ID(), $hash_thumbnail_size);
                        }
                        if ($thumbnail_original === 'on') {
                            $thumb_url = get_the_post_thumbnail_url();
                        } else {
                            $thumb_url = get_the_post_thumbnail_url(get_the_ID(), $thumbnail_size);
                        }
                        if ($lightbox === "on") {
                            echo sprintf('<a href="%1$s" class="dp_ocp_lightbox_image" ><img class="dp_oc_post_thumb %6$s" %7$ssrc="%2$s" alt="%3$s" data-lightbox-gallery="%4$s" data-gallery-image="%5$s"></a>', get_the_post_thumbnail_url(), $thumb_url, $post_title, ($lightbox_gallery === 'on') ? 'on' : 'off', $gallery_images++, ($lazy_load === 'on') ? 'owl-lazy' : '', ($lazy_load === 'on') ? 'data-' : '');
                        } else {
                            echo sprintf('<a href="%1$s"><img class="dp_oc_post_thumb %4$s" %5$ssrc="%2$s" alt="%3$s"></a>', get_the_permalink(), $thumb_url, $post_title, ($lazy_load === 'on') ? 'owl-lazy' : '', ($lazy_load === 'on') ? 'data-' : '');
                        }
                    }
                    /*
                     * Add post title
                     */
                    if ($show_post_title === 'on') {
                        if ($lightbox === 'on') {
                            echo sprintf('<h2 class="dp_oc_post_title">%1$s</h2>', $post_title);
                        } else {
                            echo sprintf('<h2 class="dp_oc_post_title"><a  href="%1$s">%2$s</a></h2>', get_the_permalink(), $post_title);
                        }
                    }
                    /*
                     * Add post terms
                     */
                    if ($show_post_category === 'on') {
                        echo sprintf('<p class="post-meta dp_oc_post_meta">%1$s</p>', get_the_term_list($post_id, $this->dp_ocp_get_taxonomy_of_post_type(get_post_type()), "", ", "));
                    }
                    /*
                     * Add post date
                     */
                    if ($show_post_date === 'on') {
                        echo sprintf('<p class="post-meta dp_oc_post_meta">%1$s</p>', get_the_date());
                    }
                    /*
                     * Add post custom fields
                     */
                    if ($post_custom_fields) {
                        foreach ($post_custom_fields as $field_display => $field_value) {
                            if ($field_value != '') {
                                echo sprintf('<p class="post-meta dp_custom_field"><span class="dp_custom_field_name">%1$s</span><span class="dp_custom_field_value">%2$s</span></p>', $field_display, $field_value);
                            }
                        }
                    }
                    /*
                     * Add post excerpts
                     */
                    if (checked($show_post_excerpt, 'on', false)):
                        echo '<div class="post-excerpt dp_oc_post_excerpt">';
                        if (has_excerpt()):
                            echo apply_filters('dp_ocp_custom_excerpt', wp_strip_all_tags(get_the_excerpt()));
                        else:
                            echo wpautop(et_delete_post_first_video(strip_shortcodes(truncate_post($post_excerpt_length, false, '', true))));
                        endif;
                        if ($read_more === 'on') {
                            echo sprintf('<a href="%1$s" class="dp_oc_read_more_link">%2$s</a>', get_the_permalink(), (!empty($read_more_text) ? $read_more_text : 'read more'));
                        }
                        echo '</div>';
                    endif;
                    echo '</div>';
                endwhile;
                echo '</div>';
            else:
                ?><p><?php echo __('No posts found', DPOCP_NAME); ?></p><?php
            endif;
            wp_reset_postdata();

            $hash_output = '';
            if ($use_hash_thumbnail === 'on') {
                $hash_output = '<div class="dp_ocp_hash_container ' . $hash_thumbnail_align . '">';
                foreach ($hash_thumbnail_array as $key => $value) {
                    $hash_output .= '<a href="#' . trim($module_class) . '_' . $key . '" ><img class="dp_ocp_hash_image" src="' . $value . '"></a> ';
                }
                $hash_output .= '</div>';
            }

            $content_carousel = ob_get_contents();
            ob_end_clean();

            $output = sprintf(
                    '<div%3$s class="et_pb_dp_oc%2$s%4$s" >%1$s%5$s</div><!-- .et_pb_dp_oc-->', $content_carousel, esc_attr($class), ( '' !== $module_id ? sprintf(' id="%1$s"', esc_attr($module_id)) : ''), ( '' !== $module_class ? sprintf(' %1$s', esc_attr($module_class)) : ''), $hash_output);
            return $output;
        }

    }

    new ET_Builder_Module_DP_OC;
}
?>