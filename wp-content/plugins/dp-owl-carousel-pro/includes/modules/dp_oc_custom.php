<?php
if (class_exists('ET_Builder_Module')) {

    class ET_Builder_Module_DP_OC_Custom extends ET_Builder_Module {

        private $image_sizes;
        public static $img_data = array();

        function init() {
            $this->name = esc_html__('DP Owl Image Carousel', DPOCP_NAME);
            $this->slug = 'et_pb_dp_oc_custom';
            $this->child_slug = 'et_pb_dp_oc_custom_item';
            $this->child_item_text = __('Image');

            global $dp_image_sizes;
            $this->image_sizes = $dp_image_sizes;

            $this->whitelisted_fields = array(
                'module_id',
                'module_class',
                'item_margin',
                'show_arrow',
                'show_control',
                'use_hash_thumbnail',
                'hash_thumbnail_align',
                'hash_thumbnail_size',
                'arrow_color',
                'arrow_size',
                'control_color',
                'control_size',
                'behavior',
                'direction',
                'center',
                'items_per_dot',
                'items_per_slide',
                'slide_auto',
                'slide_speed',
                'animation_speed',
                'arrows_speed',
                'dots_speed',
                'slide_hover',
                'number_thumb',
                'number_thumb_tablet',
                'number_thumb_phone',
                'number_thumb_last_edited',
                'thumbnail_size',
                'lightbox_gallery',
                'module_bg',
                'background_layout',
                'auto_width',
                'lazy_load',
            );
            $this->fields_defaults = array(
                'item_margin' => array('8', 'add_default_setting'),
                'show_arrow' => array('on'),
                'show_control' => array('on'),
                'use_hash_thumbnail' => array('off'),
                'hash_thumbnail_align' => array('dpoc-align-center', 'add_default_setting'),
                'hash_thumbnail_size' => array('thumbnail', 'add_default_setting'),
                'arrow_size' => array('4em', 'add_default_setting'),
                'control_size' => array('16px', 'add_default_setting'),
                'behavior' => array('loop', 'add_default_setting'),
                'direction' => array('rlf', 'add_default_setting'),
                'center' => array('off'),
                'items_per_dot' => array('3', 'add_default_setting'),
                'items_per_slide' => array('1', 'add_default_setting'),
                'slide_auto' => array('on'),
                'slide_speed' => array('5000', 'add_default_setting'),
                'animation_speed' => array('500', 'add_default_setting'),
                'arrows_speed' => array('500', 'add_default_setting'),
                'dots_speed' => array('500', 'add_default_setting'),
                'slide_hover' => array('off'),
                'thumbnail_size' => array('et-pb-portfolio-image', 'add_default_setting'),
                'lightbox_gallery' => array('off'),
                'auto_width' => array('off'),
                'lazy_load' => array('off'),
            );
            $this->options_toggles = array(
                'general' => array(
                    'toggles' => array(
                        'elements' => esc_html__('Elements', DPOCP_NAME),
                    ),
                ),
                'advanced' => array(
                    'toggles' => array(
                        'text' => array(
                            'title' => esc_html__('Text', DPOCP_NAME),
                            'priority' => 1
                        ),
                        'arrows' => array(
                            'title' => esc_html__('Arrow', DPOCP_NAME),
                            'priority' => 94
                        ),
                        'controls' => array(
                            'title' => esc_html__('Controls', DPOCP_NAME),
                            'priority' => 95
                        ),
                        'thumbnail' => array(
                            'title' => esc_html__('Thumbnails', DPOCP_NAME),
                            'priority' => 97
                        ),
                        'thumbnail_nav' => array(
                            'title' => esc_html__('Thumbnails Navigation', DPOCP_NAME),
                            'priority' => 98
                        ),
                    ),
                ),
                'custom_css' => array(
                    'toggles' => array(
                        'onclick' => array(
                            'title' => esc_html__('Click Action', DPOCP_NAME),
                            'priority' => 85
                        ),
                        'animation' => array(
                            'title' => esc_html__('Animation', DPOCP_NAME),
                            'priority' => 86
                        ),
                        'background' => array(
                            'title' => esc_html__('Background', DPOCP_NAME),
                            'priority' => 93
                        ),
                    ),
                ),
            );
            $this->main_css_element = '%%order_class%%';
            $this->advanced_options = array(
                'fonts' => array(
                    'image_title' => array(
                        'label' => __('Image Title', DPOCP_NAME),
                        'css' => array(
                            'main' => "{$this->main_css_element} .dp_oc_item .dp_oc_image_title",
                        ),
                    ),
                    'image_content' => array(
                        'label' => __('Image Content', DPOCP_NAME),
                        'css' => array(
                            'main' => "{$this->main_css_element} .dp_oc_item .dp_oc_image_content",
                        ),
                    ),
                ),
                'custom_margin_padding' => array(),
                'max_width' => array(),
                'border' => array(),
            );
            $this->custom_css_options = array(
                'ocp_item' => array(
                    'label' => __('Carousel Items Container', DPOCP_NAME),
                    'selector' => '.dp_oc_item',
                ),
                'ocp_arrow_prev' => array(
                    'label' => __('Previous Arrow', DPOCP_NAME),
                    'selector' => '.owl-carousel .owl-nav .owl-prev::before',
                ),
                'ocp_arrow_next' => array(
                    'label' => __('Next Arrow', DPOCP_NAME),
                    'selector' => '.owl-carousel .owl-nav .owl-next::before',
                ),
                'ocp_control' => array(
                    'label' => __('Controls', DPOCP_NAME),
                    'selector' => '.owl-carousel .owl-dots .owl-dot',
                ),
                'ocp_control_active' => array(
                    'label' => __('Active Control', DPOCP_NAME),
                    'selector' => '.owl-carousel .owl-dots .owl-dot.active',
                ),
                'ocp_item_image' => array(
                    'label' => __('Image', DPOCP_NAME),
                    'selector' => 'img.dp_oc_image_thumb',
                ),
                'ocp_item_image_title' => array(
                    'label' => __('Image Title', DPOCP_NAME),
                    'selector' => '.dp_oc_image_title',
                ),
                'ocp_item_image_content' => array(
                    'label' => __('Image Content', DPOCP_NAME),
                    'selector' => '.dp_oc_image_content',
                ),
                'ocp_hash_container' => array(
                    'label' => __('Navigation Thumbnail Container', DPOCP_NAME),
                    'selector' => '.dp_ocp_hash_container',
                ),
                'ocp_hash_image' => array(
                    'label' => __('Navigation Thumbnail Images', DPOCP_NAME),
                    'selector' => '.dp_ocp_hash_image',
                ),
            );
        }

        function get_fields() {
            $fields = array(
                'show_arrow' => array(
                    'label' => __('Arrows', DPOCP_NAME),
                    'type' => 'yes_no_button',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => __('No', DPOCP_NAME),
                        'on' => __('Yes', DPOCP_NAME),
                    ),
                    'default' => 'on',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'elements',
                    'affects' => array('arrow_size', 'arrow_color', 'items_per_slide'),
                    'description' => __('This setting allows you to turn the navigation arrows on or off. Arrows will only display if the number of available posts exceeds the number of thumbnails set to display per slide.', DPOCP_NAME),
                ),
                'items_per_slide' => array(
                    'label' => __('Items Per Slide Action', DPOCP_NAME),
                    'type' => 'text',
                    'option_category' => 'configuration',
                    'default' => '1',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'elements',
                    'default_show_if' => 'on',
                    'description' => __('Number of posts to slide left or right when clicking the arrows.', DPOCP_NAME),
                ),
                'show_control' => array(
                    'label' => __('Controls', DPOCP_NAME),
                    'type' => 'yes_no_button',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => __('No', DPOCP_NAME),
                        'on' => __('Yes', DPOCP_NAME),
                    ),
                    'default' => 'on',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'elements',
                    'affects' => array('control_size', 'control_color'),
                    'description' => __('Turn navigation controls on or off. Controls will only display if the number of available posts exceeds the number of thumbnails set to display.', DPOCP_NAME),
                ),
                'items_per_dot' => array(
                    'label' => __('Items Per Control Action', DPOCP_NAME),
                    'type' => 'text',
                    'option_category' => 'configuration',
                    'default' => '3',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'elements',
                    'default_show_if' => 'on',
                    'description' => __('Number of posts to slide left or right when clicking the control dots. Disabled if Center option is turned on.', DPOCP_NAME),
                ),
                'use_hash_thumbnail' => array(
                    'label' => __('Use Navigation Thumbnail Images', DPOCP_NAME),
                    'type' => 'yes_no_button',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => __('No', DPOCP_NAME),
                        'on' => __('Yes', DPOCP_NAME),
                    ),
                    'default' => 'off',
                    'affects' => array('hash_thumbnail_align', 'hash_thumbnail_size',),
                    'tab_slug' => 'general',
                    'toggle_slug' => 'elements',
                    'description' => __('Use navigation thumbnails images instead of controls.', DPOCP_NAME),
                ),
                'hash_thumbnail_align' => array(
                    'label' => __('Navigation Thumbnail Alignment', DPOCP_NAME),
                    'type' => 'select',
                    'option_category' => 'configuration',
                    'options' => array(
                        'dpoc-align-center' => __('Center', DPOCP_NAME),
                        'dpoc-align-right' => __('Right', DPOCP_NAME),
                        'dpoc-align-left' => __('Left', DPOCP_NAME),
                    ),
                    'default' => 'dpoc-align-center',
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'thumbnail_nav',
                    'description' => __('Navigation thumbnails image alignment.', DPOCP_NAME),
                ),
                'hash_thumbnail_size' => array(
                    'label' => __('Navigation Thumbnail Size', DPOCP_NAME),
                    'type' => 'select',
                    'default' => 'thumbnail',
                    'option_category' => 'configuration',
                    'options' => $this->image_sizes,
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'thumbnail_nav',
                    'description' => __('Navigation thumbnails image source size. You can further adjust the image size using CSS in the Navigation Thumbnail Image box in the Advanced tab.', DPOCP_NAME),
                ),
                'arrow_color' => array(
                    'label' => __('Arrows color', DPOCP_NAME),
                    'type' => 'color',
                    'option_category' => 'configuration',
                    'custom_color' => true,
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'arrows',
                ),
                'arrow_size' => array(
                    'label' => __('Arrows size', DPOCP_NAME),
                    'type' => 'select',
                    'option_category' => 'configuration',
                    'options' => array(
                        '2em' => __('Small', DPOCP_NAME),
                        '4em' => __('Medium', DPOCP_NAME),
                        '6em' => __('Large', DPOCP_NAME),
                    ),
                    'default' => '4em',
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'arrows',
                ),
                'control_color' => array(
                    'label' => __('Control color', DPOCP_NAME),
                    'type' => 'color',
                    'option_category' => 'configuration',
                    'custom_color' => true,
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'controls',
                ),
                'control_size' => array(
                    'label' => __('Control size', DPOCP_NAME),
                    'type' => 'select',
                    'option_category' => 'configuration',
                    'options' => array(
                        '8px' => __('Small', DPOCP_NAME),
                        '16px' => __('Medium', DPOCP_NAME),
                        '24px' => __('Large', DPOCP_NAME),
                    ),
                    'default' => '16px',
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'controls',
                ),
                'behavior' => array(
                    'label' => __('Carousel Behavior', DPOCP_NAME),
                    'type' => 'select',
                    'option_category' => 'configuration',
                    'options' => array(
                        'loop' => __('Loop', DPOCP_NAME),
                        'rewind' => __('Rewind', DPOCP_NAME),
                        'linear' => __('Linear', DPOCP_NAME),
                    ),
                    'default' => 'loop',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'elements',
                    'description' => __('Choose whether carousel should advance in an infinite loop, jump to first item when it reaches the end, or stop when it reaches the end.', DPOCP_NAME),
                ),
                'direction' => array(
                    'label' => __('Carousel Direction', DPOCP_NAME),
                    'type' => 'select',
                    'option_category' => 'configuration',
                    'options' => array(
                        'rtl' => __('Right to left', DPOCP_NAME),
                        'ltr' => __('Left to right', DPOCP_NAME),
                    ),
                    'default' => 'rtl',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'elements',
                    'description' => __('Choose which direction the carousel should advance.', DPOCP_NAME),
                ),
                'center' => array(
                    'label' => __('Center', DPOCP_NAME),
                    'type' => 'yes_no_button',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => __('No', DPOCP_NAME),
                        'on' => __('Yes', DPOCP_NAME),
                    ),
                    'default' => 'off',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'elements',
                    'description' => __('First carousel item will always start in the center of the carousel. This option must be turned on to center the clicked image in the carousel when using Navigation Thumbnails.', DPOCP_NAME),
                ),
                'auto_width' => array(
                    'label' => __('Auto Width Images', DPOCP_NAME),
                    'type' => 'yes_no_button',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => __('No', DPOCP_NAME),
                        'on' => __('Yes', DPOCP_NAME),
                    ),
                    'default' => 'off',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'elements',
                    'description' => __('Turn this option on if you want to display images in the size set in the Thumbnail Size option in the Design tab. Leave this option off if you want images to display evenly and adhere to Thumbnails Per Slide option in the Design tab.', DPOCP_NAME),
                ),
                'slide_auto' => array(
                    'label' => __('Automatic Rotate', DPOCP_NAME),
                    'type' => 'yes_no_button',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => esc_html__("No", DPOCP_NAME),
                        'on' => esc_html__('Yes', DPOCP_NAME),
                    ),
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'animation',
                    'affects' => array('slide_speed', 'slide_hover', 'animation_speed'),
                    'default' => 'on',
                    'description' => __('If you would like the carousel to rotate automatically, without the visitor having to click the next button, enable this option and then adjust the rotation speed below if desired.', DPOCP_NAME),
                ),
                'slide_speed' => array(
                    'label' => __('Automatic Rotate Speed (in ms)', DPOCP_NAME),
                    'type' => 'text',
                    'option_category' => 'configuration',
                    'default' => '5000',
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'animation',
                    'description' => __('Here you can designate how fast the carousel rotates between each slide, if \'Automatic Rotate\' option is enabled above. The higher the number the longer the pause between each rotation.', DPOCP_NAME),
                ),
                'animation_speed' => array(
                    'label' => __('Auto Rotate Animation Speed (in ms)', DPOCP_NAME),
                    'type' => 'text',
                    'option_category' => 'configuration',
                    'default' => '500',
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'animation',
                    'description' => __('Here you can designate how long it takes for the carousel to complete a rotation. The higher the number the slower the animation.', DPOCP_NAME),
                ),
                'arrows_speed' => array(
                    'label' => __('Arrow Animation Speed (in ms)', DPOCP_NAME),
                    'type' => 'text',
                    'option_category' => 'configuration',
                    'default' => '500',
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'animation',
                    'description' => __('Here you can designate how long it takes for the carousel to complete a rotation when the arrows are clicked. The higher the number the slower the animation.', DPOCP_NAME),
                ),
                'dots_speed' => array(
                    'label' => __('Control Animation Speed (in ms)', DPOCP_NAME),
                    'type' => 'text',
                    'option_category' => 'configuration',
                    'default' => '500',
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'animation',
                    'description' => __('Here you can designate how long it takes for the carousel to complete a rotation when the control dots are clicked. The higher the number the slower the animation.', DPOCP_NAME),
                ),
                'slide_hover' => array(
                    'label' => __('Pause on Hover', DPOCP_NAME),
                    'type' => 'yes_no_button',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => esc_html__("No", DPOCP_NAME),
                        'on' => esc_html__('Yes', DPOCP_NAME),
                    ),
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'animation',
                    'default' => 'off',
                    'description' => __('Pause carousel rotation when user hovers over the slides', DPOCP_NAME),
                ),
                'thumbnail_size' => array(
                    'label' => __('Thumbnail Size', DPOCP_NAME),
                    'type' => 'select',
                    'default' => 'et-pb-portfolio-image',
                    'option_category' => 'configuration',
                    'options' => $this->image_sizes,
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'thumbnail'
                ),
                'number_thumb' => array(
                    'label' => __('Thumbnails to Display', DPOCP_NAME),
                    'type' => 'range',
                    'mobile_options' => true,
                    'validate_unit' => false,
                    'default_unit' => '',
                    'option_category' => 'configuration',
                    'range_settings' => array(
                        'min' => '1',
                        'max' => '20',
                        'step' => '1',
                    ),
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'thumbnail',
                    'description' => 'This setting determines how many carousel items will be initially displayed on the screen.'
                ),
                'number_thumb_tablet' => array(
                    'type' => 'skip',
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'thumbnail'
                ),
                'number_thumb_phone' => array(
                    'type' => 'skip',
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'thumbnail'
                ),
                'number_thumb_last_edited' => array(
                    'type' => 'skip',
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'thumbnail'
                ),
                'item_margin' => array(
                    'label' => esc_html__('Item Margin', DPOCP_NAME),
                    'type' => 'text',
                    'option_category' => 'configuration',
                    'description' => esc_html__('Define the margin for each item at the carousel. Leave blank for default ( 8 ) ', DPOCP_NAME),
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'thumbnail',
                ),
                'lazy_load' => array(
                    'label' => __('Lazy Load', DPOCP_NAME),
                    'type' => 'yes_no_button',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => __('No', DPOCP_NAME),
                        'on' => __('Yes', DPOCP_NAME),
                    ),
                    'default' => 'off',
                    'tab_slug' => 'general',
                    'toggle_slug' => 'elements',
                    'description' => __('Turn this option on if you want images to load on demand. This option may not be compatible with some caching and optimizing plugins and should be turned off if you experience any display issues with the carousel.', DPOCP_NAME),
                ),
                'lightbox_gallery' => array(
                    'label' => __('Lightbox Gallery', DPOCP_NAME),
                    'type' => 'yes_no_button',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => __('No', DPOCP_NAME),
                        'on' => __('Yes', DPOCP_NAME),
                    ),
                    'default' => 'off',
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'onclick',
                    'description' => __('Turn this option on if you want the lightbox to display all images from the carousel in a gallery. Leave this option off if you only want the clicked image to display in the lightbox.', DPOCP_NAME),
                ),
                'module_bg' => array(
                    'label' => __('Items Background', DPOCP_NAME),
                    'type' => 'color',
                    'option_category' => 'layout',
                    'custom_color' => true,
                    'tab_slug' => 'general',
                    'toggle_slug' => 'background',
                ),
                'background_layout' => array(
                    'label' => esc_html__('Text Color', DPOCP_NAME),
                    'type' => 'select',
                    'option_category' => 'color_option',
                    'options' => array(
                        'light' => esc_html__('Dark', DPOCP_NAME),
                        'dark' => esc_html__('Light', DPOCP_NAME),
                    ),
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'text',
                    'description' => esc_html__('Here you can choose whether your text should be light or dark. If you are working with a dark background, then your text should be light. If your background is light, then your text should be set to dark.', DPOCP_NAME),
                ),
                'module_id' => array(
                    'label' => __('CSS ID', DPOCP_NAME),
                    'type' => 'text',
                    'option_category' => 'configuration',
                    'tab_slug' => 'custom_css',
                    'toggle_slug' => 'classes',
                    'description' => __('Enter an optional CSS ID to be used for this module. An ID can be used to create custom CSS styling, or to create links to particular sections of your page.', DPOCP_NAME),
                ),
                'module_class' => array(
                    'label' => __('CSS Class', DPOCP_NAME),
                    'type' => 'text',
                    'option_category' => 'configuration',
                    'tab_slug' => 'custom_css',
                    'toggle_slug' => 'classes',
                    'description' => __('Enter optional CSS classes to be used for this module. A CSS class can be used to create custom CSS styling. You can add multiple classes, separated with a space.', DPOCP_NAME),
                ),
                'disabled_on' => array(
                    'label' => __('Disable on', DPOCP_NAME),
                    'type' => 'multiple_checkboxes',
                    'options' => array(
                        'phone' => __('Phone', DPOCP_NAME),
                        'tablet' => __('Tablet', DPOCP_NAME),
                        'desktop' => __('Desktop', DPOCP_NAME),
                    ),
                    'additional_att' => 'disable_on',
                    'option_category' => 'configuration',
                    'description' => __('This will disable the module on selected devices', DPOCP_NAME),
                    'tab_slug' => 'custom_css',
                    'toggle_slug' => 'visibility',
                ),
                'admin_label' => array(
                    'label' => __('Admin Label', DPOCP_NAME),
                    'type' => 'text',
                    'toggle_slug' => 'admin_label',
                    'description' => __('This will change the label of the module in the builder for easy identification.', DPOCP_NAME),
                ),
            );
            return $fields;
        }

        function pre_shortcode_content() {
            self::$img_data = array();
            global $dp_oc_thumb_size_selected;
            global $lightbox_gallery;
            global $dp_gallery_images;
            global $dp_auto_width;
            global $dp_lazy_load;
            $dp_oc_thumb_size_selected = $this->shortcode_atts['thumbnail_size'];
            $lightbox_gallery = $this->shortcode_atts['lightbox_gallery'];
            $dp_auto_width = $this->shortcode_atts['auto_width'];
            $dp_lazy_load = $this->shortcode_atts['lazy_load'];
            $dp_gallery_images = 0;
        }

        static function add_child_data($img_data) {
            $id = $img_data['image_id'];
            if ($id !== 'No Image') {
                self::$img_data[] = $id;
                $img_id = count(self::$img_data) - 1;
                return $img_id;
            } else {
                return '';
            }
        }

        function shortcode_callback($atts, $content = null, $function_name) {
            $module_id = $this->shortcode_atts['module_id'];
            $module_class = $this->shortcode_atts['module_class'];
            $item_margin = $this->shortcode_atts['item_margin'];
            $show_arrow = $this->shortcode_atts['show_arrow'];
            $arrow_size = $this->shortcode_atts['arrow_size'];
            $arrow_color = $this->shortcode_atts['arrow_color'];
            $show_control = $this->shortcode_atts['show_control'];
            $use_hash_thumbnail = $this->shortcode_atts['use_hash_thumbnail'];
            $hash_thumbnail_align = $this->shortcode_atts['hash_thumbnail_align'];
            $hash_thumbnail_size = $this->shortcode_atts['hash_thumbnail_size'];
            $control_size = $this->shortcode_atts['control_size'];
            $control_color = $this->shortcode_atts['control_color'];
            $behavior = $this->shortcode_atts['behavior'];
            $direction = $this->shortcode_atts['direction'];
            $center = $this->shortcode_atts['center'];
            $items_per_dot = $this->shortcode_atts['items_per_dot'];
            $items_per_slide = $this->shortcode_atts['items_per_slide'];
            $slide_auto = $this->shortcode_atts['slide_auto'];
            $slide_speed = $this->shortcode_atts['slide_speed'];
            $animation_speed = $this->shortcode_atts['animation_speed'];
            $arrows_speed = $this->shortcode_atts['arrows_speed'];
            $control_speed = $this->shortcode_atts['dots_speed'];
            $slide_hover = $this->shortcode_atts['slide_hover'];
            $number_thumb = $this->shortcode_atts['number_thumb'];
            $number_thumb_tablet = $this->shortcode_atts['number_thumb_tablet'];
            $number_thumb_phone = $this->shortcode_atts['number_thumb_phone'];
            $number_thumb_last_edited = $this->shortcode_atts['number_thumb_last_edited'];
            $module_bg = $this->shortcode_atts['module_bg'];
            $background_layout = $this->shortcode_atts['background_layout'];
            $auto_width = $this->shortcode_atts['auto_width'];
            $lazy_load = $this->shortcode_atts['lazy_load'];

            wp_enqueue_style('dp-ocp-owl-carousel');
            wp_enqueue_style('dp-ocp-custom');
            wp_enqueue_script('dp-ocp-owl-carousel');
            wp_enqueue_script('dp-ocp-custom');

            if (et_pb_get_responsive_status($number_thumb_last_edited)) {
                if ($number_thumb_tablet === '') {
                    $number_thumb_tablet = $number_thumb;
                }
                if ($number_thumb_phone === '') {
                    $number_thumb_phone = $number_thumb_tablet;
                }
            }

            if ('' !== $module_bg) {
                ET_Builder_Element::set_style($function_name, array(
                    'selector' => '%%order_class%%.et_pb_dp_oc .dp_oc_item',
                    'declaration' => sprintf(
                            'background-color: %1$s;', esc_html($module_bg)
                    ),
                ));
            }

            if ('' !== $arrow_color) {
                ET_Builder_Element::set_style($function_name, array(
                    'selector' => '%%order_class%%.et_pb_dp_oc .owl-carousel .owl-nav',
                    'declaration' => sprintf(
                            'color: %1$s;', esc_html($arrow_color)
                    ),
                ));
            }

            if ('' !== $control_color) {
                ET_Builder_Element::set_style($function_name, array(
                    'selector' => '%%order_class%%.et_pb_dp_oc .owl-carousel .owl-dots .owl-dot',
                    'declaration' => sprintf(
                            'background-color: %1$s;', esc_html($control_color)
                    ),
                ));
            }

            if ('' !== $control_size) {
                ET_Builder_Element::set_style($function_name, array(
                    'selector' => '%%order_class%%.et_pb_dp_oc .owl-carousel .owl-dots .owl-dot',
                    'declaration' => sprintf(
                            'width: %1$s; height: %1$s', esc_html($control_size)
                    ),
                ));
            }

            $hash_thumbnail_array = self::$img_data;
            $hash_thumbnail_counter = 0;
            if ($use_hash_thumbnail === 'on') {
                $show_control = 'off';
            }

            $module_class = ET_Builder_Element::add_module_order_class($module_class, $function_name);
            $class = " et_pb_module et_pb_bg_layout_{$background_layout}";

            ob_start();
            ?>
            <div class="owl-carousel" data-rotate='<?php echo $slide_auto; ?>' data-speed='<?php echo $slide_speed; ?>' data-hover='<?php echo $slide_hover; ?>' data-arrow='<?php echo $show_arrow; ?>' data-control='<?php echo $show_control; ?>' data-items="<?php echo $number_thumb; ?>" data-items-tablet="<?php echo $number_thumb_tablet; ?>" data-items-phone="<?php echo $number_thumb_phone; ?>" data-margin="<?php echo $item_margin; ?>" data-behaviour="<?php echo $behavior; ?>" data-direction="<?php echo $direction; ?>" data-center="<?php echo $center; ?>" data-items-per-dot="<?php echo $items_per_dot; ?>" data-items-per-slide="<?php echo $items_per_slide; ?>" data-arrow-size="<?php echo $arrow_size; ?>" data-use-hash="<?php echo $use_hash_thumbnail; ?>" data-custom="yes" data-module="<?php echo trim($module_class); ?>" data-use-auto-width="<?php echo $auto_width; ?>" data-animation-speed="<?php echo $animation_speed; ?>" data-arrows-speed="<?php echo $arrows_speed; ?>" data-controls-speed="<?php echo $control_speed; ?>" data-lazy="<?php echo $lazy_load; ?>">
                <?php
                echo $this->shortcode_content;
                ?>
            </div>
            <?php
            $content_carousel = ob_get_contents();
            ob_end_clean();

            $hash_output = '';
            if ($use_hash_thumbnail === 'on') {
                $hash_output = '<div class="dp_ocp_hash_container ' . $hash_thumbnail_align . '">';
                foreach ($hash_thumbnail_array as $key => $value) {
                    $hash_output .= '<a href="#' . trim($module_class) . '_' . $key . '" ><img class="dp_ocp_hash_image" src="' . wp_get_attachment_image_url($value, $hash_thumbnail_size) . '"></a> ';
                }
                $hash_output .= '</div>';
            }

            $output = sprintf(
                    '<div%3$s class="et_pb_dp_oc%2$s%4$s" >%1$s%5$s</div><!-- .et_pb_dp_oc-->', $content_carousel, esc_attr($class), ( '' !== $module_id ? sprintf(' id="%1$s"', esc_attr($module_id)) : ''), ( '' !== $module_class ? sprintf(' %1$s', esc_attr($module_class)) : ''), $hash_output);
            return $output;
        }

    }

    new ET_Builder_Module_DP_OC_Custom;

    class ET_Builder_Module_DP_OC_Custom_Item extends ET_Builder_Module {

        function init() {
            $this->name = esc_html__('Content', DPOCP_NAME);
            $this->slug = 'et_pb_dp_oc_custom_item';
            $this->custom_css_tab = false;
            $this->type = 'child';
            $this->child_title_var = 'admin_title';
            $this->whitelisted_fields = array(
                'upload_image',
                'show_in_lightbox',
                'url',
                'url_new_window',
                'image_title',
                'image_content',
                'use_original',
                'image_alt_text',
                'image_title_text',
                'admin_title'
            );
            $this->fields_defaults = array(
                'use_original' => array('off'),
            );
            $this->advanced_setting_title_text = __('New Image', DPOCP_NAME);
            $this->settings_text = __('Image Settings', DPOCP_NAME);
            $this->main_css_element = '%%order_class%%.et_pb_dp_oc_custom_item ';
        }

        function get_fields() {
            $fields = array(
                'upload_image' => array(
                    'label' => __('Image URL', DPOCP_NAME),
                    'type' => 'upload',
                    'option_category' => 'basic_option',
                    'upload_button_text' => __('Upload an image', DPOCP_NAME),
                    'choose_text' => __('Choose an Image', DPOCP_NAME),
                    'update_text' => __('Set As Image', DPOCP_NAME),
                    'description' => __('Upload your desired image, or type in the URL to the image you would like to display.', DPOCP_NAME),
                ),
                'show_in_lightbox' => array(
                    'label' => esc_html__('Open in Lightbox', DPOCP_NAME),
                    'type' => 'yes_no_button',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => esc_html__("No", DPOCP_NAME),
                        'on' => esc_html__('Yes', DPOCP_NAME),
                    ),
                    'affects' => array(
                        'url',
                        'url_new_window',
                    ),
                    'description' => esc_html__('Here you can choose whether or not the image should open in Lightbox. Note: if you select to open the image in Lightbox, url options below will be ignored.', DPOCP_NAME),
                ),
                'use_original' => array(
                    'label' => esc_html__('Use Original Image', DPOCP_NAME),
                    'type' => 'yes_no_button',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => esc_html__("No", DPOCP_NAME),
                        'on' => esc_html__('Yes', DPOCP_NAME),
                    ),
                    'description' => esc_html__('Load the original image instead of looking for image thumbnail. If images are small and do not have a thumbnail in the size selected, this option will load the full size of the image.', DPOCP_NAME),
                ),
                'url' => array(
                    'label' => esc_html__('Link URL', DPOCP_NAME),
                    'type' => 'text',
                    'option_category' => 'basic_option',
                    'depends_show_if' => 'off',
                    'affects' => array(
                        'use_overlay',
                    ),
                    'description' => esc_html__('If you would like your image to be a link, input your destination URL here. No link will be created if this field is left blank.', DPOCP_NAME),
                ),
                'url_new_window' => array(
                    'label' => esc_html__('Url Opens', DPOCP_NAME),
                    'type' => 'select',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => esc_html__('In The Same Window', DPOCP_NAME),
                        'on' => esc_html__('In The New Tab', DPOCP_NAME),
                    ),
                    'depends_show_if' => 'off',
                    'description' => esc_html__('Here you can choose whether or not your link opens in a new window', DPOCP_NAME),
                ),
                'image_title' => array(
                    'label' => __('Image Title', DPOCP_NAME),
                    'type' => 'text',
                    'description' => __('Image title displays below image', DPOCP_NAME),
                ),
                'content' => array(
                    'label' => __('Image Content', DPOCP_NAME),
                    'type' => 'tiny_mce',
                    'description' => __('Image content displays below image title', DPOCP_NAME),
                ),
                'image_alt_text' => array(
                    'label' => __('Image Alternative Text', DPOCP_NAME),
                    'type' => 'text',
                    'option_category' => 'basic_option',
                    'description' => __('This defines the HTML ALT text. A short description of your image can be placed here.', DPOCP_NAME),
                ),
                'image_title_text' => array(
                    'label' => __('Image Title Text', DPOCP_NAME),
                    'type' => 'text',
                    'option_category' => 'basic_option',
                    'description' => __('This defines the HTML Title text.', DPOCP_NAME),
                ),
                'admin_title' => array(
                    'label' => esc_html__('Admin Label', DPOCP_NAME),
                    'type' => 'text',
                    'description' => esc_html__('This will change the label of the image in the builder for easy identification.', DPOCP_NAME),
                ),
            );
            return $fields;
        }

        function shortcode_callback($atts, $content = null, $function_name) {
            $upload_image = $this->shortcode_atts['upload_image'];
            $show_in_lightbox = $this->shortcode_atts['show_in_lightbox'];
            $url = $this->shortcode_atts['url'];
            $url_new_window = $this->shortcode_atts['url_new_window'];
            $image_title = $this->shortcode_atts['image_title'];
            $image_content = $this->shortcode_content;
            $use_original = $this->shortcode_atts['use_original'];
            $image_alt_text = $this->shortcode_atts['image_alt_text'];
            $image_title_text = $this->shortcode_atts['image_title_text'];

            global $dp_oc_thumb_size_selected;
            global $dp_image_sizes;
            $selected_size = $dp_oc_thumb_size_selected;
            global $lightbox_gallery;
            global $dp_gallery_images;
            global $dp_auto_width;
            global $dp_lazy_load;
            $image_size_width = '';
            $selected_size_width = explode('x', $dp_image_sizes[$selected_size])[0];

            if (!empty($upload_image)) {
                global $wpdb;
                $attachment_id = $wpdb->get_var($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE post_type='attachment' AND guid='%s';", $upload_image));
                if (empty($attachment_id)) {
                    $image_file_name = '%' . array_reverse(explode('/', $upload_image))[0];
                    $attachment_id = $wpdb->get_var($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE post_type='attachment' AND guid LIKE '%s';", $image_file_name));
                    $this->shortcode_atts['image_id'] = $attachment_id;
                } else {
                    $this->shortcode_atts['image_id'] = $attachment_id;
                }
                if ($dp_auto_width === 'on') {
                    if ($use_original === 'on') {
                        $image_size_width = 'style="width: ' . wp_get_attachment_metadata($attachment_id)['width'] . 'px"';
                    } else {
                        $image_size_width = wp_get_attachment_metadata($attachment_id)['sizes'][$selected_size]['width'];
                        if ($image_size_width !== '') {
                            $image_size_width = 'style="width: ' . $image_size_width . 'px"';
                        } else {
                            $image_size_width = 'style="width: ' . $selected_size_width . 'px"';
                        }
                    }
                }
            } else {
                $this->shortcode_atts['image_id'] = 'No Image';
                if ($dp_auto_width === 'on') {
                    $image_size_width = 'style="width: ' . $selected_size_width . 'px"';
                }
            }

            $this->shortcode_atts['img_id'] = ET_Builder_Module_DP_OC_Custom::add_child_data($this->shortcode_atts);

            ob_start();
            ?>
            <div class="dp_oc_item" <?php echo ' data-item="_' . $this->shortcode_atts['img_id'] . '" ' . $image_size_width . ' '; ?>>
                <?php
                if (!empty($upload_image)) {

                    if ($use_original === 'on') {
                        $image_src = $upload_image;
                    } else {
                        $image_src = wp_get_attachment_image_url($this->shortcode_atts['image_id'], $selected_size);
                    }

                    if ($url_new_window === "on") {
                        $target = "_blank";
                    } else {
                        $target = "";
                    }

                    if ($show_in_lightbox === "on") {
                        echo sprintf('<a href="%1$s" class="dp_ocp_lightbox_image" ><img class="dp_oc_image_thumb %7$s" %8$ssrc="%2$s" title="%6$s" alt="%3$s" data-lightbox-gallery="%4$s" data-gallery-image="%5$s" ></a>', $upload_image, $image_src, $image_alt_text, ($lightbox_gallery === 'on') ? 'on' : 'off', $dp_gallery_images++, $image_title_text, ($dp_lazy_load === 'on') ? 'owl-lazy' : '', ($dp_lazy_load === 'on') ? 'data-' : '');
                    } elseif (!empty($url)) {
                        echo sprintf('<a href="%1$s" target="%2$s"><img class="dp_oc_image_thumb %6$s" %7$ssrc="%3$s" title="%4$s" alt="%5$s"></a>', $url, $target, $image_src, $image_title_text, $image_alt_text, ($dp_lazy_load === 'on') ? 'owl-lazy' : '', ($dp_lazy_load === 'on') ? 'data-' : '');
                    } else {
                        echo sprintf('<img class="dp_oc_image_thumb %4$s" %5$ssrc="%1$s" title="%2$s" alt="%3$s">', $image_src, $image_title_text, $image_alt_text, ($dp_lazy_load === 'on') ? 'owl-lazy' : '', ($dp_lazy_load === 'on') ? 'data-' : '');
                    }
                }

                if (!empty($image_title)) {
                    echo sprintf('<h2 class="dp_oc_image_title">%1$s</h2>', $image_title);
                }
                if (!empty($image_content)) {
                    echo sprintf('<div class="dp_oc_image_content">%1$s</div>', $image_content);
                }
                ?>
            </div>
            <?php
            $output = ob_get_contents();
            ob_get_clean();
            return $output;
        }

    }

    new ET_Builder_Module_DP_OC_Custom_Item;
}
?>