<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://www.diviplugins.com
 * @since 1.5
 *
 * @package    Dp_Owl_Carousel_Pro
 * @subpackage Dp_Owl_Carousel_Pro/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current version of the plugin.
 *
 * @since 1.5
 * @package    Dp_Owl_Carousel_Pro
 * @subpackage Dp_Owl_Carousel_Pro/includes
 * @author     DiviPlugins <support@diviplugins.com>
 */
class Dp_Owl_Carousel_Pro {

    /**
     * The loader that's responsible for maintaining and registering all hooks that power the plugin.
     *
     * @since 1.5
     * @access   protected
     * @var      Dp_Owl_Carousel_Pro_Loader    $loader    Maintains and registers all hooks for the plugin.
     */
    protected $loader;

    /**
     * The unique identifier of this plugin.
     *
     * @since 1.5
     * @access   protected
     * @var      string    $plugin_name    The string used to uniquely identify this plugin.
     */
    protected $plugin_name;

    /**
     * The current version of the plugin.
     *
     * @since 1.5
     * @access   protected
     * @var      string    $version    The current version of the plugin.
     */
    protected $version;

    /**
     * Define the core functionality of the plugin.
     *
     * Set the plugin name and the plugin version that can be used throughout the plugin.
     * Load the dependencies, define the locale, and set the hooks for the admin area and
     * the public-facing side of the site.
     *
     * @since 1.5
     */
    public function __construct() {
        if (defined('DPOCP_VERSION')) {
            $this->version = DPOCP_VERSION;
        } else {
            $this->version = '1.0.0';
        }
        if (defined('DPOCP_NAME')) {
            $this->plugin_name = DPOCP_NAME;
        } else {
            $this->plugin_name = 'dp-owl-carousel-pro';
        }
        $this->load_dependencies();
        $this->set_locale();
        $this->define_admin_hooks();
        $this->define_public_hooks();
    }

    /**
     * Load the required dependencies for this plugin.
     *
     * Include the following files that make up the plugin:
     *
     * - Dp_Owl_Carousel_Pro_Loader. Orchestrates the hooks of the plugin.
     * - Dp_Owl_Carousel_Pro_i18n. Defines internationalization functionality.
     * - Dp_Owl_Carousel_Pro_Admin. Defines all hooks for the admin area.
     * - Dp_Owl_Carousel_Pro_Public. Defines all hooks for the public side of the site.
     * - Dp_Owl_Carousel_Pro_Updater. Defines de automatic updates class provide by EDD.
     *
     * Create an instance of the loader which will be used to register the hooks
     * with WordPress.
     *
     * @since 1.5
     * @access   private
     */
    private function load_dependencies() {

        /**
         * The class responsible for orchestrating the actions and filters of the core plugin.
         */
        require_once DPOCP_DIR . 'includes/class-dp-owl-carousel-pro-loader.php';

        /**
         * The class responsible for defining internationalization functionality of the plugin.
         */
        require_once DPOCP_DIR . 'includes/class-dp-owl-carousel-pro-i18n.php';

        /**
         * The class responsible for automatic updates.
         */
        require_once DPOCP_DIR . 'includes/class-dp-owl-carousel-pro-updater.php';
        require_once DPOCP_DIR . 'includes/class-dp-owl-carousel-pro-utils.php';

        /**
         * The class responsible for defining all actions that occur in the admin area.
         */
        require_once DPOCP_DIR . 'admin/class-dp-owl-carousel-pro-admin.php';

        /**
         * The class responsible for defining all actions that occur in the public-facing side of the site.
         */
        require_once DPOCP_DIR . 'public/class-dp-owl-carousel-pro-public.php';

        $this->loader = new Dp_Owl_Carousel_Pro_Loader();
    }

    /**
     * Define the locale for this plugin for internationalization.
     *
     * Uses the Dp_Owl_Carousel_Pro_i18n class in order to set the domain and to register the hook with WordPress.
     *
     * @since 1.5
     * @access   private
     */
    private function set_locale() {

        $plugin_i18n = new Dp_Owl_Carousel_Pro_i18n();

        $this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');
    }

    /**
     * Register all of the hooks related to the admin area functionality
     * of the plugin.
     *
     * @since 1.5
     * @access   private
     */
    private function define_admin_hooks() {
        $plugin_admin = new Dp_Owl_Carousel_Pro_Admin($this->get_plugin_name(), $this->get_version());
        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_styles');
        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts');
        /*
         * License activation hooks related
         */
        $this->loader->add_action('admin_init', $plugin_admin, 'init_plugin_updater', 0);
        $this->loader->add_action('admin_menu', $plugin_admin, 'add_license_menu_page');
        $this->loader->add_action('admin_init', $plugin_admin, 'register_license_option');
        $this->loader->add_action('admin_init', $plugin_admin, 'activate_license');
        $this->loader->add_action('admin_init', $plugin_admin, 'deactivate_license');
        $this->loader->add_action('admin_notices', $plugin_admin, 'admin_notice_license_result');
        if (get_option('dp_ocp_license_status') !== 'valid') {
            $this->loader->add_action('admin_notices', $plugin_admin, 'admin_notice_license_activation');
        }
        include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
        if (is_plugin_active('dp-owl-carousel/dp_owl_carousel.php')) {
            $this->loader->add_action('admin_notices', $plugin_admin, 'admin_notice_error');
        } else {
            $this->loader->add_action('admin_notices', $plugin_admin, 'admin_notice_welcome');
        }
        if (get_transient('ocp_need_to_clear_local_storage')) {
            $this->loader->add_action('admin_head', $plugin_admin, 'remove_from_local_storage');
            set_transient('ocp_need_to_clear_local_storage', false);
        }
        $this->loader->add_action('created_term', $plugin_admin, 'add_term', 10, 3);
        $this->loader->add_action('delete_term', $plugin_admin, 'delete_term', 10, 1);
        $this->loader->add_action('edited_terms', $plugin_admin, 'edit_term', 10, 2);
        $this->loader->add_action('registered_post_type', $plugin_admin, 'add_cpt', 10, 2);
        $this->loader->add_action('after_setup_theme', $plugin_admin, 'register_img_size');
        $this->loader->add_filter('image_size_names_choose', $plugin_admin, 'custom_size');
        $this->loader->add_action('wp_ajax_dp_ocp_ajax_reload_cpt', $plugin_admin, 'ajax_reload_cpt');
        $this->loader->add_action('wp_ajax_dp_ocp_ajax_get_cpt_and_categories', $plugin_admin, 'ajax_get_cpt_and_categories');
        $this->loader->add_action('et_builder_ready', $plugin_admin, 'include_the_modules');
        $this->loader->add_filter('et_set_default_values', $plugin_admin, 'merge_dp_ocp_defaults');
    }

    /**
     * Register all of the hooks related to the public-facing functionality
     * of the plugin.
     *
     * @since 1.5
     * @access   private
     */
    private function define_public_hooks() {
        $plugin_public = new Dp_Owl_Carousel_Pro_Public($this->get_plugin_name(), $this->get_version());
        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_styles');
        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_scripts');
    }

    /**
     * Run the loader to execute all of the hooks with WordPress.
     *
     * @since 1.5
     */
    public function run() {
        $this->loader->run();
    }

    /**
     * The name of the plugin used to uniquely identify it within the context of WordPress and to define internationalization functionality.
     *
     * @since     1.5
     * @return    string    The name of the plugin.
     */
    public function get_plugin_name() {
        return $this->plugin_name;
    }

    /**
     * The reference to the class that orchestrates the hooks with the plugin.
     *
     * @since     1.5
     * @return    Dp_Owl_Carousel_Pro_Loader    Orchestrates the hooks of the plugin.
     */
    public function get_loader() {
        return $this->loader;
    }

    /**
     * Retrieve the version number of the plugin.
     *
     * @since     1.5
     * @return    string    The version number of the plugin.
     */
    public function get_version() {
        return $this->version;
    }

}
