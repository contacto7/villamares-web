<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.diviplugins.com
 * @since 1.5
 *
 * @package    Dp_Owl_Carousel_Pro
 * @subpackage Dp_Owl_Carousel_Pro/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since 1.5
 * @package    Dp_Owl_Carousel_Pro
 * @subpackage Dp_Owl_Carousel_Pro/includes
 * @author     DiviPlugins <support@diviplugins.com>
 */
class Dp_Owl_Carousel_Pro_Deactivator {

    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     * @since    1.5
     */
    public static function deactivate() {
        set_transient('ocp_need_to_clear_local_storage', true);
        remove_image_size('dp-ocp-square-thumb');
        Dp_Owl_Carousel_Pro_Utils::remove_table();
    }

}
