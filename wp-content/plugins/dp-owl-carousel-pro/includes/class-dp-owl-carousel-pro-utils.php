<?php

class Dp_Owl_Carousel_Pro_Utils {

    static public function create_table() {
        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . "dp_available_categories";
        if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            $sql = "CREATE TABLE $table_name (
                    id bigint(20) auto_increment NOT NULL,
                    term_id mediumint(9) NOT NULL,
                    name tinytext NOT NULL,
                    pt tinytext NOT NULL,
                    tax tinytext NOT NULL,
                    PRIMARY KEY  (id)
	) $charset_collate;";
            if (!function_exists('dbDelta')) {
                require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            }
            dbDelta($sql);
            self::fill_table();
        }
    }

    static public function remove_table() {
        global $wpdb;
        $table_name = $wpdb->prefix . "dp_available_categories";
        $sql = "DROP TABLE IF EXISTS $table_name;";
        $wpdb->query($sql);
    }

    static public function get_post_types_and_taxonomies() {
        global $sitepress;
        global $polylang;
        if ($sitepress) {
            $sitepress->switch_lang('all');
        }
        $default_post_type = apply_filters('dp_default_post_type', array('post' => 'post'));
        $post_types = get_post_types(array('_builtin' => false, 'public' => true));
        $post_types += $default_post_type;
        foreach ($post_types as $pt) {
            $pt_taxs = get_object_taxonomies($pt);
            if (empty($pt_taxs)) {
                $cats_array[] = array('cat_id' => 'dp_none_taxonomy', 'cat_name' => 'dp_none_taxonomy', 'pt' => $pt, 'tax' => 'dp_none_taxonomy');
            } else {
                foreach ($pt_taxs as $pt_tax) {
                    if ($pt_tax != 'post_tag' && $pt_tax != 'post_format' && get_taxonomy($pt_tax)->show_ui) {
                        if ($sitepress) {
                            remove_filter('get_terms_args', array($sitepress, 'get_terms_args_filter'));
                            remove_filter('get_term', array($sitepress, 'get_term_adjust_id'));
                            remove_filter('terms_clauses', array($sitepress, 'terms_clauses'));
                            $terms = get_terms(array('taxonomy' => $pt_tax, 'hide_empty' => false));
                            add_filter('terms_clauses', array($sitepress, 'terms_clauses'));
                            add_filter('get_term', array($sitepress, 'get_term_adjust_id'));
                            add_filter('get_terms_args', array($sitepress, 'get_terms_args_filter'));
                        } elseif ($polylang) {
                            $pll_languages = pll_languages_list();
                            $string_languages = '';
                            if (is_array($pll_languages)) {
                                foreach ($pll_languages as $language) {
                                    $string_languages .= $language . ",";
                                }
                                $terms = get_terms(array('taxonomy' => $pt_tax, 'hide_empty' => false, 'lang' => $string_languages));
                            } else {
                                $terms = get_terms(array('taxonomy' => $pt_tax, 'hide_empty' => false));
                            }
                        } else {
                            $terms = get_terms(array('taxonomy' => $pt_tax, 'hide_empty' => false));
                        }
                        foreach ($terms as $term) {
                            $cats_array[] = array('cat_id' => $term->term_id, 'cat_name' => $term->name, 'pt' => $pt, 'tax' => $pt_tax);
                        }
                    }
                }
            }
        }
        return $cats_array;
    }

    static public function fill_table() {
        $cat_arrays = self::get_post_types_and_taxonomies();
        if (!empty($cat_arrays)) {
            global $wpdb;
            $table_name = $wpdb->prefix . "dp_available_categories";
            $wpdb->query("TRUNCATE $table_name;");
            foreach ($cat_arrays as $cat) {
                $wpdb->insert(
                        $table_name, array(
                    'term_id' => $cat['cat_id'],
                    'name' => $cat['cat_name'],
                    'pt' => $cat['pt'],
                    'tax' => $cat['tax'],
                        )
                );
            }
        }
    }

    static public function terms_array() {
        global $wpdb;
        $table_name = $wpdb->prefix . "dp_available_categories";
        if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            self::create_table();
        }
        return $wpdb->get_results("SELECT term_id,name,pt,tax FROM $table_name WHERE tax!='dp_none_taxonomy';", ARRAY_A);
    }

    static public function get_post_types() {
        global $wpdb;
        $table_name = $wpdb->prefix . "dp_available_categories";
        if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            self::create_table();
        }
        return $wpdb->get_results("SELECT DISTINCT pt FROM $table_name;", ARRAY_A);
    }

    static public function get_terms() {
        global $wpdb;
        $table_name = $wpdb->prefix . "dp_available_categories";
        if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            self::create_table();
        }
        return $wpdb->get_results("SELECT DISTINCT term_id,name,pt FROM $table_name;", ARRAY_A);
    }

    static public function get_custom_sizes() {
        $options = array();
        global $_wp_additional_image_sizes;
        foreach (get_intermediate_image_sizes() as $_size) {
            if (in_array($_size, array('thumbnail', 'medium', 'medium_large', 'large'))) {
                $sizes[$_size]['width'] = get_option("{$_size}_size_w");
                $sizes[$_size]['height'] = get_option("{$_size}_size_h");
            } elseif (isset($_wp_additional_image_sizes[$_size])) {
                $sizes[$_size] = array(
                    'width' => $_wp_additional_image_sizes[$_size]['width'],
                    'height' => $_wp_additional_image_sizes[$_size]['height'],
                );
            }
            $options[$_size] = $sizes[$_size]['width'] . "x" . $sizes[$_size]['height'];
        }
        return $options;
    }

    static public function dp_ocp_custom_query() {
        $args = apply_filters('dp_ocp_custom_query_args', array('posts_per_page' => 10, 'post_type' => 'post', 'post_status' => 'publish'));
        return $args;
    }

    static public function dp_get_custom_fields($custom_fields_array, $post_id) {
        foreach ($custom_fields_array as $field_display) {
            $custom_field = trim($field_display);
            $field_display = ucfirst(str_replace('_', ' ', ltrim($field_display)));
            $field_display .= ' - ';
            $post_custom_fields[$field_display] = get_post_meta($post_id, $custom_field, true);
        }
        return $post_custom_fields;
    }

    static public function dp_get_keyed_custom_fields($custom_fields_array, $post_id) {
        foreach ($custom_fields_array as $field_display => $field_value) {
            $custom_field = trim($field_value);
            $post_custom_fields[$field_display] = get_post_meta($post_id, $custom_field, true);
        }
        return $post_custom_fields;
    }

}
