jQuery(document).ready(function () {
    jQuery(document.body).on('click', '.dp_ocp_reload_cpt', function (ev) {
        ev.preventDefault();
        jQuery.ajax({
            type: 'POST',
            url: dpOcpAjax.ajaxurl,
            data: {
                'action': 'dp_ocp_ajax_reload_cpt',
            },
            beforeSend: function () {
            }
        }).done(function (data, textStatus, jqXHR) {
            if (data === 'OK') {
                var cpt_container = jQuery('div.et-pb-option[data-option_name="cpt_name"]').children('div.et-pb-option-container');
                var categories_container = jQuery('div.et-pb-option[data-option_name="cpt_categories"]').children('div.et-pb-option-container');
                var input_cpt = cpt_container.find('input:checked');
                var input_categories = categories_container.find('input:checked');
                var cpt_checked = [];
                var categories_checked = [];
                input_cpt.each(function () {
                    cpt_checked.push(jQuery(this).val());
                });
                input_categories.each(function () {
                    categories_checked.push(jQuery(this).val());
                });
                cpt_container.find('label').remove();
                categories_container.find('label').remove();
                cpt_container.find('br').remove();
                categories_container.find('br').remove();
                var main_container = jQuery('div.et-pb-main-settings');
                main_container.prepend('<div class="dp_ocp_admin_overlay_container"><div class="dp_ocp_loader"></div></div>');
                jQuery.ajax({
                    type: 'POST',
                    url: dpOcpAjax.ajaxurl,
                    data: {
                        'action': 'dp_ocp_ajax_get_cpt_and_categories',
                    },
                    beforeSend: function () {
                        jQuery(this).attr('disabled', true);
                    }
                }).done(function (data, textStatus, jqXHR) {
                    var data = JSON.parse(data);
                    var new_cpt = data['cpt'];
                    var new_categories = data['categories'];
                    for (var item in new_cpt) {
                        var checked = "";
                        if (cpt_checked.includes(new_cpt[item]["pt"])) {
                            var checked = "checked";
                        }
                        cpt_container.prepend('<label><input type="checkbox" class="et_dp_ocp_cpt" name="et_pb_custom_post_types" value="' + new_cpt[item]['pt'] + '" ' + checked + '> ' + new_cpt[item]["pt"].substring(0, 1).toUpperCase() + new_cpt[item]["pt"].substring(1) + '</label><br>');
                    }
                    for (var item in new_categories) {
                        var checked = "";
                        if (categories_checked.includes(new_categories[item]['term_id'])) {
                            var checked = "checked";
                        }
                        if (new_categories[item]['name'] !== 'dp_none_taxonomy') {
                            categories_container.prepend('<label class="et_dp_ocp_categories"><input type="checkbox" name="et_pb_include_categories" value="' + new_categories[item]['term_id'] + '" ' + checked + '> ' + new_categories[item]['name'] + ' (' + new_categories[item]['pt'] + ')' + '</label><br>');
                        }
                    }
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                }).always(function () {
                     jQuery('.dp_ocp_admin_overlay_container').fadeOut(1500, function () {
                        jQuery(this).remove();
                    });
                });
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        });
    });
    jQuery(document.body).on('click', '.et_dp_ocp_cpt', function () {
        var value = jQuery(this).val();
        if (jQuery(this).is(':checked')) {
            jQuery('.et_dp_ocp_categories').each(function () {
                if (jQuery(this).text().includes("(" + value + ")")) {
                    jQuery(this).children('input').attr("checked", true);
                }
            });
        } else {
            jQuery('.et_dp_ocp_categories').each(function () {
                if (jQuery(this).text().includes("(" + value + ")")) {
                    jQuery(this).children('input').attr("checked", false);
                }
            });
        }
    });
    jQuery(document.body).on('click', '.et_dp_ocp_categories', function () {
        var value = jQuery(this).text();
        if (jQuery(this).children('input').is(':checked')) {
            jQuery('.et_dp_ocp_cpt').each(function () {
                if (value.includes("(" + jQuery(this).val() + ")") && !jQuery(this).is(':checked')) {
                    jQuery(this).attr("checked", true);
                }
            });
        }
    });
});