<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.diviplugins.com
 * @since      1.5
 *
 * @package    Dp_Owl_Carousel_Pro
 * @subpackage Dp_Owl_Carousel_Pro/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Dp_Owl_Carousel_Pro
 * @subpackage Dp_Owl_Carousel_Pro/admin
 * @author     DiviPlugins <support@diviplugins.com>
 */
class Dp_Owl_Carousel_Pro_Admin {

    /**
     * The ID of this plugin.
     *
     * @since 1.5
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since 1.5
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since 1.5
     * @param      string    $plugin_name       The name of this plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version) {
        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since 1.5
     */
    public function enqueue_styles() {
        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/dp-owl-carousel-pro-admin.css', array(), $this->version, 'all');
    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since 1.5
     */
    public function enqueue_scripts() {
        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/dp-owl-carousel-pro-admin.js', array('jquery'), $this->version, false);
        wp_localize_script($this->plugin_name, 'dpOcpAjax', array('ajaxurl' => admin_url('admin-ajax.php')));
    }

    public function init_plugin_updater() {
        $license_key = trim(get_option('dp_ocp_license_key'));
        new Dp_Owl_Carousel_Pro_Updater(DPOCP_STORE_URL, DPOCP_DIR . 'dp_owl_carousel_pro.php', array(
            'version' => DPOCP_VERSION,
            'license' => $license_key,
            'item_id' => DPOCP_ITEM_ID,
            'item_name' => DPOCP_ITEM_NAME,
            'author' => 'Divi Plugins',
            'beta' => false
                )
        );
    }

    public function add_license_menu_page() {
        add_plugins_page('Plugin License', 'OCP License', 'manage_options', DPOCP_LICENSE_PAGE, array($this, 'license_page_html'));
    }

    public function license_page_html() {
        $license = get_option('dp_ocp_license_key');
        $status = get_option('dp_ocp_license_status');
        ?>
        <div class="wrap">
            <h2><?php _e('Owl Carousel Pro License Options'); ?></h2>
            <form method="post" action="options.php">
                <?php settings_fields('dp_ocp_license'); ?>
                <table class="form-table">
                    <tbody>
                        <tr valign="top">
                            <th scope="row" valign="top">
                                <?php _e('License Key', DPOCP_NAME); ?>
                            </th>
                            <td>
                                <input id="dp_ocp_license_key" name="dp_ocp_license_key" type="text" class="regular-text" value="<?php esc_attr_e($license); ?>" />
                                <label class="description" for="dp_ocp_license_key"><?php _e('Enter your license key', DPOCP_NAME); ?></label>
                            </td>
                        </tr>
                        <?php if (false !== $license) { ?>
                            <tr valign="top">
                                <th scope="row" valign="top">
                                    <?php _e('Activate License', DPOCP_NAME); ?>
                                </th>
                                <td>
                                    <?php if ($status !== false && $status == 'valid') { ?>
                                        <span style="color:green; font-weight: bold; font-size: 18px; vertical-align: bottom;"><?php _e('Active', DPOCP_NAME); ?></span>
                                        <?php wp_nonce_field('dp_ocp_license_nonce', 'dp_ocp_license_nonce'); ?>
                                        <input type="submit" class="button-secondary" name="dp_ocp_license_deactivate" value="<?php _e('Deactivate License', DPOCP_NAME); ?>"/>
                                        <?php
                                    } else {
                                        wp_nonce_field('dp_ocp_license_nonce', 'dp_ocp_license_nonce');
                                        ?>
                                        <input type="submit" class="button-secondary" name="dp_ocp_license_activate" value="<?php _e('Activate License', DPOCP_NAME); ?>"/>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <?php submit_button(); ?>

            </form>
        </div>
        <?php
    }

    public function register_license_option() {
        register_setting('dp_ocp_license', 'dp_ocp_license_key', array($this, 'sanitize_license'));
    }

    public function sanitize_license($new) {
        $old = get_option('dp_ocp_license_key');
        if ($old && $old != $new) {
            delete_option('dp_ocp_license_status');
        }
        return $new;
    }

    public function activate_license() {
        if (isset($_POST['dp_ocp_license_activate'])) {
            if (!check_admin_referer('dp_ocp_license_nonce', 'dp_ocp_license_nonce')) {
                return;
            }
            if ($_POST['dp_ocp_license_key'] !== get_option('dp_ocp_license_key')) {
                update_option('dp_ocp_license_key', $_POST['dp_ocp_license_key']);
                $license = trim($_POST['dp_ocp_license_key']);
            } else {
                $license = trim(get_option('dp_ocp_license_key'));
            }
            $api_params = array(
                'edd_action' => 'activate_license',
                'license' => $license,
                'item_name' => urlencode(DPOCP_ITEM_NAME),
                'url' => home_url()
            );
            $response = wp_remote_post(DPOCP_STORE_URL, array('timeout' => 15, 'sslverify' => false, 'body' => $api_params));
            if (is_wp_error($response) || 200 !== wp_remote_retrieve_response_code($response)) {

                if (is_wp_error($response)) {
                    $message = $response->get_error_message();
                } else {
                    $message = __('An error occurred, please try again.');
                }
            } else {
                $license_data = json_decode(wp_remote_retrieve_body($response));
                if (false === $license_data->success) {
                    switch ($license_data->error) {
                        case 'expired' :
                            $message = sprintf(
                                    __('Your license key expired on %s.', DPOCP_NAME), date_i18n(get_option('date_format'), strtotime($license_data->expires, current_time('timestamp')))
                            );
                            break;
                        case 'revoked' :
                            $message = __('Your license key has been disabled.', DPOCP_NAME);
                            break;
                        case 'missing' :
                            $message = __('Invalid license.', DPOCP_NAME);
                            break;
                        case 'invalid' :
                        case 'site_inactive' :
                            $message = __('Your license is not active for this URL.', DPOCP_NAME);
                            break;
                        case 'item_name_mismatch' :
                            $message = sprintf(__('This appears to be an invalid license key for %s.', DPOCP_NAME), DPOCP_ITEM_NAME);
                            break;
                        case 'no_activations_left':
                            $message = __('Your license key has reached its activation limit.', DPOCP_NAME);
                            break;
                        default :
                            $message = __('An error occurred, please try again.', DPOCP_NAME);
                            break;
                    }
                }
            }
            if (!empty($message)) {
                $base_url = admin_url('plugins.php?page=' . DPOCP_LICENSE_PAGE);
                $redirect = add_query_arg(array('sl_activation' => 'false', 'message' => urlencode($message)), $base_url);
                wp_redirect($redirect);
                exit();
            }
            update_option('dp_ocp_license_status', $license_data->license);
            wp_redirect(admin_url('plugins.php?page=' . DPOCP_LICENSE_PAGE . '&sl_activation=true&message=OK'));
            exit();
        }
    }

    public function deactivate_license() {
        if (isset($_POST['dp_ocp_license_deactivate'])) {
            if (!check_admin_referer('dp_ocp_license_nonce', 'dp_ocp_license_nonce')) {
                return;
            }
            $license = trim(get_option('dp_ocp_license_key'));
            $api_params = array(
                'edd_action' => 'deactivate_license',
                'license' => $license,
                'item_name' => urlencode(DPOCP_ITEM_NAME),
                'url' => home_url()
            );
            $response = wp_remote_post(DPOCP_STORE_URL, array('timeout' => 15, 'sslverify' => false, 'body' => $api_params));
            if (is_wp_error($response) || 200 !== wp_remote_retrieve_response_code($response)) {
                if (is_wp_error($response)) {
                    $message = $response->get_error_message();
                } else {
                    $message = __('An error occurred, please try again.');
                }
                $base_url = admin_url('plugins.php?page=' . DPOCP_LICENSE_PAGE);
                $redirect = add_query_arg(array('sl_activation' => 'false', 'message' => urlencode($message)), $base_url);
                wp_redirect($redirect);
                exit();
            }
            $license_data = json_decode(wp_remote_retrieve_body($response));
            if ($license_data->license == 'deactivated') {
                delete_option('dp_ocp_license_status');
            }
            wp_redirect(admin_url('plugins.php?page=' . DPOCP_LICENSE_PAGE));
            exit();
        }
    }

    public function admin_notice_license_result() {
        if (isset($_GET['sl_activation']) && !empty($_GET['message']) && $_GET['page'] === DPOCP_LICENSE_PAGE) {
            if ($_GET['sl_activation'] === 'false') {
                $message = urldecode($_GET['message']);
                echo sprintf('<div class="notice notice-error is-dismissible"><p>%1$s</p></div>', $message);
            } else {
                echo sprintf('<div class="notice notice-success is-dismissible"><p>%1$s</p></div>', __('Thanks for purchase and activate DP Owl Carousel Pro', DPOCP_NAME));
            }
        }
    }

    public function admin_notice_license_activation() {
        echo sprintf('<div class="notice notice-info is-dismissible"><p>%1$s <a href="plugins.php?page=%2$s">%3$s</a></p></div>', __('Please activate your Owl Carousel Pro license.', DPOCP_NAME), DPOCP_LICENSE_PAGE, __('OCP Activation Page', DPOCP_NAME));
    }

    public function admin_notice_welcome() {
        if (get_transient('ocp_admin_notice_welcome')) {
            echo sprintf('<div class="notice notice-success is-dismissible"><p>%1$s</p></div>', __('Thank you for purchasing <strong>DP Owl Carousel Pro!</strong>.', DPOCP_NAME));
            delete_transient('ocp_admin_notice_welcome');
        }
    }

    public function admin_notice_error() {
        if (get_transient('ocp_admin_notice_error')) {
            echo sprintf('<div class="notice notice-info notice-large is-dismissible"><p>%1$s</p></div>', __('We noticed that you have DP Owl Carousel Free version activated. We just deactivated it for you. These plugins cannot be activated at the same time.', DPOCP_NAME));
            delete_transient('ocp_admin_notice_error');
            deactivate_plugins('dp-owl-carousel/dp_owl_carousel.php');
        }
    }

    public function remove_from_local_storage() {
        echo '<script>for (var prop in localStorage) {localStorage.removeItem(prop);}</script>';
    }

    public function include_the_modules() {
        global $dp_terms_array;
        global $dp_image_sizes;
        $dp_terms_array = Dp_Owl_Carousel_Pro_Utils::terms_array();
        $dp_image_sizes = Dp_Owl_Carousel_Pro_Utils::get_custom_sizes();
        require_once DPOCP_DIR . 'includes/modules/dp_oc.php';
        require_once DPOCP_DIR . 'includes/modules/dp_oc_custom.php';
        require_once DPOCP_DIR . 'includes/modules/dp_oc_fullwidth.php';
        require_once DPOCP_DIR . 'includes/modules/dp_oc_custom_fullwidth.php';
    }

    public function merge_dp_ocp_defaults($divi_defaults) {
        $font_defaults = array(
            'size' => '14px',
            'color' => '#333333',
            'letter_spacing' => '0px',
            'line_height' => '1.7em',
        );
        /* $dp_oc_responsive_amount_items = array(
          'desktop' => 5,
          'tablet' => 3,
          'phone' => 1,
          ); */
        $dp_defaults = array(
            'et_pb_dp_oc-number_thumb' => 5,
            'et_pb_dp_oc_fw-number_thumb' => 5,
            /* 'et_pb_dp_oc-number_thumb_tablet' => $dp_oc_responsive_amount_items['tablet'],
              'et_pb_dp_oc-number_thumb_phone' => $dp_oc_responsive_amount_items['phone'], */
            'et_pb_dp_oc_custom-number_thumb' => 5,
            'et_pb_dp_oc_custom_fw-number_thumb' => 5,
            /* 'et_pb_dp_oc_custom-number_thumb_tablet' => $dp_oc_responsive_amount_items['tablet'],
              'et_pb_dp_oc_custom-number_thumb_phone' => $dp_oc_responsive_amount_items['phone'], */
            'et_pb_dp_oc-post_title_text_color' => $font_defaults['color'],
            'et_pb_dp_oc-post_title_font_style' => '',
            'et_pb_dp_oc-post_title_font_size' => $font_defaults['size'],
            'et_pb_dp_oc-post_title_line_height' => $font_defaults['line_height'],
            'et_pb_dp_oc-post_title_letter_spacing' => $font_defaults['letter_spacing'],
            'et_pb_dp_oc-post_meta_text_color' => $font_defaults['color'],
            'et_pb_dp_oc-post_meta_font_style' => '',
            'et_pb_dp_oc-post_meta_font_size' => $font_defaults['size'],
            'et_pb_dp_oc-post_meta_line_height' => $font_defaults['line_height'],
            'et_pb_dp_oc-post_meta_letter_spacing' => $font_defaults['letter_spacing'],
            'et_pb_dp_oc-post_excerpt_text_color' => $font_defaults['color'],
            'et_pb_dp_oc-post_excerpt_font_style' => '',
            'et_pb_dp_oc-post_excerpt_font_size' => $font_defaults['size'],
            'et_pb_dp_oc-post_excerpt_line_height' => $font_defaults['line_height'],
            'et_pb_dp_oc-post_excerpt_letter_spacing' => $font_defaults['letter_spacing'],
            'et_pb_dp_oc-read_more_text_color' => $font_defaults['color'],
            'et_pb_dp_oc-read_more_font_style' => '',
            'et_pb_dp_oc-read_more_font_size' => $font_defaults['size'],
            'et_pb_dp_oc-read_more_line_height' => $font_defaults['line_height'],
            'et_pb_dp_oc-read_more_letter_spacing' => $font_defaults['letter_spacing'],
            'et_pb_dp_oc-dp_custom_field_text_color' => $font_defaults['color'],
            'et_pb_dp_oc-dp_custom_field_font_style' => '',
            'et_pb_dp_oc-dp_custom_field_font_size' => $font_defaults['size'],
            'et_pb_dp_oc-dp_custom_field_line_height' => $font_defaults['line_height'],
            'et_pb_dp_oc-dp_custom_field_letter_spacing' => $font_defaults['letter_spacing'],
            'et_pb_dp_oc_fw-post_title_text_color' => $font_defaults['color'],
            'et_pb_dp_oc_fw-post_title_font_style' => '',
            'et_pb_dp_oc_fw-post_title_font_size' => $font_defaults['size'],
            'et_pb_dp_oc_fw-post_title_line_height' => $font_defaults['line_height'],
            'et_pb_dp_oc_fw-post_title_letter_spacing' => $font_defaults['letter_spacing'],
            'et_pb_dp_oc_fw-post_meta_text_color' => $font_defaults['color'],
            'et_pb_dp_oc_fw-post_meta_font_style' => '',
            'et_pb_dp_oc_fw-post_meta_font_size' => $font_defaults['size'],
            'et_pb_dp_oc_fw-post_meta_line_height' => $font_defaults['line_height'],
            'et_pb_dp_oc_fw-post_meta_letter_spacing' => $font_defaults['letter_spacing'],
            'et_pb_dp_oc_fw-post_excerpt_text_color' => $font_defaults['color'],
            'et_pb_dp_oc_fw-post_excerpt_font_style' => '',
            'et_pb_dp_oc_fw-post_excerpt_font_size' => $font_defaults['size'],
            'et_pb_dp_oc_fw-post_excerpt_line_height' => $font_defaults['line_height'],
            'et_pb_dp_oc_fw-post_excerpt_letter_spacing' => $font_defaults['letter_spacing'],
            'et_pb_dp_oc_fw-read_more_text_color' => $font_defaults['color'],
            'et_pb_dp_oc_fw-read_more_font_style' => '',
            'et_pb_dp_oc_fw-read_more_font_size' => $font_defaults['size'],
            'et_pb_dp_oc_fw-read_more_line_height' => $font_defaults['line_height'],
            'et_pb_dp_oc_fw-read_more_letter_spacing' => $font_defaults['letter_spacing'],
            'et_pb_dp_oc_fw-dp_custom_field_text_color' => $font_defaults['color'],
            'et_pb_dp_oc_fw-dp_custom_field_font_style' => '',
            'et_pb_dp_oc_fw-dp_custom_field_font_size' => $font_defaults['size'],
            'et_pb_dp_oc_fw-dp_custom_field_line_height' => $font_defaults['line_height'],
            'et_pb_dp_oc_fw-dp_custom_field_letter_spacing' => $font_defaults['letter_spacing'],
            'et_pb_dp_oc_custom-image_title_text_color' => $font_defaults['color'],
            'et_pb_dp_oc_custom-image_title_font_style' => '',
            'et_pb_dp_oc_custom-image_title_font_size' => $font_defaults['size'],
            'et_pb_dp_oc_custom-image_title_line_height' => $font_defaults['line_height'],
            'et_pb_dp_oc_custom-image_title_letter_spacing' => $font_defaults['letter_spacing'],
            'et_pb_dp_oc_custom-image_content_text_color' => $font_defaults['color'],
            'et_pb_dp_oc_custom-image_content_font_style' => '',
            'et_pb_dp_oc_custom-image_content_font_size' => $font_defaults['size'],
            'et_pb_dp_oc_custom-image_content_line_height' => $font_defaults['line_height'],
            'et_pb_dp_oc_custom-image_content_letter_spacing' => $font_defaults['letter_spacing'],
            'et_pb_dp_oc_custom_fw-image_title_text_color' => $font_defaults['color'],
            'et_pb_dp_oc_custom_fw-image_title_font_style' => '',
            'et_pb_dp_oc_custom_fw-image_title_font_size' => $font_defaults['size'],
            'et_pb_dp_oc_custom_fw-image_title_line_height' => $font_defaults['line_height'],
            'et_pb_dp_oc_custom_fw-image_title_letter_spacing' => $font_defaults['letter_spacing'],
            'et_pb_dp_oc_custom_fw-image_content_text_color' => $font_defaults['color'],
            'et_pb_dp_oc_custom_fw-image_content_font_style' => '',
            'et_pb_dp_oc_custom_fw-image_content_font_size' => $font_defaults['size'],
            'et_pb_dp_oc_custom_fw-image_content_line_height' => $font_defaults['line_height'],
            'et_pb_dp_oc_custom_fw-image_content_letter_spacing' => $font_defaults['letter_spacing'],
        );
        foreach ($dp_defaults as $setting_name => $default_value) {
            $dp_defaults[$setting_name] = array('default' => $default_value,);
            $actual_value = !et_is_builder_plugin_active() ? et_get_option($setting_name, '', '', true) : '';
            if ('' !== $actual_value) {
                $dp_defaults[$setting_name]['actual'] = $actual_value;
            }
        }
        $defaults = array_merge($dp_defaults, $divi_defaults);
        return $defaults;
    }

    public function add_term($term_id, $tt_id, $taxonomy) {
        $tax = get_taxonomy($taxonomy);
        $term = get_term_field('name', $term_id, $taxonomy);
        $default_post_type = array('post' => 'post',);
        $post_types = get_post_types(array('_builtin' => false, 'public' => true));
        $post_types += $default_post_type;
        if (in_array($tax->object_type[0], $post_types) && $tax->name !== 'post_tag') {
            global $wpdb;
            $table_name = $wpdb->prefix . "dp_available_categories";
            if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
                Dp_Owl_Carousel_Pro_Utils::create_table();
            }
            $wpdb->insert($table_name, array('term_id' => $term_id, 'name' => $term, 'tax' => $taxonomy, 'pt' => $tax->object_type[0]), array('%d', '%s', '%s', '%s'));
        }
    }

    public function delete_term($term_id) {
        global $wpdb;
        $table_name = $wpdb->prefix . "dp_available_categories";
        if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            Dp_Owl_Carousel_Pro_Utils::create_table();
        }
        $wpdb->delete($table_name, array('term_id' => $term_id), array('%d'));
    }

    public function edit_term($term_id, $taxonomy) {
        $tax = get_taxonomy($taxonomy);
        $term = get_term_field('name', $term_id, $taxonomy);
        global $wpdb;
        $table_name = $wpdb->prefix . "dp_available_categories";
        if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            Dp_Owl_Carousel_Pro_Utils::create_table();
        }
        $wpdb->update($table_name, array('term_id' => $term_id, 'name' => $term, 'tax' => $taxonomy, 'pt' => $tax->object_type[0]), array('term_id' => $term_id), array('%d', '%s', '%s', '%s'), array('%d'));
    }

    public function add_cpt($post_type, $post_type_object) {//here we get the post type slug and the post type object
        if (!$post_type_object->_builtin && $post_type_object->public && $post_type != 'project') { //if the post type is not _builtin and is public and is not project because cpt project is already in the db when the plugin is install
            global $wpdb;
            $table_name = $wpdb->prefix . "dp_available_categories";
            if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
                Dp_Owl_Carousel_Pro_Utils::create_table();
            }
            $already_in = $wpdb->get_results("SELECT DISTINCT term_id,name,pt FROM $table_name WHERE pt='" . $post_type . "';", ARRAY_A);
            if (empty($already_in)) {
                $wpdb->insert($table_name, array('term_id' => 0, 'name' => 'dp_none_taxonomy', 'tax' => 'dp_none_taxonomy', 'pt' => $post_type), array('%d', '%s', '%s', '%s'));
            }
        }
    }

    public function register_img_size() {
        add_image_size('dp-ocp-square-thumb', 400, 400, true);
    }

    public function custom_size($sizes) {
        return array_merge($sizes, array('dp-ocp-square-thumb' => __('Owl Carosuel Square', DPOCP_NAME),));
    }

    public function ajax_reload_cpt() {
        Dp_Owl_Carousel_Pro_Utils::fill_table();
        echo 'OK';
        wp_die();
    }

    public function ajax_get_cpt_and_categories() {
        $data = array();
        $data['cpt'] = array_reverse(Dp_Owl_Carousel_Pro_Utils::get_post_types());
        $data['categories'] = array_reverse(Dp_Owl_Carousel_Pro_Utils::get_terms());
        echo json_encode($data);
        wp_die();
    }

}
