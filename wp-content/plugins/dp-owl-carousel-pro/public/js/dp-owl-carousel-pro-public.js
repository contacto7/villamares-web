jQuery(document).ready(function () {
    jQuery("div.et_pb_dp_oc .owl-carousel").each(function () {
        var args = [];
        var rotate = jQuery(this).attr('data-rotate');
        var speed = jQuery(this).attr('data-speed');
        var animation_speed = jQuery(this).attr('data-animation-speed');
        var arrows_speed = jQuery(this).attr('data-arrows-speed');
        var controls_speed = jQuery(this).attr('data-controls-speed');
        var hover = jQuery(this).attr('data-hover');
        var arrow = jQuery(this).attr('data-arrow');
        var control = jQuery(this).attr('data-control');
        var use_hash_thumbnail = jQuery(this).attr('data-use-hash');
        var auto_width = jQuery(this).attr('data-use-auto-width');
        var items = jQuery(this).attr('data-items');
        var items_tablet = jQuery(this).attr('data-items-tablet');
        var items_phone = jQuery(this).attr('data-items-phone');
        var margin = parseInt(jQuery(this).attr('data-margin'));
        var behaviour = jQuery(this).attr('data-behaviour');
        var direction = jQuery(this).attr('data-direction');
        var center = jQuery(this).attr('data-center');
        var items_per_dot = jQuery(this).attr('data-items-per-dot');
        var items_per_slide = jQuery(this).attr('data-items-per-slide');
        var arrow_size = jQuery(this).attr('data-arrow-size');
        var is_custom = jQuery(this).attr('data-custom');
        var lazy_load = jQuery(this).attr('data-lazy');
        if (is_custom === 'yes' && use_hash_thumbnail === 'on') {
            var module = jQuery(this).attr('data-module');
            jQuery(this).find('.dp_oc_item').each(function () {
                var number = jQuery(this).attr('data-item');
                jQuery(this).attr('data-hash', module + number);
            });
        }
        switch (arrow_size) {
            case '6em':
                arrow_size = 'arrow_size_large';
                break;
            case '4em':
                arrow_size = 'arrow_size_medium';
                break;
            case '2em':
                arrow_size = 'arrow_size_small';
                break;
            default:
                arrow_size = 'arrow_size_small';
                break;
        }
        if (behaviour === 'loop') {
            var loop = true;
            var rewind = false;
        } else if (behaviour === 'rewind') {
            var loop = false;
            var rewind = true;
        } else {
            var loop = false;
            var rewind = false;
        }
        args['center'] = (center === 'on') ? true : false;
        args['rewind'] = rewind;
        args['loop'] = loop;
        args['nav'] = (arrow === 'on') ? true : false;
        args['dots'] = (control === 'on') ? true : false;
        args['URLhashListener'] = (use_hash_thumbnail === 'on') ? true : false;
        args['dotsEach'] = (isNaN(items_per_dot)) ? 1 : items_per_dot;
        args['slideBy'] = (isNaN(items_per_slide)) ? 1 : items_per_slide;
        args['rtl'] = (direction === 'ltr') ? true : false;
        args['navText'] = ["", ""];
        args['margin'] = (isNaN(margin)) ? 8 : margin;
        args['autoplay'] = (rotate === 'on') ? true : false;
        args['autoplayTimeout'] = (isNaN(speed)) ? 5000 : speed;
        args['autoplaySpeed'] = (isNaN(animation_speed)) ? 500 : animation_speed;
        args['navSpeed'] = (isNaN(arrows_speed)) ? 500 : arrows_speed;
        args['dotsSpeed'] = (isNaN(controls_speed)) ? 500 : controls_speed;
        args['autoplayHoverPause'] = (hover === 'on') ? true : false;
        args['responsiveClass'] = true;
        args['autoWidth'] = (auto_width === 'on') ? true : false;
        args['lazyLoad'] = (lazy_load === 'on') ? true : false;
        args['responsive'] = {0: {items: ('' === items_phone) ? 1 : items_phone}, 600: {items: ('' === items_tablet) ? 3 : items_tablet}, 1000: {items: ('' === items) ? 5 : items}};
        jQuery(this).owlCarousel(args);
        jQuery(this).find('.owl-nav').addClass(arrow_size);
    });
    function checkIsInArray(array, value) {
        var is_in = false;
        if (Array.isArray(array)) {
            for (var item in array) {
                if (array.item === value) {
                    is_in = true;
                }
            }
        }
        return is_in;
    }
    jQuery('a.dp_ocp_lightbox_image').click(function () {
        var args = {
            type: 'image',
            removalDelay: 500,
            mainClass: 'mfp-fade dp-ocp-lightbox'
        };
        var dp_ocp_gallery_images = [];
        var dp_ocp_gallery_titles = [];
        var index = 0;
        var start_at = 0;
        var trigger_by = jQuery(this).prop('href');
        if (jQuery(this).children('img').attr('data-lightbox-gallery') === 'on') {
            jQuery(this).parents('.owl-carousel').find('.dp_ocp_lightbox_image').each(function () {
                if (!checkIsInArray(dp_ocp_gallery_images, jQuery(this).prop('href'))) {
                    if (trigger_by === jQuery(this).prop('href')) {
                        start_at = index;
                    }
                    index++;
                    dp_ocp_gallery_images.push(jQuery(this).prop('href'));
                    var title = jQuery(this).children('img').attr('title');
                    var alt = jQuery(this).children('img').attr('alt');
                    if (title !== undefined && title !== '') {
                        dp_ocp_gallery_titles.push(title);
                    } else if (alt !== undefined && alt !== '' && jQuery(this).children('img').hasClass('dp_oc_post_thumb')) {
                        dp_ocp_gallery_titles.push(alt);
                    }
                }
            });
            args['gallery'] = {enabled: true, navigateByImgClick: true};
        } else {
            dp_ocp_gallery_images.push(jQuery(this).prop('href'));
            var title = jQuery(this).children('img').attr('title');
            var alt = jQuery(this).children('img').attr('alt');
            if (title !== undefined && title !== '') {
                dp_ocp_gallery_titles.push(title);
            } else if (alt !== undefined && alt !== '' && jQuery(this).children('img').hasClass('dp_oc_post_thumb')) {
                dp_ocp_gallery_titles.push(alt);
            }
        }
        for (var item in dp_ocp_gallery_images) {
            dp_ocp_gallery_images[item] = {'src': dp_ocp_gallery_images[item], 'title': dp_ocp_gallery_titles[item]};
        }
        args['items'] = dp_ocp_gallery_images;
        jQuery.magnificPopup.open(args, start_at);
        return false;
    });
});