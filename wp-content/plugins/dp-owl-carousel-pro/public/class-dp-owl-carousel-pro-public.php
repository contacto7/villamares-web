<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://www.diviplugins.com
 * @since      1.5
 *
 * @package    Dp_Owl_Carousel_Pro
 * @subpackage Dp_Owl_Carousel_Pro/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Dp_Owl_Carousel_Pro
 * @subpackage Dp_Owl_Carousel_Pro/public
 * @author     DiviPlugins <support@diviplugins.com>
 */
class Dp_Owl_Carousel_Pro_Public {

    /**
     * The ID of this plugin.
     *
     * @since 1.5
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since 1.5
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since 1.5
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since 1.5
     */
    public function enqueue_styles() {
        wp_register_style('dp-ocp-owl-carousel', DPOCP_URL . 'vendor/owl.carousel/assets/owl.carousel.min.css', false, DPOCP_VERSION);
        wp_register_style('dp-ocp-custom', DPOCP_URL . 'public/css/dp-owl-carousel-pro-public.css', array(), $this->version, 'all');
    }

    /**
     * Register the JavaScript for the public-facing side of the site.
     *
     * @since 1.5
     */
    public function enqueue_scripts() {
        wp_register_script('dp-ocp-owl-carousel', DPOCP_URL . 'vendor/owl.carousel/owl.carousel.min.js', array('jquery'), DPOCP_VERSION, true);
        wp_register_script('dp-ocp-custom', DPOCP_URL . 'public/js/dp-owl-carousel-pro-public.js', array('jquery'), $this->version, false);
    }

}
